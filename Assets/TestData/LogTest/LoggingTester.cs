using System;
using MantaMiddleware.LogUtilities;
using UnityEngine;

public class LoggingTester : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (SelectiveLogger.IsLogging(this))
        {
            SelectiveLogger.Log(this, "Should be in Start");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SelectiveLogger.IsLogging(this))
        {
            SelectiveLogger.Log(this, "Should be in Update");
        }

        try
        {
            int x = 0;
            int y = 50 / x;

        }
        catch (Exception e)
        {
            SelectiveLogger.LogException(this, e);
        }
    }

    private void LateUpdate()
    {
        if (SelectiveLogger.IsVerbose(this))
        {
            SelectiveLogger.Log(this, "Should be in LateUpdate");
        }
    }
}
