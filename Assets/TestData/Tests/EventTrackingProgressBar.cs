using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using TMPro;
using UnityEngine;

public class EventTrackingProgressBar : MonoBehaviour, 
    IRequestCastListener<ProgressBarTrackedRequest, bool>, 
    IProgressWatcher
{
    public TMP_Text progressText;
    
    void Start()
    {
        EventCaster.Main.Subscribe(this);
    }

    public UniTask<bool> Handle(ProgressBarTrackedRequest evt)
    {
        evt.WatchProgress(this);
        return new UniTask<bool>(false);
    }

    public void SetProgress(float prog)
    {
        progressText.text = prog.ToString("#.0%");
    }

    async UniTask IEventCastListener<ProgressBarTrackedRequest>.Handle(ProgressBarTrackedRequest evt)
    {
        await Handle(evt);
    }
}

