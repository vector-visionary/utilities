using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using MantaMiddleware.Events;
using UnityEngine.AddressableAssets;

public class SpawnCreationTrigger : MonoBehaviour, IEventCastListener<SetSpawnPositionEvent>
{
    public AssetReferenceGameObject prefab;
    [SerializeField] private Vector3 pos;

    private void Awake()
    {
        EventCaster.Main.Subscribe(this);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SpawnIt();
        }    
    }

    private async void SpawnIt()
    {
        Debug.Log($"Starting spawn at {Time.time}");
        var used = await EventCaster.Main.CastWithResult<SpawnObjectWithPositionRequest, bool>(new SpawnObjectWithPositionRequest(prefab, pos));
        Debug.Log($"Done with spawn at {Time.time}; used = {used}");

    }

    public async UniTask Handle(SetSpawnPositionEvent evt)
    {
        pos = evt.Position;
    }
}