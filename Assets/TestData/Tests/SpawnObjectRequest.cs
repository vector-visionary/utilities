using System;
using MantaMiddleware.Events;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

public class SpawnObjectRequest : CastableRequestBase<bool>
{
    public AssetReferenceGameObject Prefab => _prefab;
    private readonly AssetReferenceGameObject _prefab;

    public SpawnObjectRequest(AssetReferenceGameObject prefab)
    {
        _prefab = prefab;
    }

    public override bool AllowThisEventToCache => false;
    public override Type ChainUntilBaseType => typeof(SpawnObjectRequest);
}