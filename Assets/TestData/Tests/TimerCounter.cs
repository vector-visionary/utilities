using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using UnityEngine;

public class TimerCounter : MonoBehaviour, IEventCastListener<TimerRequest>
{
    private void Awake()
    {
        EventCaster.Main.Subscribe<TimerRequest>(this);
    }

    public async UniTask Handle(TimerRequest evt)
    {
        for (float t = 0f; t < evt.duration; t += Time.deltaTime)
        {
            await UniTask.DelayFrame(1);
            evt.MarkUsageState(this, new SimpleProgressState(t/evt.duration, evt.duration));
        }
    }
}