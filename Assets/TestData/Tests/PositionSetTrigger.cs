using MantaMiddleware.Events;
using UnityEngine;

public class PositionSetTrigger : MonoBehaviour
{
    public KeyCode triggerKey = KeyCode.Alpha1;
    public Vector3 myPos;

    private void Update()
    {
        if (Input.GetKeyDown(triggerKey))
        {
            EventCaster.Main.Cast(new SetSpawnPositionEvent(myPos));
        }
    }
}