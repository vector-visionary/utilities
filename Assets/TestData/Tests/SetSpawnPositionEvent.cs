using MantaMiddleware.Events;
using UnityEngine;

public class SetSpawnPositionEvent : CastableEventBase
{
    public Vector3 Position => _position;
    private readonly Vector3 _position;
    public override bool AllowThisEventToCache => true;

    public SetSpawnPositionEvent(Vector3 position)
    {
        _position = position;
    }
}