using System;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using UnityEngine;

public class TimerEventSender : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            SendEvent().Forget();
        }
    }

    private async UniTaskVoid SendEvent()
    {
        await EventCaster.Main.CastWithResult<TimerRequest, bool>(new TimerRequest(5f));
        Debug.Log("Finished");
    }
}

public class ProgressBarTrackedRequest : TrackedCastableRequestBase<bool>
{
    public override Type ChainUntilBaseType => typeof(ProgressBarTrackedRequest);
}

public class TimerRequest : ProgressBarTrackedRequest
{
    public readonly float duration;

    public TimerRequest(float duration)
    {
        this.duration = duration;
    }
    
}
