using MantaMiddleware.Events;
using UnityEngine;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;


public class ObjectSpawner : MonoBehaviour, IRequestCastListener<SpawnObjectRequest, bool>
{
    private void Awake()
    {
        EventCaster.Main.Subscribe(this);
    }

    public async UniTask<bool> Handle(SpawnObjectRequest evt)
    {
        await Addressables.InstantiateAsync(evt.Prefab, Vector3.zero, Quaternion.identity);
        return true;
    }

    async UniTask IEventCastListener<SpawnObjectRequest>.Handle(SpawnObjectRequest evt)
    {
        await Handle(evt);
    }
}