using UnityEngine;
using UnityEngine.AddressableAssets;

public class SpawnObjectWithPositionRequest : SpawnObjectRequest
{
    public Vector3 Position => _position;
    private readonly Vector3 _position;
    public SpawnObjectWithPositionRequest(AssetReferenceGameObject prefab, Vector3 position) : base(prefab)
    {
        _position = position;

    }
}