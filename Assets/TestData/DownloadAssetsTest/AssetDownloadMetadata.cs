using System;
using MantaMiddleware.DataView;
using MantaMiddleware.Events;
using UnityEngine;

[Serializable]
public class AssetDownloadMetadata : IProgressWatcher
{
    [DataViewProp] public string Name;
    [DataViewProp] public string AssetUrl;
    [DataViewProp] public float DownloadProgress = 0f;
    [DataViewProp] public bool IsDownloaded => DownloadProgress >= 1f;
    [DataViewProp] public bool IsDownloading => DownloadProgress > 0f && DownloadProgress < 1f;
    [DataViewProp] public bool CanBeDownloaded => DownloadProgress <= 0f;
    [DataViewProp] public Sprite Icon;

    [DataViewProp]
    public Color IconColor
    {
        get
        {
            if (IsDownloading) return Color.green;
            if (IsDownloaded) return Color.white;
            return Color.gray;
        }
    }

    [DataViewProp] 
    public async void DownloadMe()
    {
        var trackableEvent = new DownloadViaButtonRequest(this);
        var castTask = EventCaster.CastGloballyWithResult<DownloadViaButtonRequest, Texture2D>(trackableEvent);
        trackableEvent.WatchProgress(this);
        await castTask;
    }

    public void SetProgress(float prog)
    {
        DownloadProgress = prog;
    }
}