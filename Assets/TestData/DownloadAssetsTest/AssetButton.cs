using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.DataView;
using MantaMiddleware.Utilities;
using UnityEngine;

public class AssetButton : MonoBehaviour, IDisplayer<AssetDownloadMetadata>, IDataProvider, ISampleDataProvider
{
    private Func<UniTask> _updateMethod;
    private AssetDownloadMetadata target;

    public async UniTask SetToDisplay(AssetDownloadMetadata target)
    {
//        Debug.Log($"Updating button with data; progress = {target.DownloadProgress}");
        this.target = target;
        if (_updateMethod != null) 
            await _updateMethod.Invoke();
    }

    public async UniTask RegisterDataUpdateTrigger(Func<UniTask> updateMethod)
    {
        _updateMethod = updateMethod;
        if (this.target != null) await _updateMethod.Invoke();
    }

    public object GetData()
    {
        return target;
    }

    public IEnumerable<string> GetSampleDataSetNames()
    {
        yield return "Undownloaded";
        yield return "Partial";
        yield return "Downloaded";
    }

    public object GetSampleDataSet(string dataSetName)
    {
        switch (dataSetName)
        {
            case "Undownloaded":
                return new AssetDownloadMetadata()
                {
                    Name = "Undownloaded",
                    AssetUrl = "123",
                    DownloadProgress = 0f
                };
            case "Partial":
                return new AssetDownloadMetadata()
                {
                    Name = "Partial",
                    AssetUrl = "123",
                    DownloadProgress = 0.3f
                };
            case "Downloaded":    
                return new AssetDownloadMetadata()
                {
                    Name = "Downloaded",
                    AssetUrl = "123",
                    DownloadProgress = 1f
                };
            default:
                return new AssetDownloadMetadata()
                {
                    Name = "Default",
                    AssetUrl = "123",
                    DownloadProgress = 0f
                };
        }
    }

    public IEnumerable<Type> GetDataSourceTypes()
    {
        yield return typeof(AssetDownloadMetadata);
    }
}
