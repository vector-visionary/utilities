using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using MantaMiddleware.Utilities;
using UnityEngine;
using UnityEngine.Networking;

public class DownloadAssetsPanel : MonoBehaviour, IRequestCastListener<DownloadViaButtonRequest, Texture2D>
{
    public AssetButton ButtonPrefab;
    public RectTransform ContentParent;

    public AssetDownloadMetadata[] URLs;
    private MatchedDisplayerSet<AssetDownloadMetadata> buttonDisplayers;

    // Start is called before the first frame update
    void Start()
    {
        buttonDisplayers = new MatchedDisplayerSet<AssetDownloadMetadata>(
            async () =>
            {
                return await ObjectPool.Instantiate(ButtonPrefab, ContentParent);
            }, async (obj) =>
            {
                await ObjectPool.Destroy((obj as Component).gameObject);
            });
        buttonDisplayers.setTransformIndexBasedOnListOrder = true;
        buttonDisplayers.SetToMatch(URLs).Forget();

        EventCaster.Main.Subscribe(this);
    }


    public async UniTask<Texture2D> Handle(DownloadViaButtonRequest evt)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(evt.Data.AssetUrl);
        request.SendWebRequest();
        while (!request.isDone)
        {
            await UniTask.DelayFrame(1);
            evt.MarkUsageState(this, new SimpleProgressState(request.downloadProgress));
            await buttonDisplayers.UpdateSingle(evt.Data);
        }

        Texture2D rtn = null;
        if (request.result == UnityWebRequest.Result.Success)
        {
            Texture.allowThreadedTextureCreation = true;
            rtn = ((DownloadHandlerTexture)request.downloadHandler).texture;
            Debug.Log($"Success! {rtn.width} x {rtn.height}");
            evt.Data.Icon = Sprite.Create(rtn, new Rect(0, 0, rtn.width, rtn.height), Vector2.zero, 1f, 0, SpriteMeshType.FullRect);
        }
        else
        {
            Debug.Log($"Result: {request.result} {request.error}");
        }
        evt.MarkUsageState(this, new SimpleProgressState(1f));
        await buttonDisplayers.UpdateSingle(evt.Data);
        return rtn;
    }

    async UniTask IEventCastListener<DownloadViaButtonRequest>.Handle(DownloadViaButtonRequest evt)
    {
        await Handle(evt);
    }
}

public class DownloadViaButtonRequest : TrackedCastableRequestBase<Texture2D>
{
    public AssetDownloadMetadata Data { get; }

    public DownloadViaButtonRequest(AssetDownloadMetadata data)
    {
        Data = data;
    }
}
