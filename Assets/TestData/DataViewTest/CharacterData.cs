using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.DataView;
using UnitsNet;
using UnityEngine;

[System.Serializable]
public class CharacterData
{
    [DataViewProp("Bio", "Name")] public string Name;
    [DataViewProp("Bio", "Age")] public int Age;

    [DataViewProp("XP")] public int xp;

    [DataViewProp("Bio", "Height")] public Length height;

    [DataViewProp("Happiness")] [RangeDecoration(0f, 100f)] public float happiness;
    [DataViewProp("Unranged")] public float unranged = 1f;

    [DataViewPropGroup("Attributes")] public Dictionary<string, int> attributes;

    [DataViewProp("Add XP", RefreshFullDataViewAfterSet = true)]
    public async UniTask AddXP()
    {
        xp += 5;
        Debug.Log($"Waiting... {Time.time}");
        await UniTask.DelayFrame(400);
        Debug.Log($"Waited {Time.time}");
    }

    [DataViewProp("Show Attributes")]
    public bool doShowAttributeBox = false;

    [DataViewProp("Show XP Button")]
    public bool addXPButtonEnabled = false;

    [DataViewPostChangeAction]
    public void LogChange()
    {
        Debug.Log($"After change, xp is {xp}");
    }
}
