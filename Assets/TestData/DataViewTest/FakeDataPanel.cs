using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.DataView;
using UnityEngine;

public class FakeDataPanel : MonoBehaviour, IDataProvider, ISampleDataProvider
{
    private Func<UniTask> updateDataFunc;
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) updateDataFunc();
    }


    public IEnumerable<string> GetSampleDataSetNames()
    {
        yield return "Default";
        yield return "Twenty";
        yield return "Inherited";
    }

    public object GetSampleDataSet(string dataSetName)
    {
        switch (dataSetName)
        {
            case "Default":
                return new FakeDataSource();
            case "Twenty":
                return new FakeDataSource() { fakeFloatValue = 20f };
            case "Inherited":
                return new InheritSource() { fakeFloatValue = 20f };
        }

        return new FakeDataSource();
    }

    public object GetData()
    {
        return new InheritSource();
    }

    public IEnumerable<Type> GetDataSourceTypes()
    {
        yield return typeof(FakeDataSource);
        yield return typeof(InheritSource);
    }

    public UniTask RegisterDataUpdateTrigger(Func<UniTask> updateMethod)
    {
        updateDataFunc = updateMethod;
        return UniTask.CompletedTask;
    }
}
