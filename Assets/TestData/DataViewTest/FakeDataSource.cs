using System;
using System.Collections;
using System.Collections.Generic;
using MantaMiddleware.DataView;
using UnityEngine;

public class FakeDataSource
{
    [DataViewProp("FakeFloat")]
    public float fakeFloatValue = 5f;

    [DataViewProp("GameTime")] public float gameTime => Time.time;
}

public class InheritSource : FakeDataSource
{
    [DataViewProp("NewInt")] public int newInt = 12;
    [DataViewProp("DoShow")] public bool doShow = true;
}