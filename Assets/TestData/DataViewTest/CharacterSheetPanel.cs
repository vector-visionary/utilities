using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.DataView;
using UnityEngine;

public class CharacterSheetPanel : MonoBehaviour, IDataProvider, ISampleDataProvider
{
    [SerializeField] private CharacterData[] allData;
    private Func<UniTask> updateData;
    public UniTask RegisterDataUpdateTrigger(Func<UniTask> updateMethod)
    {
        updateData = updateMethod;
        return UniTask.CompletedTask;
    }

    private IEnumerator Start()
    {
        return UpdateDataCycle().ToCoroutine();
    }

    private async UniTask UpdateDataCycle() {
        while (true)
        {
            Debug.Log($"Updating Data at {Time.time}");
            if (updateData != null) await updateData();
            
            await UniTask.DelayFrame(5);
        }
    }

    public object GetData()
    {
        return allData[0];
    }

    public IEnumerable<string> GetSampleDataSetNames()
    {
        if (allData != null)
        {
            foreach (var character in allData)
            {
                yield return character.Name;
            }
        }
    }

    public object GetSampleDataSet(string dataSetName)
    {
        foreach (var character in allData)
        {
            if (character.Name == dataSetName) return character;
        }

        return null;
    }

    public IEnumerable<Type> GetDataSourceTypes()
    {
        yield return typeof(CharacterData);
    }
}
