﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using MantaMiddleware.Utilities;
using UnityEngine.Serialization;

namespace MantaMiddleware.SceneControl
{
	public class GameLevelScene<T> : SceneDataController<T, GameLevelSceneData> where T : MonoBehaviour
	{
		protected override void OnDrawGizmos()
		{
			base.OnDrawGizmos();
			if (SceneData.playerStartPosition != null) Gizmos.DrawWireSphere(SceneData.playerStartPosition.position, 1f);
		}
		protected override async UniTask OnStart()
		{
			await base.OnStart();
			SceneManager.SetActiveScene(gameObject.scene);
		}

		public override bool ArePrerequisitesForStartingSceneMet()
		{
			return true; // TODO: replace CoreGameplayScene.Manager != null;
		}
	}

	[Serializable]
	public struct GameLevelSceneData
	{
		public Transform playerStartPosition;
	}
}