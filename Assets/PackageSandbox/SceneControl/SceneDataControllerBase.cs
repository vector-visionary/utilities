using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;

namespace MantaMiddleware.SceneControl
{
    public class SceneDataControllerBase : MonoBehaviour
    {
        private static Dictionary<Scene, SceneDataControllerBase> sceneLookup = new Dictionary<Scene, SceneDataControllerBase>();

        public static void LoadSceneByName(string sceneName)
        {
            // TODO: This will have to be able to load from other assemblies
            Type t = typeof(SceneDataControllerBase).Assembly.GetType($"MantaMiddleware.SceneControl.{sceneName}");
            if (t == null) Debug.LogError($"Couldn't find a class named {sceneName}");

            var method = t.GetMethod("LoadScene");
            while (method == null && t != null)
            {
                t = t.BaseType;
                if (t != null) method = t.GetMethod("LoadScene");
            }
            if (method == null) Debug.LogError($"Class {t} or its base classes did not have a LoadScene method.");
            method.Invoke(null, null);

        }

        private static HashSet<SceneDataControllerBase> scenesWithBounds = new HashSet<SceneDataControllerBase>();
        protected virtual void Awake()
        {
            director = GetComponent<PlayableDirector>();
            director.stopped += CompleteTimeline;
            if (storyCamsParent != null) storyCamsParent.SetActive(false);

        }

        private void Start()
        {
            if (!ArePrerequisitesForStartingSceneMet())
            {
                Debug.Log($"Unloading scene {GetType()} because its prerequisites are not met.");
                SceneManager.UnloadSceneAsync(GetScene());
                gameObject.SetActive(false);
            }
            else
            {
                OnStart().Forget();
            }
        }

        protected virtual async UniTask OnStart()
        {
        }

        protected virtual void OnValidate()
        {
            director = GetComponent<PlayableDirector>();
            if (director == null) director = gameObject.AddComponent<PlayableDirector>();
            director.playOnAwake = false;
        }

        public GameObject storyCamsParent;
        private PlayableDirector director;

        public async UniTask PlayTimelineAsset(TimelineAsset timeline)
        {
            if (director == null)
                director = GetComponent<PlayableDirector>();

            bool didComplete = false;
            Action<PlayableDirector> onCompleteTimeline = (dir) =>
            {
                didComplete = true;
            };
            director.stopped += onCompleteTimeline;
            director.Play();
            while (!didComplete)
            {
                await UniTask.DelayFrame(1);
            }

            director.stopped -= onCompleteTimeline;
            EventCaster.CastGlobally(new DirectorTimelineEvent(director, timeline, DirectorTimelineEvent.StateType.PrepareToPlay));
            storyCamsParent?.SetActive(true);
        }
        public void SkipTimeline()
        {
            //TODO: bring in StoryEventTimelineMarker from Nightbear?
            /*
			var timeline = director.playableAsset as TimelineAsset;
			if (timeline != null)
			{
				var markers = timeline.markerTrack.GetMarkers();
				foreach (var marker in markers)
				{
					if (marker.time > director.time && marker is StoryEventTimelineMarker setm)
					{
						setm.thisEvent.BeginEvent(this);
					}
				}
				director.Stop();
				//director.stopped event will continue this down below
			}
			*/
        }

        public void CompleteTimeline(PlayableDirector director)
        {
            storyCamsParent?.SetActive(false);
        }



        protected virtual void OnEnable()
        {
            sceneLookup.Add(gameObject.scene, this);
            if (sceneBounds.size.sqrMagnitude > 0.001f) scenesWithBounds.Add(this);
        }
        protected virtual void OnDisable()
        {
            sceneLookup.Remove(gameObject.scene);
            if (scenesWithBounds.Contains(this)) scenesWithBounds.Remove(this);
        }
        [SerializeField]
        protected Bounds sceneBounds = new Bounds(); //leave at 0 for "not a level" scenes
        protected virtual void OnDrawGizmosSelected()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(sceneBounds.center, sceneBounds.extents);
        }

        public static SceneDataControllerBase Get(Scene scene)
        {
            if (sceneLookup.TryGetValue(scene, out var sceneData))
            {
                return sceneData;
            }
            return null;
        }
        public static SceneDataControllerBase Get(Component target)
        {
            return Get(target.gameObject.scene);
        }

        public Scene GetScene()
        {
            return gameObject.scene;
        }

        public static bool IsPositionInAnyScene(Vector3 position)
        {
            foreach (var scdb in scenesWithBounds)
            {
                if (scdb.sceneBounds.Contains(scdb.transform.InverseTransformPoint(position))) return true;
            }
            return false;
        }
        public static SceneDataControllerBase[] GetAllForPosition(Vector3 position) {
            List<SceneDataControllerBase> rtn = new List<SceneDataControllerBase>();
            foreach (var scdb in scenesWithBounds)
            {
                if (scdb.sceneBounds.Contains(scdb.transform.InverseTransformPoint(position))) rtn.Add(scdb);
            }
            return rtn.ToArray();
        }

        public static async UniTask UnloadAllScenesInheritingFrom<T>() where T : SceneDataControllerBase
        {
            var allUnloadTasks = new HashSet<UniTask>();
            foreach (var sceneKVP in sceneLookup)
            {
                if (sceneKVP.Value is T)
                {
                    Func<UniTask> task = async () => { await SceneManager.UnloadSceneAsync(sceneKVP.Value.GetType().Name); };
                    allUnloadTasks.Add(task());
                }
            }

            await UniTask.WhenAll(allUnloadTasks);
        }

        public virtual bool ArePrerequisitesForStartingSceneMet()
        {
            return true;
        }
    }
}