﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;

namespace MantaMiddleware.SceneControl.MantaMiddleware.SceneControl
{
	public class SceneDataControllerEditor
	{
		[MenuItem("Assets/Scenes/New Controller from Scene", validate = false)]
		public static void CreateNewSceneController()
		{
			var selection = Selection.activeObject;
			if (selection != null && selection is SceneAsset selectedScene)
			{
				var path = AssetDatabase.GetAssetPath(selectedScene);
				var folderPath = path.Substring(0, path.LastIndexOfAny("/\\".ToCharArray()));
				CreateSceneControllerFromBaseType(folderPath, selectedScene, "SceneDataController");
			}
		}

		private static void CreateSceneControllerFromBaseType(string folderPath, SceneAsset scene, string baseType)
		{
			string className = scene.name;
			className = className.Replace(" ", "");
			if (className != scene.name)
			{
				var oldPath = AssetDatabase.GetAssetPath(scene);
				AssetDatabase.RenameAsset(oldPath, $"{className}.unity");
			}
			string newClassContents = $"using UnityEngine;\n" +
				$"using MantaMiddleware.SceneControl;\n" +
				$"\n" +
				$"namespace YourNamespace.SceneControl {{\n" +
				$"    public class {className} : {baseType}<{className}>\n" +
				$"    {{\n" +
				$"    }}\n" +
				$"}}\n";
			File.WriteAllText($"{folderPath}/{className}.cs", newClassContents);
			AssetDatabase.Refresh();
			var edScenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
			bool sceneIsInBuildSettings = false;
			string newScenePath = $"{folderPath}/{scene.name}.unity";
			foreach (var sc in edScenes)
			{
				if (sc.path == newScenePath)
				{
					sc.enabled = true;
					sceneIsInBuildSettings = true;
					break;
				}
			}
			if (!sceneIsInBuildSettings)
			{
				edScenes.Add(new EditorBuildSettingsScene(newScenePath, true));
				EditorBuildSettings.scenes = edScenes.ToArray();
			}
		}

		[MenuItem("Assets/Scenes/New Controller from Scene", validate = true)]
		public static bool CanCreateNewSceneController()
		{
			var sceneFile = Selection.activeObject;
			if (sceneFile != null)
			{
				return sceneFile is SceneAsset;
			}
			return false;
		}

		[MenuItem("Assets/Scenes/New Game Scene Controller from Scene", validate = false)]
		public static void CreateNewGameSceneController()
		{
			var selection = Selection.activeObject;
			if (selection != null && selection is SceneAsset selectedScene)
			{
				var path = AssetDatabase.GetAssetPath(selectedScene);
				var folderPath = path.Substring(0, path.LastIndexOfAny("/\\".ToCharArray()));
				CreateSceneControllerFromBaseType(folderPath, selectedScene, "GameLevelScene");
			}
		}

		[MenuItem("Assets/Scenes/New Game Scene Controller from Scene", validate = true)]
		public static bool CanCreateNewGameSceneController()
		{
			var sceneFile = Selection.activeObject;
			if (sceneFile != null)
			{
				return sceneFile is SceneAsset;
			}
			return false;
		}

	}
}