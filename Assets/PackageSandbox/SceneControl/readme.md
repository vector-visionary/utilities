# SceneManager

SceneManager is a helper class to assist with holding scene-level data.

There must be one class for each scene managed by this system, and that class name must match the scene name. This class must derive from SceneDataController, where T is a class or struct holding the main data for that scene.

When the scene is loaded, it will broadcast the event **OnSceneStartCastableRequest**, with that scene data object as a parameter.

It is recommended to take advantage of inheritance for these, and create mid-level classes for the types of scenes you'll have in your game. The included GameLevelSceneData is an example of such a mid-level scene, and the individual game levels would derive from that class. It contains a struct holding the player's start position. Your player prefab or player spawner might respond to the OnSceneStartCastableRequest in order to place the player at this start position. The scene data could be modified to include anything else relevant to your game, such as a goal position, enemy spawn points, etc.