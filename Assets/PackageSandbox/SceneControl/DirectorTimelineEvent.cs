using MantaMiddleware.Events;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace MantaMiddleware.SceneControl
{
    public class DirectorTimelineEvent : CastableEventBase
    {
        public TimelineAsset TimelineAsset { get; }
        public PlayableDirector Director { get; }
        public enum StateType { PrepareToPlay, Started, Stopped }

        public StateType type { get; }

        public DirectorTimelineEvent(PlayableDirector director, TimelineAsset timelineAsset, StateType type)
        {
            TimelineAsset = timelineAsset;
            Director = director;
            this.type = type;
        }
    }
}