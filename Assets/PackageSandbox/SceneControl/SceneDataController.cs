﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;
using System.Reflection;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using UnityEngine.Serialization;

namespace MantaMiddleware.SceneControl
{
	[RequireComponent(typeof(PlayableDirector))]
	public abstract class SceneDataController<TSelf, TSceneData> : SceneDataControllerBase where TSelf : MonoBehaviour
	{
		public TSceneData SceneData;

		private static bool expectManagerToBeLoaded = false;
		public static async UniTask LoadScene()
		{
			if (Manager != null) return;
			if (expectManagerToBeLoaded)
			{
				Debug.LogError($"{typeof(TSelf).Name}.LoadScene expected the manager to exist (the scene has been loaded) but it does not. " +
					$"\nMost likely this manager needs to be added into the scene, or the scene has been unloaded by another means.");
			}
			await SceneManager.LoadSceneAsync(typeof(TSelf).Name, LoadSceneMode.Additive);
			expectManagerToBeLoaded = true;
		}

		public static Scene Scene
		{
			get
			{
				return SceneManager.GetSceneByName(typeof(TSelf).Name);
			}
		}
		public static void UnloadScene()
		{
			if (Manager != null)
				SceneManager.UnloadSceneAsync(typeof(TSelf).Name);
			expectManagerToBeLoaded = false;
		}

		public static TSelf Manager
		{
			get {
				if (_manager == null)
				{
					_manager = UnityEngine.Object.FindFirstObjectByType<TSelf>();
				}
				return _manager;
			}
		}
		private static TSelf _manager;

		protected override async UniTask OnStart()
		{
			var result = await EventCaster.CastGloballyWithResult<OnSceneStartCastableRequest<TSceneData>, bool>(new OnSceneStartCastableRequest<TSceneData>(SceneData, sceneBounds));
			await base.OnStart();
		}

		protected virtual void OnDrawGizmos()
		{

		}

	}
}