using MantaMiddleware.Events;
using UnityEngine;

namespace MantaMiddleware.SceneControl
{
    public class OnSceneStartCastableRequest<SceneDataT> : CastableRequestBase<bool>
    {
        public SceneDataT GameLevelScene { get; }
        public Bounds bounds;

        public OnSceneStartCastableRequest(SceneDataT gameLevelScene, Bounds bounds)
        {
            this.bounds = bounds;
            GameLevelScene = gameLevelScene;
        }
    }
}