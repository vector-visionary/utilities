﻿using System;
using UnityEngine;

namespace MantaMiddleware.Audio
{
	public class AudioClipSegment : ScriptableObject
	{
		public AudioClip mainClip;

		[Range(0, 255)] public int priority = 128;
		public float StartTime
		{
			get => startTime >= 0f ? startTime : 0f;
			set => startTime = value;
		}
		[SerializeField] public float startTime = -1f;
		public float EndTime
		{
			get => endTime >= 0f ? endTime : mainClip.length;
			set => endTime = value;
		}
		[SerializeField] public float endTime = -1f; 
		public float Duration => EndTime - StartTime;


		[SerializeField] private AnimationCurve volumeCurve = new AnimationCurve(new Keyframe(0f, 1f), new Keyframe(1f, 1f));

		public float GetVolume(float time)
		{
			if (!IsTimeInSegment(time)) return 0f;
			float normTimeInClip = (time - StartTime) / Duration;
			return volumeCurve.Evaluate(normTimeInClip);
		}

		public float pitchRandomization = 0f;
		public int pitchStepOffset = 0;

		public bool loop = false;

		public bool IsTimeInSegment(float time) => time > StartTime && time < EndTime;
	}
}