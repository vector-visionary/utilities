using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Reflection;
using System;

namespace MantaMiddleware.Audio
{
	public class AudioClipSegmentEditor : EditorWindow
	{
		[MenuItem("Assets/Create Audio Segments", validate = true)]
		private static bool CanCreateSegments()
		{
			return Selection.activeObject is AudioClip;
		}

		[MenuItem("Assets/Create Audio Segments", validate = false)]
		private static void CreateSegments()
		{
			var win = EditorWindow.GetWindow<AudioClipSegmentEditor>();
			
		}

		private AudioClip baseClip;
		private AudioClipSegment selectedSegment;
		string newSegName = "New Segment";

		private void OnInspectorUpdate()
		{
			var selectedClip = Selection.activeObject as AudioClip;
			if (AudioUtility.IsClipPlaying() && (selectedClip == null || selectedClip == baseClip)) // there's seriously no way to tell WHAT clip is playing??
			{
				if (selectedSegment == null)
				{
					AudioUtility.StopAllClips();
				}
				else if (AudioUtility.GetClipPosition() > selectedSegment.EndTime)
				{
					if (selectedSegment.loop)
					{
						AudioUtility.SetClipPosition(baseClip, AudioUtility.GetClipPosition() - selectedSegment.Duration);
					}
					else
					{
						AudioUtility.StopAllClips();
					}
				}
				Repaint();
			}
		}

		private void OnGUI()
		{
			if (baseClip == null)
			{
				baseClip = Selection.activeObject as AudioClip;
				selectedSegment = null;
			}
			if (baseClip == null)
			{
				selectedSegment = Selection.activeObject as AudioClipSegment;
				baseClip = selectedSegment.mainClip;
			}
			if (baseClip == null)
			{
				GUILayout.Label("Window has no audio clip");
				return;
			}
			if (Selection.activeObject is AudioClipSegment seg)
			{
				selectedSegment = seg;
			}

			string path = AssetDatabase.GetAssetPath(baseClip);
			AudioClipSegment[] segments = GetSegmentsForFile(path);
			GUILayoutWaveform(GUILayoutUtility.GetRect(50f, 50f, GUILayout.ExpandWidth(true)), baseClip, 0f, baseClip.length, segments, selectedSegment);
			if (selectedSegment != null)
			{
				GUI.changed = false;
				EditorGUILayout.MinMaxSlider(ref selectedSegment.startTime, ref selectedSegment.endTime, 0f, baseClip.length);

				if (GUI.changed) EditorUtility.SetDirty(selectedSegment); 

				bool isPlaying = AudioUtility.IsClipPlaying();
				if (!isPlaying && GUILayout.Button("Play")) {
					AudioUtility.PlayClipAtTime(baseClip, selectedSegment.StartTime);
				}
				if (isPlaying && GUILayout.Button("Stop"))
				{
					AudioUtility.StopAllClips();
				}
			}

			foreach (var segment in segments)
			{
				GUILayout.BeginHorizontal();
				GUI.color = (selectedSegment == segment ? Color.white : Color.gray);
				if (GUILayout.Button(segment.name))
				{
					selectedSegment = segment;
					Selection.activeObject = segment;
				}
				GUILayout.Label(new GUIContent($"{segment.mainClip.name}@{segment.StartTime}-{segment.EndTime}"));
				GUILayout.EndHorizontal();
			}
			GUI.color = Color.white;
			newSegName = GUILayout.TextField(newSegName);
			if (GUILayout.Button("New clip"))
			{
				int i = path.LastIndexOfAny("/\\".ToCharArray());
				int j = path.LastIndexOf('.');
				var folder = path.Substring(0, i);
				var baseFilename = path.Substring(i + 1, j - i - 1);
				var newSeg = new AudioClipSegment();
				newSeg.mainClip = baseClip;
				newSeg.startTime = 0f;
				newSeg.endTime = baseClip.length; 
				AssetDatabase.CreateAsset(newSeg, $"{folder}/{baseFilename}_{newSegName}.asset");
			}
			OnInspectorUpdate(); // we call this here so we get an instant refresh, smooth play head movement, and accurate stop time
		}

		public AudioClipSegment[] GetSegmentsForFile(string path)
		{
			int i = path.LastIndexOfAny("/\\".ToCharArray());
			int j = path.LastIndexOf('.');
			var folder = path.Substring(0, i);
			var baseFilename = path.Substring(i+1, j-i-1);
			var files = Directory.EnumerateFiles(folder, $"{baseFilename}_*.asset", SearchOption.TopDirectoryOnly);
			var rtn = new List<AudioClipSegment>();
			foreach (var file in files)
			{
				var asset = AssetDatabase.LoadAssetAtPath<AudioClipSegment>(file);
				if (asset != null) rtn.Add(asset);
			}
			return rtn.ToArray();
		}

		public void GUILayoutWaveform(Rect rect, AudioClip clip, float startTime, float endTime, AudioClipSegment[] allSegments, AudioClipSegment highlightedSegment)
		{
			if (rect.height <= 1 || rect.width <= 1) return;

			var tex = PaintWaveformSpectrum(clip, Mathf.RoundToInt(rect.width), Mathf.RoundToInt(rect.height), allSegments, highlightedSegment);
			GUI.DrawTexture(rect, tex, ScaleMode.ScaleToFit);

			var selectedClip = Selection.activeObject as AudioClip;
			if (AudioUtility.IsClipPlaying() && (selectedClip == null || selectedClip == baseClip)) // there's seriously no way to tell WHAT clip is playing??
			{
				float currentTime = AudioUtility.GetClipPosition();
				float xPos = currentTime * rect.width / clip.length + rect.xMin;
				GUI.Box(new Rect(xPos, rect.yMin, 2f, rect.height), "");
			}
		}

		public Color nonSegmentColor = Color.black;
		public Color waveformColor = Color.yellow;
		public Color selectedSegmentColor = new Color(.5f, .5f, .5f, 1f);
		public Color unselectedSegmentColorOffset = new Color(.15f, .15f, .15f, 1f);

		Color[] cachedColors;
		public Texture2D PaintWaveformSpectrum(AudioClip audio, int width, int height, AudioClipSegment[] allSegments, AudioClipSegment highlightedSegment)
		{
			if (cachedColors == null || cachedColors.Length != width * height) cachedColors = new Color[width * height];
			float[] samples = new float[audio.samples * audio.channels];
			float[] waveform = new float[width];
			float maxWaveform = 0f;
			audio.GetData(samples, 0);
			int packSize = (audio.samples * audio.channels / width) + 1;
			int s = 0;
			for (int i = 0; i < audio.samples * audio.channels; i += packSize)
			{
				waveform[s] = Mathf.Abs(samples[i]);
				if (waveform[s] > maxWaveform) maxWaveform = waveform[s];
				s++;
			}

			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					if (y <= waveform[x] / maxWaveform * (float)height)
					{
						cachedColors[y * width + x] = waveformColor;
					}
					else
					{
						Color baseEmptyColor = nonSegmentColor;
						float time = (float)x * audio.length / width;
						if (highlightedSegment != null && y < highlightedSegment.GetVolume(time) * (float)height )
						{
							baseEmptyColor = selectedSegmentColor;
						}
						foreach (var segment in allSegments)
						{
							if (segment != null && y < segment.GetVolume(time) * (float)height)
							{
								baseEmptyColor += unselectedSegmentColorOffset;
							}
						}

						cachedColors[y * width + x] = baseEmptyColor;
					}
				}
			}

			Texture2D tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
			tex.SetPixels(cachedColors);
			tex.Apply();

			return tex;
		}
	}
}