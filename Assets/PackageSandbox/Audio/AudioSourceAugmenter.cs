﻿using UnityEngine;

namespace MantaMiddleware.Audio
{
	public class AudioSourceAugmenter : MonoBehaviour
	{
		private AudioSource source;
		private void Awake()
		{
			source = GetComponent<AudioSource>();
		}
		public void Play(AudioClipSegment clip, int note)
		{
			myClip = clip;
			source.clip = clip.mainClip;
			source.time = clip.StartTime;
			float basePitch = Mathf.Pow(2f, (float)(clip.pitchStepOffset + note) / 12f);
			source.pitch = basePitch * Mathf.Pow(2f, (Random.value - 0.5f) * clip.pitchRandomization);
			source.volume = clip.GetVolume(source.time);
			source.priority = clip.priority;
			source.Play();
		}
		private AudioClipSegment myClip;

		private void Update()
		{
			if (source.time >= myClip.EndTime)
			{
				if (myClip.loop)
				{
					source.time -= myClip.Duration; // do it this way instead of just setting it in case we're trying to keep a beat
				}
				else
				{
					source.Stop();
					source.clip = null;
					Destroy(this);
				}
			}
			else
			{
				source.volume = myClip.GetVolume(source.time);
			}
		}
	}
}