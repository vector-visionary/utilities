﻿using UnityEngine;

namespace MantaMiddleware.Audio
{
	public static class AudioUtils
	{
		public static void PlayThemeSound(this Component component, string clipName)
		{
			CascadingSoundTheme.PlaySound(clipName, component);
		}
		public static void PlayOneShot(this AudioSource audioSource, AudioClipSegment clip)
		{
			Play(audioSource, clip, 0);
		}
		public static void PlayOneShotAsNote(this AudioSource audioSource, AudioClipSegment clip, int note)
		{
			Play(audioSource, clip, note);
		}
		public static void Play(this AudioSource audioSource, AudioClipSegment clip, int note)
		{
			var augmenter = audioSource.gameObject.GetComponent<AudioSourceAugmenter>();
			if (augmenter == null) augmenter = audioSource.gameObject.AddComponent<AudioSourceAugmenter>();
			augmenter.Play(clip, note);
		}

	}
}