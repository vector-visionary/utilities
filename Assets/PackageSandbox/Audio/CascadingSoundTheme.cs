using System;
using System.Collections;
using System.Collections.Generic;
using MantaMiddleware.Utilities;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace MantaMiddleware.Audio
{
	public class CascadingSoundTheme : MonoBehaviour
    {
		public static bool PlaySound(string soundName, Component component = null)
		{
			soundName = soundName.ToLower();
			if (main == null) main = FindObjectOfType<CascadingSoundTheme>();

			// first try the local sound theme
			if (component != null)
			{
				var themes = component.transform.GetComponentsAlongParentageInOrder<CascadingSoundTheme>();
				foreach (var theme in themes) { 
					if (theme.PlaySoundByName(soundName, component))
					{
						return true;
					}
				}
			}

			if (main != null)
			{
				main.PlaySoundByName(soundName, component);
			}

			return false;
		}

		public bool PlaySoundByName(string soundName, Component component)
		{
			if (localClipList.TryGetValue(soundName.ToLower(), out var list))
			{
				return PlayRandomSoundFromClipSet(list, component);
			}
			return false;
		}

		private static CascadingSoundTheme main;
		public static Dictionary<string, ClipList> localClipList = new Dictionary<string, ClipList>();


		public bool isMain = false;

		public ClipList[] allClips;
		public AudioSource audioSource
		{
			get
			{
				if (localAudioSource != null) return localAudioSource;
				if (main != null && main != this) return main.audioSource;
				return null;
			}
		}
		private AudioSource localAudioSource;

		private void Awake()
		{
			if (isMain) main = this;
			foreach (var clip in allClips)
			{
				localClipList.Add(clip.soundName.ToLower(), clip);
			}
		}
		private void Start()
		{
			localAudioSource = GetComponent<AudioSource>();
			if (localAudioSource == null && isMain) localAudioSource = gameObject.AddComponent<AudioSource>();
			var allButtons = GetComponentsInChildren<Button>();
			foreach (var button in allButtons)
			{
				button.onClick.AddListener(() => PlaySound("ButtonClick"));
			}
		}
		private bool PlayRandomSoundFromClipSet(ClipList list, Component component)
		{
			if (audioSource == null) return false; //someone's trying to play sounds during initialization, we can ignore that
			var snd = list.clips.RandomItem();
			if (snd != null)
			{
				audioSource.PlayOneShot(snd);
			}
			return snd != null;
		}

		[Serializable]
		public class ClipList
		{
			public string soundName;
			public AudioClipSegment[] clips;
		}
	}
}