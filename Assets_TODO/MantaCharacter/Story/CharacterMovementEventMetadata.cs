﻿using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.MantaCharacter.Intentions;
using MantaMiddleware.MantaCharacter.SceneData;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{

	[CreateAssetMenu(fileName ="New Character Movement", menuName = "MantaMiddleware/Story/Movement", order = 20)]
	public class CharacterMovementEventMetadata : LocatableObjectBasedEventMetadata<SceneTarget>
	{
		public string characterName;

		public float movementRate = 1f;

		protected override async UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			var character = GameCharacter.FindByName(characterName);
			if (character == null)
			{
				Debug.LogWarning($"Could not find character with name {characterName}; leaving story event");
				FinishEvent(originatingEventMetadata);
				return;
			}
			var movementIntention = character.GetComponentInChildren<MoveToMovementTargetIntention>();
			if (movementIntention == null)
			{
				Debug.LogWarning($"The character {characterName} does not have a MoveToMovementTargetIntention attached.");
				FinishEvent(originatingEventMetadata);
				return;
			}
			movementIntention.TargetID = locatableObjectID;
			movementIntention.rate = movementRate;
			movementIntention.OnReachTargetCallback += () =>
			{
				FinishEvent(originatingEventMetadata);
			};
		}
	}
}