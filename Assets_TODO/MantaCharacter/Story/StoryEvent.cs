﻿using MantaMiddleware.Utilities;
using Nightbear.GameFlow;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Nightbear.Story
{
	public abstract class StoryEvent : ScriptableObject
	{
		private static HashSet<StoryEvent> eventsThatShouldBeChainSkipped = new HashSet<StoryEvent>();
		public Action OnEventCompleted;
		public void BeginEvent(IStoryTrigger trigger, StoryEvent originatingEvent = null)
		{
			if (originatingEvent == null) originatingEvent = this;
			Debug.Log($"{GetType().Name}.<b>{nameof(BeginEvent)}</b>({trigger.name}, {originatingEvent.name})");
			CoreGameplayScene.BeginStoryEvent(this);
			this.activeTrigger = trigger;
			Invoke(originatingEvent);
			if (eventsThatShouldBeChainSkipped.Contains(this))
			{
				Debug.Log($"Chain skipping {name}");
				eventsThatShouldBeChainSkipped.Remove(this);
				TimeUtil.ExecuteAfterOneFrame(() =>
				{
					Debug.Log("Chain skip 2");
					SkipEvent();
				});
			}
		}
		protected IStoryTrigger activeTrigger;
		protected abstract void Invoke(StoryEvent originatingEvent);
		public void CancelEvent()
		{
			Debug.Log($"{GetType().Name}.<b>{nameof(CancelEvent)}</b>()");
			CoreGameplayScene.EndStoryEvent(this);
			OnEventCompleted?.Invoke();
			OnEventCompleted = null;
		}
		public void FinishEvent(StoryEvent originatingEvent)
		{
			if (activeTrigger != null && originatingEvent != null)
			{
				Debug.Log($"{GetType().Name}.<b>{nameof(FinishEvent)}</b>({activeTrigger.name}, {originatingEvent.name})");
			}
			else
			{
				Debug.Log($"{GetType().Name}.<b>{nameof(FinishEvent)}</b>(*, *)");
			}
			CoreGameplayScene.EndStoryEvent(this);
			OnEventCompleted?.Invoke();
			OnEventCompleted = null;
			if (nextElement != null) nextElement.BeginEvent(activeTrigger);
			else activeTrigger?.OnStoryChainCompleted(originatingEvent);
			this.activeTrigger = null;
		}

		public bool doesBlockPlayerInput = true;
		public bool doesBlockCombat = true;
		public bool doesHidePlayerUI = true;
		public bool canBeSkipped;
		public StoryEvent nextElement;

		public void SkipEvent()
		{
			if (nextElement !=null && nextElement.canBeSkipped) eventsThatShouldBeChainSkipped.Add(nextElement);
			DoSkipEventCleanup();
		}

		protected virtual void DoSkipEventCleanup() { }
	}

	public interface IStoryTrigger
	{
		string name { get; }

		Scene GetScene();
		void OnStoryChainCompleted(StoryEvent originatingStoryEvent);
	}
}