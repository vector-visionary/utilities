﻿using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{
	public class SpecialLandscapeBase : MonoBehaviour
	{
		protected Collider triggerCollider
		{
			get
			{
				if (_triggerCollider == null) _triggerCollider = GetComponent<Collider>();
				return _triggerCollider;
			}
		}
		private Collider _triggerCollider;

		protected virtual void Awake()
		{
			_triggerCollider = GetComponent<Collider>();
			if (triggerCollider != null)
			{
				triggerCollider.isTrigger = true;
			}
			gameObject.layer = 8;
		}
	}
}