﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.Timeline;

namespace MantaMiddleware.MantaCharacter.Story
{
	public class StoryEventTimelineMarker : Marker, INotification {
		public PropertyName id => new PropertyName();
        [FormerlySerializedAs("thisEvent")] public StoryEventMetadata thisEventMetadata;

	}

	public class StoryEventTimelineReceiver : INotificationReceiver, IStoryTrigger
	{
		public string name => throw new System.NotImplementedException();

		public Scene GetScene()
		{
			throw new System.NotImplementedException();
		}

		public void OnNotify(Playable origin, INotification notification, object context)
		{
			throw new System.NotImplementedException();
		}

		public void OnStoryChainCompleted(StoryEventMetadata originatingStoryEventMetadata)
		{
			throw new System.NotImplementedException();
		}
	}
}