﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{
	[CreateAssetMenu(fileName = "New Multitrigger Event", menuName = "MantaMiddleware/Story/MultiTrigger", order = 60)]
	public class MultiTriggerStoryEventMetadataStarter : StoryEventMetadata
	{
		public int requireUniqueTriggersCount = 0;
		public string[] requireAllSpecificTriggerNames;
		private HashSet<string> allNamesTriggered = new HashSet<string>();

		protected async override UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			if (allNamesTriggered.Contains(activeTrigger.name))
			{
				CancelEvent();
				return;
			}

			allNamesTriggered.Add(activeTrigger.name);
			foreach (var name in requireAllSpecificTriggerNames)
			{
				if (!allNamesTriggered.Contains(name))
				{
					CancelEvent();
					return;
				}
			}
			if (allNamesTriggered.Count < requireUniqueTriggersCount)
			{
				CancelEvent();
				return;
			}

			FinishEvent(originatingEventMetadata);
			nextElement.BeginEvent(activeTrigger, originatingEventMetadata);
			// reset so we can be triggered again later
			allNamesTriggered.Clear();
		}

		
	}
}