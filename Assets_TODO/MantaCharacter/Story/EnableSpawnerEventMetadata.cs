﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{
	[CreateAssetMenu(fileName = "New Enable Spawner Event", menuName = "MantaMiddleware/Story/EnableSpawner", order = 41)]
	public class EnableSpawnerEventMetadata : LocatableObjectBasedEventMetadata<EnemySpawner>
	{
		protected override UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			var target = TargetInScene;
			target.allowSpawning = true;
			FinishEvent(originatingEventMetadata);
		}

	}
}