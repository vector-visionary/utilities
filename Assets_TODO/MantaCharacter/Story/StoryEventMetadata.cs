﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using MantaMiddleware.LogUtilities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MantaMiddleware.MantaCharacter.Story
{
	public abstract class StoryEventMetadata : ScriptableObject
	{
		private static HashSet<StoryEventMetadata> eventsThatShouldBeChainSkipped = new HashSet<StoryEventMetadata>();
		public Action OnEventCompleted;
		public void BeginEvent(IStoryTrigger trigger, StoryEventMetadata originatingEventMetadata = null)
		{
			if (originatingEventMetadata == null) originatingEventMetadata = this;
			SelectiveLogger.Log(this, $"<b>{nameof(BeginEvent)}</b>({trigger.name}, {originatingEventMetadata.name})");
			EventCaster.CastGlobally(new StoryEventStateUpdate(this, StoryEventStateUpdate.StateType.Started));
			this.activeTrigger = trigger;
			Invoke(originatingEventMetadata);
			if (eventsThatShouldBeChainSkipped.Contains(this))
			{
				SelectiveLogger.Log(this, $"Chain skipping {name}");
				eventsThatShouldBeChainSkipped.Remove(this);
				UniTask.Void(async () =>
				{
					await UniTask.DelayFrame(1);
					SelectiveLogger.Log(this, "Chain skip 2");
					SkipEvent();
				});
			}
		}
		protected IStoryTrigger activeTrigger;
		protected abstract UniTask Invoke(StoryEventMetadata originatingEventMetadata);
		public void CancelEvent()
		{
			Debug.Log($"{GetType().Name}.<b>{nameof(CancelEvent)}</b>()");
			EventCaster.CastGlobally(new StoryEventStateUpdate(this, StoryEventStateUpdate.StateType.Cancelled));
			OnEventCompleted?.Invoke();
			OnEventCompleted = null;
		}
		public void FinishEvent(StoryEventMetadata originatingEventMetadata)
		{
			if (activeTrigger != null && originatingEventMetadata != null)
			{
				Debug.Log($"{GetType().Name}.<b>{nameof(FinishEvent)}</b>({activeTrigger.name}, {originatingEventMetadata.name})");
			}
			else
			{
				Debug.Log($"{GetType().Name}.<b>{nameof(FinishEvent)}</b>(*, *)");
			}
			EventCaster.CastGlobally(new StoryEventStateUpdate(this, StoryEventStateUpdate.StateType.Finished));
			OnEventCompleted?.Invoke();
			OnEventCompleted = null;
			if (nextElement != null) nextElement.BeginEvent(activeTrigger);
			else activeTrigger?.OnStoryChainCompleted(originatingEventMetadata);
			this.activeTrigger = null;
		}

		public bool doesBlockPlayerInput = true;
		public bool doesBlockCombat = true;
		public bool doesHidePlayerUI = true;
		public bool canBeSkipped;
		public StoryEventMetadata nextElement;

		public void SkipEvent()
		{
			if (nextElement !=null && nextElement.canBeSkipped) eventsThatShouldBeChainSkipped.Add(nextElement);
			DoSkipEventCleanup();
		}

		protected virtual void DoSkipEventCleanup() { }
	}

	public interface IStoryTrigger
	{
		string name { get; }

		Scene GetScene();
		void OnStoryChainCompleted(StoryEventMetadata originatingStoryEventMetadata);
	}
}