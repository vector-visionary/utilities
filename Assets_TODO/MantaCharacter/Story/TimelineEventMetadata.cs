﻿using Cysharp.Threading.Tasks;
using MantaMiddleware.SceneControl;
using UnityEngine;
using UnityEngine.Timeline;

namespace MantaMiddleware.MantaCharacter.Story
{
	[CreateAssetMenu(fileName = "New Timeline Element", menuName = "MantaMiddleware/Story/Timeline", order = 10)]
	public class TimelineEventMetadata : StoryEventMetadata
	{
		public TimelineAsset timeline;
		protected async override UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			var sdcb = SceneDataControllerBase.Get(activeTrigger.GetScene());
			await sdcb.PlayTimelineAsset(timeline);
			FinishEvent(originatingEventMetadata);
		}

		protected override void DoSkipEventCleanup()
		{
			var sdcb = SceneDataControllerBase.Get(activeTrigger.GetScene());
			sdcb.SkipTimeline();
		}
	}
}