using System.Collections;
using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.MantaCharacter.SceneData;
using MantaMiddleware.MantaCharacter.Story;
using UnityEngine;
using Nightbear.Characters;
using MantaMiddleware.Utilities.Pooling;
using Nightbear.Story;
using UnityEngine.SceneManagement;
using MantaMiddleware.Utilities;

namespace Nightbear.SceneElements
{
	public class EnemySpawner : SceneTarget, IStoryTrigger
    {
		public bool allowSpawning = true;
        public CombatCharacter enemyPrefab;
		[SerializeField] private float spawnInterval = 2f;
		[SerializeField] private int totalSpawnNumber = 10;
		[SerializeField] private int maxInSceneSimultaneously = 5;
		[SerializeField] private EnemySpawnerVolume[] spawningVolumes;
		[SerializeField] private bool onlySpawnIfVolumeClear = true;

		private float lastSpawned = -999f;
		private List<CombatCharacter> weSpawned = new List<CombatCharacter>();
		private int numSpawned = 0;

		public StoryEvent eventOnAllSpawned;
		public StoryEvent eventOnAllKilled;

		public bool allKilledEventWasTriggered = false;

		public Scene GetScene() => gameObject.scene;
		public void OnStoryChainCompleted(StoryEventMetadata originatingStoryEventMetadata)
		{
			
		}


		private void Update()
		{
			bool allowSpawnNow = allowSpawning && Time.time > lastSpawned + spawnInterval;
			if (maxInSceneSimultaneously > 0) allowSpawnNow = allowSpawnNow && weSpawned.Count < maxInSceneSimultaneously;
			if (totalSpawnNumber > 0) allowSpawnNow = allowSpawnNow && numSpawned < totalSpawnNumber;
			List<EnemySpawnerVolume> viableVolumes = new List<EnemySpawnerVolume>();
			if (onlySpawnIfVolumeClear && spawningVolumes.Length > 0)
			{
				foreach (var spawnerVol in spawningVolumes)
					if (spawnerVol.OverlappingCount == 0) viableVolumes.Add(spawnerVol);
				allowSpawnNow = allowSpawnNow && viableVolumes.Count > 0;
			}
			if (!onlySpawnIfVolumeClear)
			{
				foreach (var spawnerVol in spawningVolumes)
					viableVolumes.Add(spawnerVol);
			}

			if (allowSpawnNow)
			{
				Vector3 spawnPos = transform.position;
				Quaternion spawnRot = transform.rotation;
				if (viableVolumes.Count > 0)
				{
					var thisVol = viableVolumes.RandomItem();
					spawnPos = thisVol.GetRandomPointInVolume();
					spawnRot = thisVol.transform.rotation;
				}
				lastSpawned = Time.time;
				numSpawned++;
				var thisSpawn = ObjectPool.Instantiate(enemyPrefab, spawnPos, spawnRot);
				weSpawned.Add(thisSpawn);
				thisSpawn.OnDeath += () =>
				{
					weSpawned.Remove(thisSpawn);
				};
				if (numSpawned == totalSpawnNumber && eventOnAllSpawned != null) eventOnAllSpawned.BeginEvent(this);
			}

			if (weSpawned.Count == 0 && numSpawned >= totalSpawnNumber && !allKilledEventWasTriggered)
			{
				if (eventOnAllKilled != null) eventOnAllKilled.BeginEvent(this);
				allKilledEventWasTriggered = true;
			}
		}
	}
}