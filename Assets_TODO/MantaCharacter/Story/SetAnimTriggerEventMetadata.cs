﻿using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{
	[CreateAssetMenu(fileName = "New Character anim trigger", menuName = "MantaMiddleware/Story/Character Animation Trigger", order = 30)]
	public class SetAnimTriggerEventMetadata : StoryEventMetadata
	{
		public string characterName;
		public string triggerName;

		protected async override UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			var character = GameCharacter.FindByName(characterName);
			character.animator.SetTrigger(triggerName);
			FinishEvent(originatingEventMetadata);
		}
	}
}