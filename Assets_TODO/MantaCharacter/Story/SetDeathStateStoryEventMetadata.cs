﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{
	[CreateAssetMenu(fileName = "New Death State Event", menuName = "MantaMiddleware/Story/Death State", order = 80)]
	public class SetDeathStateStoryEventMetadata : StoryEventMetadata
	{
		public bool didDie = false;

		protected async override UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			// TODO?
			//EndCreditsScene.SetCharacterDeathState(didDie);
			FinishEvent(originatingEventMetadata);
		}
	}
}