﻿using System;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;
using UnityEngine.Events;

namespace MantaMiddleware.MantaCharacter.Story
{
	public class ShowDialogEvent : CastableRequestBase<int>
	{
		public DialogItem DialogItem;

		public ShowDialogEvent(DialogItem dialogItem)
		{
			DialogItem = dialogItem;
		}
	}
	
	[CreateAssetMenu(fileName = "New Dialog", menuName = "MantaMiddleware/Story/Dialog", order = 10)]
	public class DialogEventMetadata : StoryEventMetadata
	{
		public DialogItem[] dialogs;

		protected override async UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			for (int i = 0; i < dialogs.Length; i++)
			{
				await EventCaster.CastGloballyWithResult<ShowDialogEvent, int>(new ShowDialogEvent(dialogs[i]));
			}
			FinishEvent(originatingEventMetadata);
		}

	}

	[Serializable]
	public class DialogItem
	{
		public GameCharacter Speaker
		{
			get
			{
				if (speaker == null) speaker = GameCharacter.FindByName(findSpeakerByName);
				return speaker;
			}
		}
		[SerializeField] private GameCharacter speaker;
		public string findSpeakerByName;
		public string specialCameraPositionName;
		public string dialog;
		public UnityEvent onComplete;
	}
}