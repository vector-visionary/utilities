using MantaMiddleware.Events;

namespace MantaMiddleware.MantaCharacter.Story
{
    public class StoryEventStateUpdate : CastableEventBase
    {
        public StoryEventMetadata EventMetadata;
        public enum StateType { Started, Finished, Cancelled }

        public StateType type = StateType.Started;

        public StoryEventStateUpdate(StoryEventMetadata eventMetadata, StateType type)
        {
            EventMetadata = eventMetadata;
            this.type = type;
        }
    }
}