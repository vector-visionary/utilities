﻿using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.MantaCharacter.Intentions;
using MantaMiddleware.MantaCharacter.SceneData;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{

	[CreateAssetMenu(fileName = "New Character Spawning", menuName = "MantaMiddleware/Story/SpawnCharacter", order = 40)]
	public class CharacterSpawnEventMetadata : LocatableObjectBasedEventMetadata<SceneTarget>
	{
		public GameCharacter characterPrefab;

		protected override async UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			var target = TargetInScene;
			var character = ObjectPool.Instantiate(characterPrefab, target.transform.position, target.transform.rotation);
			FinishEvent(originatingEventMetadata);
		}
	}
}