﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{
	[CreateAssetMenu(fileName ="New Compound Event", menuName = "MantaMiddleware/Story/Compound", order = 40)]
	public class SimultaneousCompoundEventMetadata : StoryEventMetadata
	{
		public StoryEventMetadata[] childEvents;
		
		protected async override UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			bool[] completionStatus = new bool[childEvents.Length];
			for (int e=0; e<childEvents.Length;e++)
			{
				completionStatus[e] = false;
				int persistentE = e;
				childEvents[e].OnEventCompleted = () =>
				{
					completionStatus[persistentE] = true;
					foreach (var cs in completionStatus)
					{
						if (!cs) return;
					}
					FinishEvent(originatingEventMetadata);
				};
				childEvents[e].BeginEvent(activeTrigger, originatingEventMetadata);
			}
		}
	}
}