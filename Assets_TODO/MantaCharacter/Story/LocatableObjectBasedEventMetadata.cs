﻿using MantaMiddleware.MantaCharacter.Intentions;
using MantaMiddleware.MantaCharacter.SceneData;

namespace MantaMiddleware.MantaCharacter.Story
{
	public abstract class LocatableObjectBasedEventMetadata<T> : StoryEventMetadata where T : SceneTarget
	{

		protected virtual void OnValidate()
		{
		}

		public string locatableObjectID;
		protected T TargetInScene => SceneTarget.FindByID<T>(locatableObjectID);

	}
}