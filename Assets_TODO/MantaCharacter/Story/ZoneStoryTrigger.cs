﻿using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace MantaMiddleware.MantaCharacter.Story
{
	public class ZoneStoryTrigger : SpecialLandscapeBase, IStoryTrigger
	{
		[FormerlySerializedAs("thisEvent")] public StoryEventMetadata thisEventMetadata;
		public bool limitOnce = true;
		private bool hasTriggered = false;
		
		public enum TriggeredByType { MainCharacter, AnyCharacter, SpecificCharacter, SpecificFaction, AnyObject }
		public TriggeredByType triggeredBy = TriggeredByType.MainCharacter;
		public string triggererName = "";

		public void ResetTriggerState()
		{
			hasTriggered = false;
		}

		private void OnTriggerEnter(Collider other)
		{
			var foundCharacter = other.GetComponent<GameCharacter>();
			bool isTriggerValid = triggeredBy switch
			{
				TriggeredByType.AnyCharacter => foundCharacter != null,
				TriggeredByType.AnyObject => true,
				TriggeredByType.MainCharacter => foundCharacter == MainCharacter.Main,
				TriggeredByType.SpecificCharacter => foundCharacter != null && foundCharacter.dialogName == triggererName,
				TriggeredByType.SpecificFaction => foundCharacter is CombatCharacter cc && cc.factionName == triggererName,
				_ => false
			};
			if (!limitOnce || !hasTriggered)
			{
				hasTriggered = true;
				thisEventMetadata.BeginEvent(this);
			}
		}

		public Scene GetScene() => gameObject.scene;

		public void OnStoryChainCompleted(StoryEventMetadata originatingStoryEventMetadata)
		{
			
		}
	}
}