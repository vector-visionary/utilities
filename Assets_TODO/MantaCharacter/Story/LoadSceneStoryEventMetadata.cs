﻿using Cysharp.Threading.Tasks;
using MantaMiddleware.SceneControl;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Story
{
	[CreateAssetMenu(fileName = "New LoadSceneEvent", menuName = "MantaMiddleware/Story/Load Scene", order = 80)]
	public class LoadSceneStoryEventMetadata : StoryEventMetadata
	{
		public string sceneName = "";

		protected async override UniTask Invoke(StoryEventMetadata originatingEventMetadata)
		{
			SceneDataControllerBase.LoadSceneByName(sceneName);
			FinishEvent(originatingEventMetadata);
		}

	}
}