﻿namespace MantaMiddleware.MantaCharacter.Intentions
{
	public class PlayerSpecialAttackIntention : AttackIntention
	{
		public override void UpdateIntentionPriorityInfo()
		{
			// the attack is called directly elsewhere

			UpdateAnimator();
		}
	}

}