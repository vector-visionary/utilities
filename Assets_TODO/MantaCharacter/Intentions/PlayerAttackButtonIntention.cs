﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;
using UnityEngine.InputSystem;

namespace MantaMiddleware.MantaCharacter.Intentions
{
	public class PlayerAttackButtonIntention : AttackIntention
	{
		public InputActionProperty triggersOnInputAction;
		public float triggerAfterHold = 0f;
		public bool triggerOnButtonRelease = false;
		public bool triggerContinuouslyOnHold = false;
		
		public async override UniTask Handle(CharacterInitializationEvent evt)
		{
			await base.Handle(evt);
			triggersOnInputAction.action.Enable();
			priorityMultiplier = 0f;
			
		}

		public override IEnumerable<IntentionAvailabilitySetEvent.IntentionCategory> GetAvailabilityCategories()
		{
			foreach (var baseCat in base.GetAvailabilityCategories()) yield return baseCat;
			yield return IntentionAvailabilitySetEvent.IntentionCategory.PlayerInput;
		}

		private void OnDestroy()
		{
			triggersOnInputAction.action.Disable();
		}

		private float lastTimeButtonNotPressed = -999f;
		private bool wasPressed = false;
		private bool wasHeld = false;
		public override void UpdateIntentionPriorityInfo()
		{
			// collect our data
			float btnValue = triggersOnInputAction.action.ReadValue<float>();
			bool isPressed = btnValue > 0f;

			if (!isPressed) lastTimeButtonNotPressed = Time.time;
			bool isHeld = isPressed && Time.time >= lastTimeButtonNotPressed + triggerAfterHold;

			// apply our data based on our settings
			priorityMultiplier = 0f;
			if (triggerContinuouslyOnHold && isHeld)
			{
				priorityMultiplier = 1f;
			}
			else
			{
				if (triggerOnButtonRelease)
				{
					if (!isHeld && wasHeld)
					{
						priorityMultiplier = 1f;
					}
				}
				else
				{
					if (isHeld && !wasHeld)
					{
						priorityMultiplier = 1f;
					}
				}
			}

			UpdateAnimator();
			if (priorityMultiplier > 0f) shouldAttackWithHitbox.ArmAction(useActionInfo);

			//store these for next frame
			wasPressed = isPressed;
			wasHeld = isHeld;
		}

	}

}