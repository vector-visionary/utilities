﻿using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.MantaCharacter.Story;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Intentions
{
	public class InstantDeathVolume : SpecialLandscapeBase
	{
		private void OnTriggerEnter(Collider other)
		{
			var character = other.GetComponent<GameCharacter>();
			if (character != null) character.Death().Forget();
		}
	}
}