﻿using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.BaseScripts;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MantaMiddleware.MantaCharacter.Intentions
{

	public class WanderIntention : CharacterIntentionBase
	{
		public float intervalBetweenRandomization = 2f;
		private float lastRandomized = -9999f;
		

		public override void UpdateIntentionPriorityInfo()
		{
			priorityMultiplier = 1f;
			if (Time.time > lastRandomized + intervalBetweenRandomization)
			{
				lastRandomized = Time.time;
				movement = Random.onUnitSphere;
				bodyFacing = movement;
				headFacing = movement;
			}
		}

		public override void PreExecuteIntention()
		{
			
		}

		public override void PostExecuteIntention()
		{
			
		}

		public override IEnumerable<IntentionAvailabilitySetEvent.IntentionCategory> GetAvailabilityCategories()
		{
			yield return IntentionAvailabilitySetEvent.IntentionCategory.NoneSpecified;
		}
	}

}