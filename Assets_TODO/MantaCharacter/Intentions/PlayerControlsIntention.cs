﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace MantaMiddleware.MantaCharacter.Intentions
{
	public class PlayerControlsIntention<TInput> : CharacterIntentionBase where TInput : new()
	{
		public static TInput Inputs
		{
			get {
				if (inputs == null) inputs = new TInput();
				return inputs;
			}
		}
		
		public override IEnumerable<IntentionAvailabilitySetEvent.IntentionCategory> GetAvailabilityCategories()
		{
			yield return IntentionAvailabilitySetEvent.IntentionCategory.PlayerInput;
		}

		public async override UniTask Handle(CharacterInitializationEvent evt)
		{
			await base.Handle(evt);
			var specialAttackIntentions = character.GetComponentsInChildren<PlayerSpecialAttackIntention>();
			specialAttacks = new CharacterAction[specialAttackIntentions.Length];
			for (int a=0;a<specialAttackIntentions.Length;a++)
			{
				specialAttacks[a] = specialAttackIntentions[a].action;
			}

		}

		public CharacterAction[] specialAttacks;

		private static TInput inputs;

		public static event Action<PlayerControlsIntention<TInput>> OnSpecialAttackMenuOpen;
		public static event Action<PlayerControlsIntention<TInput>, InputAction.CallbackContext> OnSpecialAttackMenuSelectMoved;
		public static event Action<PlayerControlsIntention<TInput>> OnSpecialAttackMenuClose;

		[SerializeField] private Transform cameraTransform;
		[SerializeField] private Vector3 manualTargetFacingOffset = Vector3.zero;


		protected override void Awake()
		{
			base.Awake();
			inputs = new TInput();

			inputs.Common.Jump.started += Jump;
			inputs.Common.Jump.canceled += CancelJump;

			//inputs.Combat.LightHeavyAttack.started += StartAttack;
			//inputs.Combat.LightHeavyAttack.canceled += ReleaseAttack;
			/*
			 * Removing these features for the sake of time
			inputs.Combat.SpecialAttackPrepare.started += SpecialAttackPrepare_started;
			inputs.Combat.SpecialAttackPrepare.canceled += SpecialAttackPrepare_canceled;

			inputs.Combat.SpecialTargetPrepare.started += SpecialTargetPrepare_started;
			inputs.Combat.SpecialTargetPrepare.canceled += SpecialTargetPrepare_canceled;
			*/

			if (cameraTransform == null) cameraTransform = Camera.main.transform;
			Cursor.lockState = CursorLockMode.Locked;

		}

		protected override void OnEnable()
		{
			base.OnEnable();
			inputs.Enable();
		}
		protected override void OnDisable()
		{
			base.OnDisable();
			inputs.Disable();
		}


		private void SpecialTargetPrepare_started(InputAction.CallbackContext obj)
		{
			inputs.Combat.SpecialModeSelection.started += SpecialTargetSelectionMoved;
			character.targetingSystem.BeginTargetSelectionMode();
		}

		private void SpecialTargetSelectionMoved(InputAction.CallbackContext obj)
		{
			character.targetingSystem.MoveTargetSelection(InputToWorldSpace(obj.ReadValue<Vector2>()));
		}

		private void SpecialTargetPrepare_canceled(InputAction.CallbackContext obj)
		{
			inputs.Combat.SpecialModeSelection.started -= SpecialTargetSelectionMoved;
			character.targetingSystem.EndTargetSelectionMode();
		}

		private void SpecialAttackPrepare_started(InputAction.CallbackContext obj)
		{
			inputs.Combat.SpecialModeSelection.started += SpecialAttackSelectionMoved;
			OnSpecialAttackMenuOpen?.Invoke(this);
		}
		private void SpecialAttackSelectionMoved(InputAction.CallbackContext obj)
		{
			OnSpecialAttackMenuSelectMoved?.Invoke(this, obj);
		}
		private void SpecialAttackPrepare_canceled(InputAction.CallbackContext obj)
		{
			inputs.Combat.SpecialModeSelection.started -= SpecialAttackSelectionMoved;
			OnSpecialAttackMenuClose?.Invoke(this);
		}

		private void Jump(InputAction.CallbackContext obj)
		{
			jump = true;
		}

		private void CancelJump(InputAction.CallbackContext obj)
		{
			jump = false;
		}


		public override void UpdateIntentionPriorityInfo()
		{
			priorityMultiplier = 1f;

			if (character.targetingSystem.manuallySelectedTarget != null)
			{
				headFacing = (character.targetingSystem.manuallySelectedTarget.transform.position - transform.position) + manualTargetFacingOffset;
			}
			else
			{
				headFacing = cameraTransform.forward.FlattenAndRetainMagnitude();
			}

			Vector2 rawInput = inputs.Common.Move.ReadValue<Vector2>();
			float movementRate = (inputs.Common.Sprint.ReadValue<float>() + 1f) * rawInput.magnitude;
			if (rawInput != Vector2.zero)
			{
				//				Debug.DrawLine(transform.position, transform.position + worldSpaceInput * 5f);
				var transformedInput = InputToWorldSpace(rawInput);
				movement = transformedInput * movementRate;
				bodyFacing = transformedInput;
			}
			else
			{
				movement = Vector3.zero;
				// don't change desired body facing
			}
		}




		public override void PreExecuteIntention()
		{
			//throw new NotImplementedException();
		}

		public override void PostExecuteIntention()
		{
			//throw new NotImplementedException();
		}

		public Vector3 InputToWorldSpace(Vector2 rawInput)
		{
			return Quaternion.LookRotation(headFacing) * new Vector3(rawInput.x, 0f, rawInput.y);
		}
	}

}