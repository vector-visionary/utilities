﻿using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.BaseScripts;

namespace MantaMiddleware.MantaCharacter.Intentions
{
	public class TowardsEnemyIntention : CharacterIntentionBase
	{
		public override void UpdateIntentionPriorityInfo()
		{
			if (character.targetingSystem.primaryTarget == null) character.targetingSystem.automaticallySelectedTarget = character.targetingSystem.AutoSelectBestTarget();
			if (character.targetingSystem.primaryTarget == null)
			{
				priorityMultiplier = 0f;
			}
			else
			{
				priorityMultiplier = 1f;
				movement = (character.targetingSystem.primaryTarget.transform.position - character.transform.position).normalized;
				bodyFacing = movement;
				headFacing = movement;
			}
		}

		public override IEnumerable<IntentionAvailabilitySetEvent.IntentionCategory> GetAvailabilityCategories()
		{
			yield return IntentionAvailabilitySetEvent.IntentionCategory.InCombatOnly;
		}
	}

}