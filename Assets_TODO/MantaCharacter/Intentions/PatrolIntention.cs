﻿using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.BaseScripts;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MantaMiddleware.MantaCharacter.Intentions
{
	[RequireComponent(typeof(Collider))]
	public class PatrolIntention : CharacterIntentionBase
	{
		public enum FindPatrollingVolumeStrategy { ByName, ByBeingContained, ByNearest}
		public FindPatrollingVolumeStrategy findVolumeStrategy = FindPatrollingVolumeStrategy.ByNearest;
		public string patrollingVolumeName;
		private PatrolVolume patrollingVolume;
		
		public Vector3 targetPosition;
		public float radiusForRetargeting = 2f;

		protected override void Awake()
		{
			base.Awake();
			FindPatrollingVolume();
			if (patrollingVolume != null) ChooseRandomSpot();
		}

		public void FindPatrollingVolume()
		{
			if (findVolumeStrategy == FindPatrollingVolumeStrategy.ByName)
			{
				patrollingVolume = PatrolVolume.FindByName(patrollingVolumeName);
			}
			else if (findVolumeStrategy == FindPatrollingVolumeStrategy.ByNearest)
			{
				patrollingVolume = PatrolVolume.FindByNearest(transform.position);
			}
			else if (findVolumeStrategy == FindPatrollingVolumeStrategy.ByBeingContained)
			{
				patrollingVolume = PatrolVolume.FindByContainedPoint(transform.position);
			}
		}

		public override void UpdateIntentionPriorityInfo()
		{
			if (patrollingVolume == null)
			{
				FindPatrollingVolume();
			}

			priorityMultiplier = (patrollingVolume != null) ? 1f : 0f;
			if (patrollingVolume == null) return;

			if (Vector3.Distance(transform.position, targetPosition) < radiusForRetargeting) ChooseRandomSpot();
			movement = (targetPosition - transform.position).normalized;
			bodyFacing = movement;
			headFacing = movement;
		}

		public void ChooseRandomSpot()
		{
			if (priorityMultiplier > 0f) targetPosition = patrollingVolume.transform.TransformPoint(Random.insideUnitSphere * 0.5f);
		}

		protected override void OnDrawGizmosSelected()
		{
			base.OnDrawGizmosSelected();
			Gizmos.DrawLine(transform.position, targetPosition);
		}

		public override IEnumerable<IntentionAvailabilitySetEvent.IntentionCategory> GetAvailabilityCategories()
		{
			yield return IntentionAvailabilitySetEvent.IntentionCategory.NoneSpecified;
		}
	}

}