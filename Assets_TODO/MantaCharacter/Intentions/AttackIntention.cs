﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Intentions
{
	public abstract class AttackIntention : CharacterIntentionBase
	{
		public AttackHitbox shouldAttackWithHitbox;
		public AttackMetadataBase useActionInfo;

		public CharacterAction action { get; private set; }

		public async override UniTask Handle(CharacterInitializationEvent evt)
		{
			await base.Handle(evt);
			if (useActionInfo == null) Debug.LogError($"{character.gameObject.name}.{gameObject.name}: useActionInfo was not assigned", this);
			action = character.GetAction(useActionInfo);
		}

		public override IEnumerable<IntentionAvailabilitySetEvent.IntentionCategory> GetAvailabilityCategories()
		{
			yield return IntentionAvailabilitySetEvent.IntentionCategory.InCombatOnly;
		}

		public override bool isExclusive => false;

		public virtual void Reset()
		{
			shouldAttackWithHitbox = GetComponent<AttackHitbox>();
		}
	}

}