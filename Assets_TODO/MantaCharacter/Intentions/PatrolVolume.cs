﻿using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.Story;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Intentions
{
	public class PatrolVolume : SpecialLandscapeBase
	{
		private static List<PatrolVolume> all = new List<PatrolVolume>();
		private static DictionarySet<string, PatrolVolume> allByName = new DictionarySet<string, PatrolVolume>();

		public static PatrolVolume FindByName(string name)
		{
			if (allByName.TryGetOneItem(name, out var rtn)) return rtn;
			return null;
		}

		public static PatrolVolume FindByNearest(Vector3 position)
		{
			float nearestDist = 9999f;
			PatrolVolume nearest = null;
			foreach (var vol in all)
			{
				float thisDist = (position - vol.triggerCollider.ClosestPoint(position)).sqrMagnitude;
				if (thisDist < nearestDist)
				{
					nearestDist = thisDist;
					nearest = vol;
				}
			}
			return nearest;
		}
		public static PatrolVolume FindByContainedPoint(Vector3 position)
		{
			foreach (var vol in all)
			{
				float thisDist = (position - vol.triggerCollider.ClosestPoint(position)).sqrMagnitude;
				if (thisDist < 0.00001f)
				{
					return vol;
				}
			}
			return null;
		}
		private void OnEnable()
		{
			all.Add(this);
			if (!string.IsNullOrEmpty(volumeName)) allByName.Add(volumeName, this);
		}
		private void OnDisable()
		{
			all.Remove(this);
			if (!string.IsNullOrEmpty(volumeName)) allByName.Remove(volumeName, this);
		}
		[SerializeField] private string volumeName;
	}
}