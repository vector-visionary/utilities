﻿using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.Story;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.SceneData
{
	public class SceneTarget : SpecialLandscapeBase
	{
		private void Reset()
		{
			if (string.IsNullOrEmpty(id)) id = gameObject.name;
		}

		public string id = "";
		private static Dictionary<string, SceneTarget> allByID = new Dictionary<string, SceneTarget>();
		protected virtual void OnEnable()
		{
			if (!string.IsNullOrEmpty(id))
			{
				if (allByID.TryGetValue(id.ToLowerInvariant(), out var notThis))
				{
					Debug.LogWarning($"There's already a MovementTarget with ID {id}, and it's {notThis}", notThis);
				}
				allByID.Add(id.ToLowerInvariant(), this);
			}
		}

		protected virtual void OnValidate()
		{
			gameObject.name = $"{GetType().Name}: id={id}";
		}

		protected virtual void OnDisable()
		{
			allByID.Remove(id.ToLowerInvariant());
		}

		public static SceneTarget FindByID(string id)
		{
			if (allByID.TryGetValue(id.ToLowerInvariant(), out var rtn)) return rtn;
			return null;
		}
		public static T FindByID<T>(string movementTargetID) where T : SceneTarget
		{
			SceneTarget rtn = FindByID(movementTargetID);
			return rtn as T;
		}

		private void OnDrawGizmos()
		{
			Gizmos.color = Color.magenta;
			Gizmos.DrawWireSphere(transform.position, 1f);
		}
	}

}