﻿using MantaMiddleware.MantaCharacter.BaseScripts;

namespace MantaMiddleware.MantaCharacter.Intentions
{
	public class AutoTriggerAttackIntention : AttackIntention
	{
		public AttackHitbox triggersAttackHitbox;

		public override void UpdateIntentionPriorityInfo()
		{
			priorityMultiplier = triggersAttackHitbox.GetOverlappedTargets().Length > 0 ? 1f : 0f;

			shouldAttackWithHitbox.ArmAction(useActionInfo);

			UpdateAnimator();
		}
	}

}