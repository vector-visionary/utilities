﻿using System;
using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.MantaCharacter.SceneData;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Intentions
{

	public class MoveToMovementTargetIntention : CharacterIntentionBase
	{
		public string TargetID;
		public event Action OnReachTargetCallback;
		private SceneTarget foundTarget;
		public float rate = 1f;
		public bool turnToMatchFoundTarget;
		public float successRangePos = 0.1f;
		public float successRangeAngle = 5f;
		public bool ignoreVertical = true;
		public float timeoutSecondsPerUnitDistance = 1f; // will allow us to exit an erroneous/blocked state in a reasonable amount of time
		private float timeToGiveUpOnThisTarget = 99999f;

		public override void UpdateIntentionPriorityInfo()
		{
			priorityMultiplier = -1f;
			if (foundTarget == null) {
				if (string.IsNullOrEmpty(TargetID))
				{
					return;
				}
				foundTarget = SceneTarget.FindByID(TargetID);
				if (foundTarget != null) timeToGiveUpOnThisTarget = Time.time + (timeoutSecondsPerUnitDistance * Vector3.Distance(transform.position, foundTarget.transform.position));
			}
			if (foundTarget == null)
			{
				CompleteMovement();
				return;
			}
			if (Time.time > timeToGiveUpOnThisTarget)
			{
				Debug.Log($"{character.name}.{nameof(MoveToMovementTargetIntention)} has timed out attempting to reach {foundTarget.gameObject.name}");
				CompleteMovement();
				return;
			}

			priorityMultiplier = 1f;
			if (showDebug) {
				Debug.DrawLine(transform.position, foundTarget.transform.position, Color.magenta, 0.1f);
			}
			Vector3 movementDelta = (foundTarget.transform.position - transform.position);
			if (ignoreVertical) movementDelta = movementDelta.FlattenAlongAxis(character.Up);
			if (movementDelta.magnitude < successRangePos)
			{
				movement = Vector3.zero;
				//We're at the target. Do we need to turn?
				if (turnToMatchFoundTarget)
				{
					headFacing = foundTarget.transform.forward;

					if (ignoreVertical) bodyFacing = headFacing.FlattenAndRetainMagnitude(character.Up);
					else bodyFacing = headFacing;

					var actualFacing = character.transform.forward;
					if (ignoreVertical) actualFacing = actualFacing.FlattenAlongAxis(character.Up);
					float angleDelta = Vector3.Angle(actualFacing, bodyFacing);
					if (angleDelta < successRangeAngle)
					{
						CompleteMovement();
					}
				}
				else
				{
					CompleteMovement();
				}
			}
			else
			{
				movement = movementDelta.normalized * rate;
				headFacing = movement;
				bodyFacing = movement;
			}
		}

		public override IEnumerable<IntentionAvailabilitySetEvent.IntentionCategory> GetAvailabilityCategories()
		{
			yield return IntentionAvailabilitySetEvent.IntentionCategory.NoneSpecified;
		}

		private void CompleteMovement()
		{
			TargetID = "";
			foundTarget = null;
			if (OnReachTargetCallback != null) OnReachTargetCallback();
			OnReachTargetCallback = null;
			movement = Vector3.zero;
			priorityMultiplier = -1f;
		}
	}

}