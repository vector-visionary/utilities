using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.LogUtilities;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Characters
{
    public class AttributeSet : MonoBehaviour, IPoolResetter
    {
        [SerializeField] private CharacterAttributeInfoBase[] attributeInfos;
        public string ResourceName = "Generic";


        private void Awake()
        {
            InitializeAttributeDictionaries().Forget();
        }

        private Dictionary<CharacterAttributeInfoBase, CharacterAttributeBase> attributesByInfo = null;
        private Dictionary<string, CharacterAttributeBase> attributesByName = null;
        private Dictionary<Type, Dictionary<string, CharacterAttributeBase>> attributesByPrimitiveAndName = null;
        private async UniTask InitializeAttributeDictionaries()
        {
            // build the dictionary
            attributesByInfo = new Dictionary<CharacterAttributeInfoBase, CharacterAttributeBase>();
            attributesByName = new Dictionary<string, CharacterAttributeBase>();
            attributesByPrimitiveAndName = new Dictionary<Type, Dictionary<string, CharacterAttributeBase>>();
            await AddAttributesFromSerializedList();

            foreach (var att in attributesByName)
            {
                att.Value.Initialize(this);
            }
        }

        public void AddAttribute(CharacterAttributeBase att)
        {
            if (att != null && att.isInitialized)
            {
                attributesByInfo.Add(att.info_Base, att);

                attributesByName.Add(att.name, att);

                if (!attributesByPrimitiveAndName.TryGetValue(att.MyType, out var thisDict))
                {
                    thisDict = new Dictionary<string, CharacterAttributeBase>();
                    attributesByPrimitiveAndName.Add(att.MyType, thisDict);
                }

                thisDict.Add(att.name, att);
            }
        }

        public Dictionary<string, CharacterAttributeBase> GetAttributeDictionaryForPrimitive<T>()
        {
            if (attributesByPrimitiveAndName == null)
            {
                SelectiveLogger.LogError(this, $"Tried to get attribute dictionary of {typeof(T)} but the attribute dictionaries are not initialized");
                return null;
            }
            return attributesByPrimitiveAndName[typeof(T)];
        }

        public async UniTask AddAttributesFromSerializedList()
        {
            // override to add all attributes
            for (int a=0; a<attributeInfos.Length;a++)
            {
                if (attributeInfos[a] is GenericCharacterAttributeInfoBase<float>) AddAttribute(await CharacterAttribute<float>.CreateForInfo(attributeInfos[a].name, this));
                if (attributeInfos[a] is GenericCharacterAttributeInfoBase<bool>)  AddAttribute(await CharacterAttribute<bool>.CreateForInfo(attributeInfos[a].name, this));
                if (attributeInfos[a] is GenericCharacterAttributeInfoBase<int>)   AddAttribute(await CharacterAttribute<int>.CreateForInfo(attributeInfos[a].name, this));
            }
        }
        public CharacterAttributeBase GetAttributeForName(string name)
        {
            if (attributesByName.TryGetValue(name, out var rtn))
            {
                return rtn;
            }
            return null;
        }
        public CharacterAttribute<T> GetAttributeForName<T>(string name)
        {
            if (attributesByName.TryGetValue(name, out var rtn))
            {
                return rtn as CharacterAttribute<T>;
            }
            return null;
        }

        public CharacterAttributeBase GetAttributeForInfo(CharacterAttributeInfoBase attributeInfo)
        {
            if (attributesByInfo == null) InitializeAttributeDictionaries();

            return attributesByInfo[attributeInfo];
        }

        public async UniTask PoolStart()
        {
            foreach (var attribute in attributesByName)
            {
                attribute.Value.Reset();
            }
        }

        public IEnumerable<CharacterAttributeBase> IterateAttributes()
        {
            foreach (var attr in attributesByName)
            {
                yield return attr.Value;
            }
        }

    }
}