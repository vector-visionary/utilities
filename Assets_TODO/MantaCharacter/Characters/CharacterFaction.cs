﻿using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.BaseScripts;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Characters
{
	public class CharacterFaction
	{
        public static CharacterFaction GetFaction(string factionName)
		{
            if (string.IsNullOrEmpty(factionName)) return null;

            factionName = factionName.ToLower();
            if (!allFactions.ContainsKey(factionName)) allFactions.Add(factionName, new CharacterFaction(factionName));
            return allFactions[factionName];
		}

		public void RegisterCharacter(CombatCharacter combatCharacter)
		{
			characters.Add(combatCharacter);
		}

		public void DeregisterCharacter(CombatCharacter combatCharacter)
		{
			characters.Remove(combatCharacter);
		}

		public ActionTarget[] GetTargetsOfEnemiesInRange(Vector3 center, float radius)
		{
			float radSquared = radius * radius;
			List<ActionTarget> rtn = new List<ActionTarget>();
			foreach (var otherFactionKVP in allFactions)
			{
				if (otherFactionKVP.Value != this && !alliedFactions.Contains(otherFactionKVP.Value))
				{
					foreach (var character in otherFactionKVP.Value.characters)
					{
						foreach (var target in character.actionTargets)
						{
							if (radius == Mathf.Infinity) {
								rtn.Add(target);
							}
							else
							{
								Vector3 relativePos = target.transform.position - center;
								if (relativePos.sqrMagnitude < radSquared)
								{
									rtn.Add(target);
								}
							}
						}
					}
				}
			}
			return rtn.ToArray();
		}

		public CombatCharacter[] GetEnemiesInRange(Vector3 center, float radius)
		{
			float radSquared = radius * radius;
			List<CombatCharacter> rtn = new List<CombatCharacter>();
			foreach (var otherFactionKVP in allFactions)
			{
				if (otherFactionKVP.Value != this && !alliedFactions.Contains(otherFactionKVP.Value))
				{
					foreach (var character in otherFactionKVP.Value.characters)
					{
						Vector3 relativePos = character.transform.position - center;
						if (relativePos.sqrMagnitude < radSquared) {
							rtn.Add(character);
						}
					}
				}
			}
			return rtn.ToArray();
		}

		private static Dictionary<string, CharacterFaction> allFactions = new Dictionary<string, CharacterFaction>();

		private string factionName;

		private HashSet<CombatCharacter> characters = new HashSet<CombatCharacter>();
		private HashSet<CharacterFaction> alliedFactions = new HashSet<CharacterFaction>();

		public CharacterFaction(string factionName)
		{
			this.factionName = factionName;
		}

	}
}