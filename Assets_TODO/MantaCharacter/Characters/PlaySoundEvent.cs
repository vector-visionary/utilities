using MantaMiddleware.Events;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Characters
{
    public class PlaySoundEvent : CastableEventBase
    {
        public AudioClip Clip { get; private set; }

        public PlaySoundEvent(AudioClip clip)
        {
            Clip = clip;
        }
    }
}