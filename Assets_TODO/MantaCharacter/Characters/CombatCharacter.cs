﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.MantaCharacter.Story;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Characters
{
	public class CombatCharacter : GameCharacter
	{
		public string factionName;
		public CharacterFaction faction => CharacterFaction.GetFaction(factionName);

		public bool destroyOnDeath = true;

		public float threatLevel = 0f;

		protected override async UniTask OnAwake()
		{
			base.OnAwake();
			_attributeSet.AddAttribute(await CharacterAttribute<float>.CreateForInfo($"Health", _attributeSet));
			_attributeSet.AddAttribute(await CharacterAttribute<float>.CreateForInfo($"ATB", _attributeSet));
			_attributeSet.AddAttribute(await CharacterAttribute<float>.CreateForInfo($"ATBRecharge", _attributeSet));
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			faction?.RegisterCharacter(this);
		}


		protected override void OnDisable()
		{
			base.OnDisable();
			faction?.DeregisterCharacter(this);
		}

		public delegate void DamageRecieveDelegate(CombatCharacter attacker, CombatCharacter target, CharacterAction attack);

		


		public override bool isDead => _isDead;
		private bool _isDead = false;

		public override async UniTask Death()
		{
			_isDead = true;
			if (destroyOnDeath) await ObjectPool.Destroy(gameObject);
		}

		public override async UniTask PoolStart()
		{
			await base.PoolStart();
			_isDead = false;
		}

		public ActionTarget[] actionTargets
		{
			get
			{
				if (_targetsArray == null)
				{
					_targetsArray = new ActionTarget[_targets.Count];
					_targets.CopyTo(_targetsArray);
				}
				return _targetsArray;
			}
		}
		private ActionTarget[] _targetsArray = null;
		private HashSet<ActionTarget> _targets = new HashSet<ActionTarget>();

		public void RegisterSelfTarget(ActionTarget target)
		{
			_targets.Add(target);
			_targetsArray = null;
		}
		public void DeregisterSelfTarget(ActionTarget target)
		{
			_targets.Remove(target);
			_targetsArray = null;
		}


		private ActionTarget lastUsedTarget;
		private AttackMetadataBase lastUsedAction;
		private float timeLastActionTargetUsed = -999f;
		public void UseActionOnTarget<T>(AttackInformation<T> attackInformation, ActionTarget actionTarget)
		{
			lastUsedTarget = actionTarget;
			lastUsedAction = attackInformation;
			timeLastActionTargetUsed = Time.time;
		}

		public override ActionTarget GetLastUsedActionTarget()
		{
			if (timeLastActionTargetUsed == Time.time)
			{
				return lastUsedTarget;
			}
			return null;
		}

		public bool WasActionUsedThisFrame<T>(AttackInformation<T> attackInformation)
		{
			return (lastUsedAction == attackInformation && timeLastActionTargetUsed == Time.time);
		}

		private Dictionary<string, AttackHitbox> allHitboxes = new Dictionary<string, AttackHitbox>();
		public void RegisterAttackHitbox(AttackHitbox hitbox)
		{
			allHitboxes.Add(hitbox.id, hitbox);
		}

		public void DeregisterAttackHitbox(AttackHitbox hitbox)
		{
			allHitboxes.Remove(hitbox.id);
		}

		public float GetMaximumThreatLevelOfTargetsInRange()
		{
			float rtn = -1f;
			foreach (var target in targetingSystem.allActiveTargets)
			{
				if (target.parent.threatLevel > rtn) rtn = target.parent.threatLevel;
			}
			return rtn;
		}

		public void SetAttackHitboxActive(IAttackEnabler attackEnabler, bool toBeEnabled)
		{
			if (allHitboxes.TryGetValue(attackEnabler.HitboxID, out var hitbox)) {
				hitbox.SetLive(attackEnabler, toBeEnabled);
			}
		}
	}
}