using System;
using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.BaseScripts;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Characters
{

	public class MainCharacter : CombatCharacter
    {
		public static MainCharacter Main => main;
		private static MainCharacter main;

		
		protected override async UniTask OnAwake()
		{
			main = this;
			await base.OnAwake();
		}

		public override async UniTask Death()
		{
			await base.Death();
			Debug.Log($"The main character has died");
		}
		
	}


}