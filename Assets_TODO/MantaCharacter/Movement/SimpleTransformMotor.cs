﻿using MantaMiddleware.MantaCharacter.BaseScripts;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Movement
{
	public class SimpleTransformMotor : CharacterMotorBase
	{
		public float speed = 1f;
		public float rotationSpeed = 90f;

		public override void ExecuteOnIntention(CharacterIntentionBase intention)
		{
			transform.position += intention.movement * speed * Time.deltaTime;
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(intention.bodyFacing), rotationSpeed * Time.deltaTime);
			
		}

	}
}