﻿using System;
using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.AnimatorBehaviours;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Movement
{
	public class CharacterAnimatorMotor : CharacterMotorBase, IPoolDestroyer, IAnimatorStateMessageListener
	{
		private Animator animator;
		private int jump, isGrounded, translateX, translateY, translateZ, rotateX;


		protected override void Awake()
		{
			base.Awake();
			animator = GetComponent<Animator>();
			jump = Animator.StringToHash("Jump");
			isGrounded = Animator.StringToHash("IsGrounded");
			translateX = Animator.StringToHash("TranslateX");
			translateY = Animator.StringToHash("TranslateY");
			translateZ = Animator.StringToHash("TranslateZ");
			rotateX = Animator.StringToHash("RotateX");
		}

		public override void ExecuteOnIntention(CharacterIntentionBase intention)
		{
			if (intention != null)
			{
				animator.SetBool(jump, intention.jump);
				animator.SetBool(isGrounded, character.isGrounded);
				Vector3 strafing = characterBodyModel.InverseTransformDirection(intention.movement);
				animator.SetFloat(translateX, strafing.x);
				animator.SetFloat(translateY, strafing.y);
				animator.SetFloat(translateZ, strafing.z);
				Vector3 turning = characterBodyModel.InverseTransformDirection(intention.bodyFacing);
				animator.SetFloat(rotateX, turning.x);


			}
		}
		private Vector3 headFacing = Vector3.forward;

		private void OnAnimatorIK(int layerIndex)
		{
			animator.SetLookAtPosition(transform.position + headFacing);
		}
		
		public void OnAnimatorStateMessage(string s, AnimatorStateMessage origin)
		{
			if (s == "DeathFinished")
			{
				deathAnimDidFinish = true;
			}
		}

		private bool deathAnimDidFinish = false;
		public async UniTask PoolStop()
		{
			deathAnimDidFinish = false;
			animator.SetTrigger("Death");
			while (!deathAnimDidFinish)
			{
				await UniTask.DelayFrame(1);
			}
		}
	}
}