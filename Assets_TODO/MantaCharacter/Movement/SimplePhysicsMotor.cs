﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Movement
{

	[RequireComponent(typeof(Rigidbody))]
    public class SimplePhysicsMotor : CharacterMotorBase, IPoolResetter
	{
		public override float Importance => 100f;
		[SerializeField] private float jumpSpeed = 0f;
		[SerializeField] protected GravityDirection gravityDirectionType = GravityDirection.Standard;
		[SerializeField] private float gravityStrength = 1f; //multiplied by the world's gravity
		[SerializeField] protected bool flattenMovementAlongUpDirection = true;
		[SerializeField] protected bool disableMovementUntilFacingDirection = true;

		private HashSet<CharacterIntentionBase> intentionsWantingControlledJump = new HashSet<CharacterIntentionBase>();
		protected bool isInControlledJump => intentionsWantingControlledJump.Count > 0;
		[SerializeField] private float slopeNormalLimit = 0.6f;

		protected bool isInUncontrolledFall => !isGrounded && Time.time < uncontrolledFallStartedAtTime + uncontrolledFallTimeAfterCollision;
		[SerializeField] private float uncontrolledFallTimeAfterCollision = 0.2f;
		[SerializeField] private float minimumVelocityForUncontrolledFall = 0.3f;
		private float uncontrolledFallStartedAtTime = -999f;

		protected bool isGrounded => character.isGrounded;
		[SerializeField] private AnimationCurve rateToSpeedCurve = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(1f, 6f), new Keyframe(2f, 10f) });
		public float turnSpeed = 90f;

		public override Vector3 Up => up;
		public Vector3 up = Vector3.up;

		private new Rigidbody rigidbody;
		protected new Collider collider;

		protected override void Awake()
		{
            rigidbody = GetComponent<Rigidbody>();
			collider = GetComponent<Collider>();
			rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			rigidbody.useGravity = false; //we'll do our own
			
			base.Awake();
		}

		public Vector3 findNearestSurfaceIdealPoint = new Vector3(0f, 0.5f, 0.2f);
		private void FixedUpdate()
		{
			if (!IsInAnySceneBounds) return; // if we're not in a level, don't fall endlessly
			// custom-direction gravity
			if (gravityDirectionType == GravityDirection.NearestSurfaceDirection || gravityDirectionType == GravityDirection.NearestSurfaceNormal)
			{
				var nearestSurf = RaycastForNearestSurface(transform.TransformPoint(findNearestSurfaceIdealPoint));
				up = (gravityDirectionType == GravityDirection.NearestSurfaceDirection) ? transform.position - nearestSurf.point : nearestSurf.normal;
			}
			else
			{
				up = Vector3.up;
			}

			Vector3 gravityForce = Up * Physics.gravity.y * gravityStrength; // yeah we're gonna ignore X/Z
			rigidbody.velocity += gravityForce * Time.deltaTime;
		}

		public override void ExecuteOnIntention(CharacterIntentionBase intention)
		{
			if (intention == null) return;

			Vector3 oldVel = rigidbody.velocity;
			Vector3 oldVelAlongVertical = oldVel.IsolateAlongAxis(Up);
			Vector3 newVel = oldVel.FlattenAlongAxis(Up);

			float desiredMovementRate = intention.movement.magnitude;
			if (!character.doesCurrentAnimAllowMovement) desiredMovementRate = 0f;
			Vector3 desiredMovementNormalized;
			if (desiredMovementRate > 0f)
				desiredMovementNormalized = intention.movement / desiredMovementRate;
			else
				desiredMovementNormalized = Vector3.zero;

			if (showDebugLines)
			{
				var startPos = transform.position + debugLinesOffset;
				Debug.DrawLine(startPos, startPos + desiredMovementNormalized, Color.blue);
				Debug.DrawLine(startPos, startPos + Up);
			}

			if (intention.bodyFacing.sqrMagnitude > 0f)
			{
				Quaternion oldRot = characterBodyModel.rotation;
				Quaternion targetRot = Quaternion.LookRotation(intention.bodyFacing.FlattenAlongAxis(Up), Up);
				Quaternion newRot = Quaternion.RotateTowards(oldRot, targetRot, turnSpeed * Time.deltaTime);
				characterBodyModel.rotation = newRot;
				if (disableMovementUntilFacingDirection && newRot != targetRot) newVel = Vector3.zero;
			}

			if (!isInUncontrolledFall)
			{
				newVel = desiredMovementNormalized * rateToSpeedCurve.Evaluate(desiredMovementRate);

				if (flattenMovementAlongUpDirection)
				{
					newVel = newVel.FlattenAndRetainMagnitude(Up);
				}
			}

			newVel += oldVelAlongVertical;

			if (intention.jump && isGrounded && !isInControlledJump && character.doesCurrentAnimAllowMovement)
			{
				newVel += Up * jumpSpeed;
				intentionsWantingControlledJump.Add(intention);
			}
			if (!intention.jump && intentionsWantingControlledJump.Contains(intention))
			{
				intentionsWantingControlledJump.Remove(intention);

				if (!isInControlledJump) //e.g. "if we just removed the last one"
				{
					newVel.FlattenAlongAxis(Up);
				}
			}

			if (Vector3.Angle(Up, newVel) > 90f) intentionsWantingControlledJump.Clear();


			rigidbody.velocity = newVel;
		}

		protected RaycastHit RaycastForNearestSurface(Vector3 idealPosition)
		{
			if (allVectorsLocal == null)
			{
				allVectorsLocal = new List<Vector3>();
				allVectorsLocal.Add(Vector3.up);
				allVectorsLocal.Add(Vector3.down);
				for (float rn = 0f; rn < 1f; rn += 0.25f) { 
					for (float h = -1f; h < 1f; h += 0.25f)
					{
						float r = rn * Mathf.PI * 2f;
						allVectorsLocal.Add(new Vector3(Mathf.Cos(r), h, Mathf.Sin(r)));
					}
				}
			}
			RaycastHit bestHit = new RaycastHit();
			float bestActualDistSq = 9999f;
			bool didHit = false;
			foreach (var vec in allVectorsLocal)
			{
				Vector3 thisLocalVec = characterBodyModel.TransformDirection(vec);
				if (Physics.Raycast(origin: characterBodyModel.position - thisLocalVec, direction: thisLocalVec, hitInfo: out var thisHit, maxDistance:50f, layerMask: groundLayerMask))
				{
					float thisActualDistSq = (thisHit.point - idealPosition).sqrMagnitude;
					if (!didHit || thisActualDistSq < bestActualDistSq)
					{
						didHit = true;
						bestHit = thisHit;
						bestActualDistSq = thisActualDistSq;
					}
				}
			}
			if (showDebugLines)
			{
				Debug.DrawLine(characterBodyModel.position, bestHit.point, Color.yellow);
			}
			return bestHit;
		}
		private List<Vector3> allVectorsLocal;
		[SerializeField] private LayerMask groundLayerMask;

		protected virtual void OnCollisionStay(Collision col)
		{
			foreach (var contact in col.contacts)
			{
				var localContactPos = characterBodyModel.InverseTransformPoint(contact.point);
				var localContactNorm = characterBodyModel.InverseTransformDirection(contact.normal);
				var localColliderCenter = characterBodyModel.InverseTransformPoint(collider.bounds.center);
				if (localContactPos.y < localColliderCenter.y && localContactNorm.y > slopeNormalLimit)
				{
					character.isGrounded = true;
				}
				else if (col.relativeVelocity.magnitude > minimumVelocityForUncontrolledFall)
				{
					uncontrolledFallStartedAtTime = Time.time;
				}
			}
		}

		protected override void OnDrawGizmos()
		{
			base.OnDrawGizmos();
		}

		public async UniTask PoolStart()
		{
			rigidbody.velocity = Vector3.zero;
		}
	}
}