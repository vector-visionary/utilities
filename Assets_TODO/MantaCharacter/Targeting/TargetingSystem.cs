using System;
using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Targeting
{
	public class TargetingSystem : MonoBehaviour
    {
		public CombatCharacter combatCharacter;
		[SerializeField] private float updateTargetListInterval = 0.5f;
		private float lastUpdatedTargetAt = -999f;
		private static float globalLastUpdatedTargetAt = -999f;
		[SerializeField] private float worldManualTargetingMovementScale = 0.1f;
		[SerializeField] private float manualSelectDeadZone = 1f;
		public ActionTarget[] allActiveTargets;
		public ActionTarget manuallySelectedTarget;
		public ActionTarget automaticallySelectedTarget;

		[Header("Debug")]
		[SerializeField] private bool showDebug = false;
		[SerializeField] [Range(0.01f, 3f)] private float debugAngleInterval = 0.1f;
		[SerializeField] [Range(0.01f, .99f)] private float debugRadiusInterval = 0.05f;

		private TargetingWeight[] allWeights;
		private float range = Mathf.Infinity;
		private void Awake()
		{
			if (combatCharacter == null) combatCharacter = GetComponentInParent<CombatCharacter>();
		}
		private void Start()
		{
			CalcRange();
		}
		private void CalcRange()
		{
			range = Mathf.Infinity;
			allWeights = GetComponents<TargetingWeight>();
			foreach (var weight in allWeights)
			{
				range = Mathf.Min(range, weight.maxRange);
			}
		}

		public ActionTarget primaryTarget
		{
			get
			{
				if (manuallySelectedTarget != null) return manuallySelectedTarget;
				if (automaticallySelectedTarget == null) automaticallySelectedTarget = AutoSelectBestTarget();
				return automaticallySelectedTarget;
			}
		}

		private void Update()
		{
			if (Time.time > lastUpdatedTargetAt + updateTargetListInterval && Time.time > globalLastUpdatedTargetAt)
			{
				lastUpdatedTargetAt = Time.time;
				globalLastUpdatedTargetAt = Time.time;
				allActiveTargets = combatCharacter.faction.GetTargetsOfEnemiesInRange(combatCharacter.transform.position, range);
			}
		}


		public void SetManualTarget(ActionTarget target)
		{
			manuallySelectedTarget = target;
		}

		public ActionTarget AutoSelectBestTarget()
		{
			scoresCache.Clear();
			scoresLastRecalculated = Time.time;
			if (allActiveTargets.Length > 0)
			{
				ActionTarget bestFitTarget = null;
				float bestScore = 0f;
				foreach (var testTarget in allActiveTargets) {
					if (testTarget != null && testTarget.gameObject.activeInHierarchy) // a target can get destroyed
					{
						float thisScore = CalculateAutoSetScore(testTarget.parent, combatCharacter.InverseTransformPoint(testTarget.transform.position));
						scoresCache.Add(testTarget, thisScore);
						if (thisScore > bestScore)
						{
							bestScore = thisScore;
							bestFitTarget = testTarget;
						}
					}
				}
				return bestFitTarget;
			}
			return null;
		}

		private float scoresLastRecalculated = -999f;
		private Dictionary<ActionTarget, float> scoresCache = new Dictionary<ActionTarget, float>();
		public float GetAutoSetScore(ActionTarget target)
		{
			if (Time.time > scoresLastRecalculated) AutoSelectBestTarget();
			if (scoresCache.TryGetValue(target, out var score)) return score;
			return -1f;
		}

		private float CalculateAutoSetScore(GameCharacter character, Vector3 positionInTargetingSystemSpace)
		{
			if (!Application.isPlaying) allWeights = GetComponents<TargetingWeight>();
			float totalScore = 0f;
			float totalWeight = 0f;
			foreach (var weight in allWeights)
			{
				float thisScore = Mathf.Clamp01(weight.Score(character, positionInTargetingSystemSpace)) * weight.weight + weight.offset * weight.weight;
				if (weight.positiveResultMandatory && thisScore <= 0f) return -1f;
				totalScore += thisScore;
				totalWeight += weight.weight;
			}
			if (totalWeight == 0f) return 0f;
			return totalScore / totalWeight;
		}

		// MANUAL TARGET SELECTION
		private Vector3 currentManualTargetingOffset = Vector3.zero;
		public event Action<Vector3, bool> OnSetReticleStatus;

		public void BeginTargetSelectionMode()
		{
			currentManualTargetingOffset = Vector3.zero;
			OnSetReticleStatus?.Invoke(transform.position, true);
		}
		public void MoveTargetSelection(Vector3 directionMovedInWorldSpace)
		{
			currentManualTargetingOffset += directionMovedInWorldSpace * worldManualTargetingMovementScale;
			Vector3 newPos = transform.position + currentManualTargetingOffset;
			OnSetReticleStatus?.Invoke(newPos, true);
			manuallySelectedTarget = VectorUtilities.GetClosest(allActiveTargets, transform.position + currentManualTargetingOffset);
		}

		public void EndTargetSelectionMode()
		{
			OnSetReticleStatus?.Invoke(transform.position, false);
			if (currentManualTargetingOffset.sqrMagnitude < manualSelectDeadZone)
			{
				manuallySelectedTarget = null;
			}
			else
			{
				manuallySelectedTarget = VectorUtilities.GetClosest(allActiveTargets, transform.position + currentManualTargetingOffset);
			}
		}

		private void OnDrawGizmosSelected()
		{
			if (showDebug)
			{
				if (!Application.isPlaying) CalcRange();
				if (combatCharacter != null)
				{
					Gizmos.color = Color.yellow;
					Gizmos.DrawWireSphere(combatCharacter.transform.position, range);
					for (float angle = 0f; angle < 6.28f; angle += debugAngleInterval)
					{
						for (float radius = 1f; radius < range; radius += range * debugRadiusInterval)
						{
							Vector3 thisPosLocal = new Vector3(Mathf.Cos(angle) * radius, 0f, Mathf.Sin(angle) * radius);
							float score = CalculateAutoSetScore(null, thisPosLocal);
							var thisPosWorld = transform.TransformPoint(thisPosLocal);
							Gizmos.color = score < 0f ? Color.black : Color.Lerp(Color.red, Color.green, score);
							Gizmos.DrawCube(thisPosWorld, Vector3.one * 0.2f);
						}
					}
				}
			}
		}
	}
}