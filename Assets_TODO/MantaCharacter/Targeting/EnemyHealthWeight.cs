﻿using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Targeting
{
	public class EnemyHealthWeight : TargetingWeight
	{
		public AnimationCurve normalizedHealthToScoreCurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(0.01f, 1f), new Keyframe(1f, 1f));
		public override float Score(GameCharacter character, Vector3 positionInTargetingSystemSpace)
		{
			if (character != null)
			{
				var healthAtt = character.AttributeSet.GetAttributeForName("Health");
				if (healthAtt != null)
				{
					return normalizedHealthToScoreCurve.Evaluate(healthAtt.normalizedValue);
				}
			}
				return 1f;
		}
	}
}