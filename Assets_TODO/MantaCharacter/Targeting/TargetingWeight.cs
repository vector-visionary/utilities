﻿using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Targeting
{
	public abstract class TargetingWeight : MonoBehaviour
	{
		public virtual float maxRange => Mathf.Infinity;
		[Range(-1f, 2f)]
		public float weight = 1f;
		[Range(-1f, 1f)]
		public float offset = 0f;
		// returns 0 to 1 - 0 being "not a target", 1 being "highest value target"
		public abstract float Score(GameCharacter character, Vector3 positionInTargetingSystemSpace);
		public bool positiveResultMandatory = false;
	}
}