﻿using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Targeting
{
	public class RangeWeight : TargetingWeight
	{
		public float range = 20f;
		public override float maxRange => range;
		public AnimationCurve curveInRange = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 1f), new Keyframe(1f, 0f) });
		public override float Score(GameCharacter character, Vector3 positionInTargetingSystemSpace)
		{
			return curveInRange.Evaluate(positionInTargetingSystemSpace.magnitude / maxRange);
		}
	}
}