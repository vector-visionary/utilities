﻿using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Targeting
{
	public class LineOfSightWeight : TargetingWeight
	{
		public LayerMask landscapeLayer;
		public override float Score(GameCharacter character, Vector3 positionInTargetingSystemSpace)
		{
			if (Physics.Linecast(start: transform.position, end: transform.TransformPoint(positionInTargetingSystemSpace), layerMask: landscapeLayer))
			{
				return 0f;
			}
			return 1f;
		}
	}
}