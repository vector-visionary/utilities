﻿using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.Targeting
{
	public class FacingWeight : TargetingWeight
	{
		[SerializeField] AnimationCurve angleToScoreCurve = new AnimationCurve(new Keyframe(0f, 1f), new Keyframe(180f, 0f));
		public override float Score(GameCharacter character, Vector3 positionInTargetingSystemSpace)
		{
			return angleToScoreCurve.Evaluate(Vector3.Angle(Vector3.forward, positionInTargetingSystemSpace));
		}
	}
}