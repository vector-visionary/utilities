using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
    public class AttackHitbox : MonoBehaviour
    {
		private HashSet<ActionTarget> overlappingActionTargets = new HashSet<ActionTarget>();
		public CombatCharacter combatCharacter;
		public string id;
		private CharacterAction isArmedWithAction;
		[SerializeField] private int overlappingCountDebugOutput = 0;

		private void Awake()
		{
			combatCharacter = transform.GetComponentAlongParentage<CombatCharacter>();
		}

		private void OnEnable()
		{
			combatCharacter.RegisterAttackHitbox(this);
		}

		private void OnDisable()
		{
			overlappingActionTargets.Clear();
			combatCharacter.DeregisterAttackHitbox(this);
		}

		private ActionTarget[] overlapReturnCache; // to minimum GC allocs
		public ActionTarget[] GetOverlappedTargets()
		{
			if (overlappingActionTargets.Count == 0) return new ActionTarget[0];
			overlappingActionTargets.RemoveWhere((target) => target == null || !target.isActiveAndEnabled || target.parent == null || !target.parent.isActiveAndEnabled);
			if (overlapReturnCache == null || overlapReturnCache.Length != overlappingActionTargets.Count)
			{
				overlapReturnCache = new ActionTarget[overlappingActionTargets.Count];
			}
			overlappingActionTargets.CopyTo(overlapReturnCache);
			return overlapReturnCache;
		}

		public void ArmAction(AttackMetadataBase useActionInfo)
		{
			isArmedWithAction = combatCharacter.GetAction(useActionInfo);
		}

		private void OnTriggerEnter(Collider other)
		{
			var actionTarget = other.GetComponent<ActionTarget>();
			if (actionTarget != null && actionTarget.parent.faction != combatCharacter.faction)
			{
				overlappingActionTargets.Add(actionTarget);
				if (isLive && isArmedWithAction != null && isArmedWithAction.CanBeUsed)
				{
					isArmedWithAction.TriggerAction(actionTarget);
				}
			}
			overlappingCountDebugOutput = overlappingActionTargets.Count;

		}

		private void OnTriggerExit(Collider other)
		{
			var cc = other.GetComponent<ActionTarget>();
			if (cc != null && cc.parent.faction != combatCharacter.faction)
			{
				overlappingActionTargets.Remove(cc);
			}
			overlappingCountDebugOutput = overlappingActionTargets.Count;
		}

		private void Update()
		{
			if (isLive && isArmedWithAction != null)
			{
				bool wasActionUsed = isArmedWithAction.TriggerAction();
				if (wasActionUsed && isArmedWithAction.info.HitsOnlyOnce)
				{
					isArmedWithAction = null;
				}
			}
		}


		[SerializeField] private bool isAlwaysLive = false;
		private HashSet<IAttackEnabler> attackEnablers = new HashSet<IAttackEnabler>();
		public bool isLive => isAlwaysLive || attackEnablers.Count > 0;
		public void SetLive(IAttackEnabler attackEnabler, bool toBeEnabled)
		{
			if (toBeEnabled && !attackEnablers.Contains(attackEnabler)) attackEnablers.Add(attackEnabler);
			if (!toBeEnabled && attackEnablers.Contains(attackEnabler)) attackEnablers.Remove(attackEnabler);
		}
	}
}