﻿namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	public abstract class AttackInformation<T> : AttackMetadataBase
	{
		public GenericCharacterAttributeInfoBase<T> affectsTargetAttribute;
		public T effectValue;
		public T effectValueVariance;
		public float critChance = 0.05f;
		public float critMultiplier = 3f;

		public override void AffectActionTarget(CharacterAction action, ActionTarget actionTarget)
		{
			if (allowMultiTargetHitsOnSameCharacter || !actionTarget.parent.WasActionUsedThisFrame(this))
			{
				actionTarget.parent.UseActionOnTarget(this, actionTarget);
				var targetAttribute = actionTarget.parent.AttributeSet.GetAttributeForInfo(affectsTargetAttribute) as CharacterAttribute<T>;
				if (targetAttribute != null)
				{
					ApplyEffectToAttribute(targetAttribute);
				}
				
			}
		}

		public abstract void ApplyEffectToAttribute(CharacterAttribute<T> targetAttribute);

	}
}