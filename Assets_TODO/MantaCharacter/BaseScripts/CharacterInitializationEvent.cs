using MantaMiddleware.Events;
using MantaMiddleware.MantaCharacter.Characters;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
    public class CharacterInitializationEvent : CastableEventBase
    {
        public GameCharacter character;

        public CharacterInitializationEvent(GameCharacter character)
        {
            this.character = character;
        }
    }
}