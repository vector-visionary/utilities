using System;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.SceneControl;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	public abstract class CharacterMotorBase : MonoBehaviour, IEventCastListener<CharacterInitializationEvent>, IEventCastListener<ExecuteOnIntentionEvent>
    {
        public GameCharacter character;
		public Transform characterBodyModel => transform;
		[SerializeField] protected bool showDebugLines;
		[SerializeField] protected Vector3 debugLinesOffset;
		public virtual float Importance => 0f;

		public enum GravityDirection { Standard, NearestSurfaceNormal, NearestSurfaceDirection, ZeroGravity }

        protected virtual void Awake() {
            EventCaster.GetCasterFor(transform).Subscribe<CharacterInitializationEvent, ExecuteOnIntentionEvent>(this);
        }
		public bool IsInAnySceneBounds => SceneDataControllerBase.IsPositionInAnyScene(transform.position);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="worldDirection">Normalized value; magnitude = 1 for walking, 2 for running, higher for super-running?</param>
		public virtual Vector3 Up
		{
			get { return Vector3.up; }
		}

		protected virtual void OnDrawGizmos()
		{
			
		}


		public async UniTask Handle(CharacterInitializationEvent evt)
		{
			character = evt.character;
		}

		public async UniTask Handle(ExecuteOnIntentionEvent evt)
		{
			ExecuteOnIntention(evt.intention);
		}
		public abstract void ExecuteOnIntention(CharacterIntentionBase intention);
    }
}