﻿using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.MantaCharacter.Characters;
using MantaMiddleware.MantaCharacter.Movement;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.AnimatorBehaviours
{
    public class AnimatorStateAttackEnabler : StateMachineBehaviour, IAttackEnabler
	{
        [SerializeField] private string hitboxID = "";
        public string HitboxID => hitboxID;


		override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            (animator.GetComponent<CharacterAnimatorMotor>().character as CombatCharacter)?.SetAttackHitboxActive(this, true);
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            (animator.GetComponent<CharacterAnimatorMotor>().character as CombatCharacter)?.SetAttackHitboxActive(this, false);
        }

    }
}