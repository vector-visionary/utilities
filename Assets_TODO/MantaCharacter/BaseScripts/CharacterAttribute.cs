﻿using System;
using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	public abstract class CharacterAttributeBase { 
		public abstract string name { get; }
		public abstract bool isInitialized { get; }
		public abstract Type MyType { get; }

		public abstract float normalizedValue { get; }
		public float lastUpdateCycle = -1f;

		public abstract CharacterAttributeInfoBase info_Base { get; }

		public AttributeSet owner;

		public abstract void Initialize(AttributeSet owner);

		public abstract void Reset();
		public abstract bool FitsRequirement(AttributeRequirement attReq);
	}

	[Serializable]
	public class CharacterAttribute<T> : CharacterAttributeBase
	{
		public static async UniTask<CharacterAttribute<T>> CreateForInfo(string infoName, AttributeSet forCharacter)
		{
			var thisInfo = await GenericCharacterAttributeInfoBase<T>.Find(infoName, null);
			if (thisInfo == null)
			{
				Debug.LogError($"No attribute info found at path Resources/Attributes/{infoName}");
			}
			var rtn = new CharacterAttribute<T>();
			rtn.info = thisInfo;

			// We need all character to share common attributes (e.g. "Health") so that attacks etc can reference them
			// But if we need to change the values of these for specific characters, use this to override those default values.
			var valueOverrides = await GenericCharacterAttributeInfoBase<T>.Find(infoName, forCharacter);
			if (valueOverrides != null) {
				rtn.infoValuesProvider = valueOverrides;
			}
			else
			{
				rtn.infoValuesProvider = thisInfo;
			}
			return rtn;
		}

		public override void Initialize(AttributeSet owner)
		{
			isInitializing = true;
			if (infoValuesProvider == null) throw new Exception($"Attribute must have field info assigned.");
			this.owner = owner;
			this.minValue = infoValuesProvider.defaultMinValue;
			this.maxValue = infoValuesProvider.defaultMaxValue;
			this.currentValue = infoValuesProvider.startingValue;
			isInitializing = false;
		}

		public override void Reset()
		{
			isInitializing = true;
			this.currentValue = infoValuesProvider.startingValue;
			isInitializing = false;
		}

		public override string name => info.name;
		public override Type MyType => typeof(T);
		public GenericCharacterAttributeInfoBase<T> info;
		public override CharacterAttributeInfoBase info_Base => info;

		public override bool isInitialized => infoValuesProvider != null && !isInitializing;
		private bool isInitializing = false;

		// Current status of attribute
		public T currentValue
		{
			get
			{
				return _currentValue;
			}
			set
			{
				if (!value.Equals(_currentValue))
				{
					var oldValue = _currentValue;
					_currentValue = infoValuesProvider.ProcessChangedValue(this, oldValue, value);
				}
			}
		}
		private T _currentValue = default(T);

		public T minValue;
		public T maxValue;
		private GenericCharacterAttributeInfoBase<T> infoValuesProvider;
		[NonSerialized] public bool wasAffectedByCrit = false;

		public override float normalizedValue => info.GetNormalizedValue(this);

		public override bool FitsRequirement(AttributeRequirement attReq)
		{
			if (attReq.useNormValueForRequirement)
			{
				if (attReq.requiresHigher && normalizedValue < attReq.normValueRequirement) return false;
				if (!attReq.requiresHigher && normalizedValue > attReq.normValueRequirement) return false;
			}
			else
			{
				if (this is CharacterAttribute<float> fThis)
				{
					if (attReq.requiresHigher && fThis.currentValue < attReq.rawValueRequirement) return false;
					if (!attReq.requiresHigher && fThis.currentValue > attReq.rawValueRequirement) return false;
				}
				else if (this is CharacterAttribute<bool> bThis)
				{
					if (attReq.requiresHigher != bThis.currentValue) return false;
				}
				else if (this is CharacterAttribute<int> iThis)
				{
					if (attReq.requiresHigher && iThis.currentValue < attReq.rawValueRequirement) return false;
					if (!attReq.requiresHigher && iThis.currentValue > attReq.rawValueRequirement) return false;
				}
			}
			return true;
		}
	}
}