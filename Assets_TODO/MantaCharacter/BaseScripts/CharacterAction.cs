﻿using System;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	[System.Serializable]
	public class CharacterAction
	{
		public static CharacterAction CreateActionFromInfo(AttackMetadataBase info, AttackHitbox hitbox)
		{
			var rtn = new CharacterAction();
			rtn.info = info;
			rtn.hitbox = hitbox;
			return rtn;
		}

		public string name => info?.name;
		public AttackHitbox hitbox;
		public AttackMetadataBase info;
		public float timeLastUsed = -9999f;
		[NonSerialized]
		public GameCharacter user;

		// returns: did hit its target
		public bool TriggerAction(ActionTarget target = null)
		{
			if (!CanBeUsed) return false;
			timeLastUsed = Time.time;

			// Use up any usable attributes
			foreach (var attReq in info.attributeRequirements)
			{
				if (attReq.reducesAttributeOnUse)
				{
					var liveAtt = user.AttributeSet.GetAttributeForInfo(attReq.attribute);
					if (liveAtt is CharacterAttribute<float> fAtt)
					{
						float delta = attReq.useNormValueForRequirement ? attReq.normValueRequirement * (fAtt.maxValue - fAtt.minValue) : attReq.rawValueRequirement;
						if (attReq.requiresHigher)
						{
							fAtt.currentValue -= delta;
						}
						else
						{
							fAtt.currentValue += delta;
						}
					}
					else if (liveAtt is CharacterAttribute<bool> bAtt)
					{
						bAtt.currentValue = !attReq.requiresHigher;
					}
				}
			}

			// TODO: event
			//info.effectOnUser.Invoke(user);

			bool didHit = false;
			if (target != null)
			{
				target.ReceiveAction(this);
				didHit = true;
			}
			else
			{
				if (hitbox != null)
				{
					var targets = hitbox.GetOverlappedTargets();
					foreach (var thisTarget in targets)
					{
						thisTarget.ReceiveAction(this);
						didHit = true;
					}
				}
			}
			return didHit;
		}

		public bool HasCooldownPeriod => info.HasCooldownPeriod;

		public float CooldownFillAmount
		{
			get {
				if (!HasCooldownPeriod) return 0f;
				if (Time.time > timeLastUsed + info.CooldownPeriod) return 1f;
				return (Time.time - timeLastUsed) / info.CooldownPeriod;
			}
		}

		public bool CanBeUsed
		{
			get
			{
				if (HasCooldownPeriod && Time.time < timeLastUsed + info.CooldownPeriod) {
					return false;
				}

				foreach (var attReq in info.attributeRequirements)
				{
					if (attReq != null)
					{
						var liveAtt = user.AttributeSet.GetAttributeForInfo(attReq.attribute);
						if (!liveAtt.FitsRequirement(attReq)) return false;
					}
				}
				return true;
			}
		}

		public void Reset()
		{
			timeLastUsed = -9999f;
		}
	}
}