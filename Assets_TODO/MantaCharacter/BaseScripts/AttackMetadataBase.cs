﻿using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	public abstract class AttackMetadataBase : ScriptableObject
	{
		public Sprite icon;
		public float CooldownPeriod = 1f;
		public bool HasCooldownPeriod => CooldownPeriod > 0f;

		public abstract bool HitsOnlyOnce { get; }
		public bool allowMultiTargetHitsOnSameCharacter = false;

		public AttributeRequirement[] attributeRequirements;

		public abstract void AffectActionTarget(CharacterAction action, ActionTarget actionTarget);
	}
}