﻿using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	[System.Serializable]
	public class AttributeRequirement
	{
		public CharacterAttributeInfoBase attribute;
		public bool useNormValueForRequirement = false;
		[Range(0f, 1f)]
		public float normValueRequirement = 0.5f;
		public float rawValueRequirement = 1f; //for bools, 1 = true
		public bool requiresHigher = true;
		public bool reducesAttributeOnUse = false;
	}
}