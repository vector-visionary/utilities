﻿using Cysharp.Threading.Tasks;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	public abstract class CharacterAttributeInfoBase : ScriptableObject
	{
		public bool isVisibleInUI = true;
		public Color attributeColor = Color.white;
		public abstract object startingValue_obj { get; }
		public abstract float updateCycleInterval { get; }
		public virtual void UpdateAttribute(CharacterAttributeBase liveAttribute, float deltaTime)
		{

		}
	}

	public abstract class GenericCharacterAttributeInfoBase<T> : CharacterAttributeInfoBase
	{
		public override object startingValue_obj => startingValue;
		public T startingValue;
		public T defaultMinValue;
		public T defaultMaxValue;
		public abstract float GetNormalizedValue(CharacterAttribute<T> obj);



		public static async UniTask<GenericCharacterAttributeInfoBase<T>> Find(string name, AttributeSet forCharacter)
		{
			if (forCharacter != null)
			{
				var specificAttr = await Resources.LoadAsync<GenericCharacterAttributeInfoBase<T>>($"Attributes/{forCharacter.ResourceName}/{name}");
				if (specificAttr != null) return (GenericCharacterAttributeInfoBase<T>)specificAttr;
			}
			var baseAttr = await Resources.LoadAsync<GenericCharacterAttributeInfoBase<T>>($"Attributes/{name}");

			return (GenericCharacterAttributeInfoBase<T>)baseAttr;
		}

		public virtual T ProcessChangedValue(CharacterAttribute<T> characterAttribute, T oldValue, T newValue)
		{
			if (!oldValue.Equals(newValue))
			{
				//TODO: event
				//onChange.Invoke(characterAttribute, baseAttribute.onChange.Invoke);
			}
			//mostly exists to be overridden
			return newValue;
		}

		// override to any positive nonzero number to use updates at specific time intervals,
		// or 0 to update every frame
		public override float updateCycleInterval => -1f;


	}
}