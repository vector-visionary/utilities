﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	public abstract class CharacterIntentionBase : MonoBehaviour, IEventCastListener<CharacterInitializationEvent>, IEventCastListener<IntentionAvailabilitySetEvent>
	{
		public GameCharacter character { get; private set; }
		public virtual bool isExclusive => true;
		public virtual bool characterMustBeAlive => true;

		public float priority = 1f;

		public string animationParameter;
		public enum AnimatorParamType { Trigger, Bool, Float }
		public AnimatorParamType animationParameterType = AnimatorParamType.Bool;

		[Header("Intention Output")]
		public bool showDebug = false;
		public Vector3 movement = Vector3.zero;
		public Vector3 bodyFacing = Vector3.zero;
		public Vector3 headFacing = Vector3.zero;

		public bool jump = false;
		[Range(0f, 1f)] public float priorityMultiplier = 1f;


		public virtual void SetPriorityDisabled()
		{
			priorityMultiplier = 0f;
			UpdateAnimator();
		}

		public bool UpdatePriorityAndAvailability()
		{
			UpdateIntentionPriorityInfo();
			return IsAvailable();
		}
		public abstract void UpdateIntentionPriorityInfo();
		public virtual void PreExecuteIntention() { }
		public virtual void PostExecuteIntention() { }

		protected virtual void Awake()
		{
			EventCaster.GetCasterFor(transform).Subscribe<CharacterInitializationEvent, IntentionAvailabilitySetEvent>(this);
		}
		protected virtual void OnEnable() { }
		protected virtual void OnDisable() { }
		
		protected virtual void OnDrawGizmosSelected()
		{

		}

		protected virtual void UpdateAnimator()
		{
			if (character.animator != null && !string.IsNullOrEmpty(animationParameter))
			{
				if (animationParameterType == AnimatorParamType.Bool)
					character.animator.SetBool(animationParameter, priorityMultiplier > 0f);
				else if (animationParameterType == AnimatorParamType.Trigger)
				{
					if (priorityMultiplier > 0f) character.animator.SetTrigger(animationParameter);
				}
				else if (animationParameterType == AnimatorParamType.Float)
					character.animator.SetFloat(animationParameter, priorityMultiplier);
			}
		}

		public async virtual UniTask Handle(CharacterInitializationEvent evt)
		{
			this.character = evt.character;
		}


		public bool IsAvailable()
		{
			return categoriesCurrentlyBlockingIntentionAvailability.Count > 0;
		}
		public async UniTask Handle(IntentionAvailabilitySetEvent evt)
		{
			foreach (var category in GetAvailabilityCategories())
			{
				if (category == evt.category)
				{
					if (evt.IsEnabled)
					{
						categoriesCurrentlyBlockingIntentionAvailability.Add(category);
					}

					if (!evt.IsEnabled && categoriesCurrentlyBlockingIntentionAvailability.Contains(category))
					{
						categoriesCurrentlyBlockingIntentionAvailability.Remove(category);
					}
				}
			}
		}

		private HashSet<IntentionAvailabilitySetEvent.IntentionCategory>
			categoriesCurrentlyBlockingIntentionAvailability =
				new HashSet<IntentionAvailabilitySetEvent.IntentionCategory>();
		public abstract IEnumerable<IntentionAvailabilitySetEvent.IntentionCategory> GetAvailabilityCategories();
	}

}