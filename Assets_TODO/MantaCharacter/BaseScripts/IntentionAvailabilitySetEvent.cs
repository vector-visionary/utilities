using MantaMiddleware.Events;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
    public class IntentionAvailabilitySetEvent : CastableEventBase
    {
        public enum IntentionCategory { NoneSpecified, PlayerInput, InCombatOnly, AliveOnly }

        public IntentionCategory category = IntentionCategory.InCombatOnly;
        public bool IsEnabled = false;

        public IntentionAvailabilitySetEvent(IntentionCategory category, bool isEnabled)
        {
            this.category = category;
            IsEnabled = isEnabled;
        }
    }
}