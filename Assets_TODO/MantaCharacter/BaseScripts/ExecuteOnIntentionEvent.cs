using MantaMiddleware.Events;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
    public class ExecuteOnIntentionEvent : CastableEventBase
    {
        public readonly CharacterIntentionBase intention;

        public ExecuteOnIntentionEvent(CharacterIntentionBase intention)
        {
            this.intention = intention;
        }
    }
}