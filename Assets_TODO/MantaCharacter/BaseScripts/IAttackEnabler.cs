namespace MantaMiddleware.MantaCharacter.BaseScripts
{
    public interface IAttackEnabler
    {
        string HitboxID { get; }
    }
}