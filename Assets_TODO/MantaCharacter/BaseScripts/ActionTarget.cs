﻿using System.Collections.Generic;
using MantaMiddleware.MantaCharacter.Characters;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.BaseScripts
{
	[RequireComponent(typeof(Collider))]
	public class ActionTarget : MonoBehaviour
	{
		public static HashSet<ActionTarget> all => _all;
		private static HashSet<ActionTarget> _all = new HashSet<ActionTarget>();

		public CombatCharacter parent;
		public bool isVisibleToPlayer = true;
		public bool isNameVisibleToPlayer = true;

		private void Awake()
		{
			if (parent == null)
			{
				parent = GetComponentInParent<CombatCharacter>();
			}
		}

		private void OnEnable()
		{
			_all.Add(this);
			parent.RegisterSelfTarget(this);
		}

		private void OnDisable()
		{
			_all.Remove(this);
			parent.DeregisterSelfTarget(this);
		}

		public void ReceiveAction(CharacterAction action)
		{
			var incomingActionInfo = action.info;
			incomingActionInfo.AffectActionTarget(action, this);
		}
	}
}