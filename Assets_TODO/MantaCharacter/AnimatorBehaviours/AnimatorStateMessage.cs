﻿using UnityEngine;

namespace MantaMiddleware.MantaCharacter.AnimatorBehaviours
{
	public class AnimatorStateMessage : StateMachineBehaviour
	{
        public string messageOnEnter = "";
        public string messageDuringUpdate = "";
        public string messageOnExit = "";

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!string.IsNullOrEmpty(messageOnEnter))
            {
                var allListeners = animator.GetComponentsInChildren<IAnimatorStateMessageListener>();
                foreach (var listener in allListeners)
                {
                    listener.OnAnimatorStateMessage(messageOnEnter, this);
                }
            }
        }

		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
            if (!string.IsNullOrEmpty(messageDuringUpdate))
            {
                var allListeners = animator.GetComponentsInChildren<IAnimatorStateMessageListener>();
                foreach (var listener in allListeners)
                {
                    listener.OnAnimatorStateMessage(messageDuringUpdate, this);
                }
                var allTimeListeners = animator.GetComponentsInChildren<IAnimatorTimeStateMessageListener>();
                foreach (var listener in allTimeListeners)
                {
                    listener.OnAnimatorStateMessage(messageDuringUpdate, this, stateInfo.normalizedTime);
                }
            }
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (!string.IsNullOrEmpty(messageOnExit))
            {
                var allListeners = animator.GetComponentsInChildren<IAnimatorStateMessageListener>();
                foreach (var listener in allListeners)
                {
                    listener.OnAnimatorStateMessage(messageOnExit, this);
                }
            }
        }
    }

    public interface IAnimatorStateMessageListener
	{
        void OnAnimatorStateMessage(string s, AnimatorStateMessage origin);
	}

    public interface IAnimatorTimeStateMessageListener
	{
        void OnAnimatorStateMessage(string s, AnimatorStateMessage origin, float normalizedTime);
    }
}