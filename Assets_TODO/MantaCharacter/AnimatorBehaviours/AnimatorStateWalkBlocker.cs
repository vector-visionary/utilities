using MantaMiddleware.Events;
using MantaMiddleware.MantaCharacter.BaseScripts;
using MantaMiddleware.MantaCharacter.Movement;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.AnimatorBehaviours
{
    public class AnimatorStateWalkBlocker : StateMachineBehaviour, IWalkBlocker
    {
        
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            EventCaster.GetCasterFor(animator).Cast(new SetWalkBlockerEvent(this, true));
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            EventCaster.GetCasterFor(animator).Cast(new SetWalkBlockerEvent(this, false));
        }
    }

    public class SetWalkBlockerEvent : CastableEventBase
    {
        public IWalkBlocker blocker;
        public bool doesBlock;

        public SetWalkBlockerEvent(IWalkBlocker blocker, bool doesBlock)
        {
            this.blocker = blocker;
            this.doesBlock = doesBlock;
        }
    }
}