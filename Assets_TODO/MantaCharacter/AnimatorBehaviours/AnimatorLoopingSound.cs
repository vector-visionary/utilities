﻿using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.MantaCharacter.AnimatorBehaviours
{
	public class AnimatorLoopingSound : StateMachineBehaviour
	{
        public AudioClip defaultClip;
        public string variantIndexParamName = "VariantIndex";
        public AudioClip[] clipsByVariantIndex;

        public AudioClip GetClip(Animator animator)
		{
            if (animator.HasParameter(variantIndexParamName, AnimatorControllerParameterType.Int)) {
                int variant = animator.GetInteger(variantIndexParamName);
                if (variant >= 0 && variant < clipsByVariantIndex.Length)
                {
                    return clipsByVariantIndex[variant];
                }
            }
            return defaultClip;
		}

		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
            var source = animator.GetComponent<AudioSource>();
            if (source == null) source = animator.gameObject.AddComponent<AudioSource>();
            if (source.clip == GetClip(animator) && source.isPlaying)
			{
                source.loop = true;
                return;
			}

            source.clip = GetClip(animator);
            source.loop = true;
            source.Play();
		}

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var source = animator.GetComponent<AudioSource>();
            if (source == null) return;
            if (source.clip == GetClip(animator) && source.isPlaying)
            {
                source.Stop();
            }

        }
    }
}