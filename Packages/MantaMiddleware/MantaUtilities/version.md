# MantaUtilities Version History

## 1.2

Documentation updates mainly

## 1.1

* Added MatchedDisplayerSet.UpdateSingle to call SetToDisplay on the one matching object for the provided data item

## 1.0

* Initial Release


# Future Roadmap

No specific plans yet