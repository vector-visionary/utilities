﻿namespace MantaMiddleware.Utilities
{
	public static class RangedLerpableSet
	{
		public delegate T LerpDelegate<T>(T a, T b, float lerpValue) where T : IRangedLerpable;
		public static T Lerp<T>(T[] collection, float targetValue, LerpDelegate<T> lerpFunction) where T : IRangedLerpable
		{
			bool foundLower = false;
			bool foundUpper = false;
			T lowerBound = default;
			T upperBound = default;
			foreach (var test in collection)
			{
				if (targetValue < test.MinTargetValue)
				{
					if (foundLower)
					{
						if (targetValue - test.MaxTargetValue < targetValue - lowerBound.MaxTargetValue) lowerBound = test;
					}
					else
					{
						lowerBound = test;
					}
					foundLower = true;
				}
				else if (targetValue > test.MaxTargetValue)
				{
					if (foundUpper)
					{
						if (targetValue - test.MaxTargetValue > targetValue - upperBound.MaxTargetValue) upperBound = test;
					}
					else
					{
						upperBound = test;
					}
					foundUpper = true;
				}
				else
				{
					return test;
				}
			}
			if (foundLower && !foundUpper) return lowerBound;
			if (foundUpper && !foundLower) return upperBound;

			float lerpVal = (targetValue - lowerBound.MaxTargetValue) / (upperBound.MinTargetValue - lowerBound.MaxTargetValue);
			return lerpFunction(lowerBound, upperBound, lerpVal);
		}
	}
	public interface IRangedLerpable
	{
		float MinTargetValue { get; }
		float MaxTargetValue { get; }
	}

}