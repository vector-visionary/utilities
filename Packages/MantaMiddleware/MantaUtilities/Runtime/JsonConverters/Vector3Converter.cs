﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MantaMiddleware.MantaUtilities.JsonConverters
{
	public class Vector3Converter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(UnityEngine.Vector3);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject obj = JObject.Load(reader);
            UnityEngine.Vector3 rtn = new UnityEngine.Vector3(obj["x"].Value<float>(), obj["y"].Value<float>(), obj["z"].Value<float>());

            return rtn;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            UnityEngine.Vector3 v3 = (UnityEngine.Vector3)value;
            JObject obj = new JObject();
            obj["x"] = v3.x;
            obj["y"] = v3.y;
            obj["z"] = v3.z;
            obj.WriteTo(writer);
        }
    }
}