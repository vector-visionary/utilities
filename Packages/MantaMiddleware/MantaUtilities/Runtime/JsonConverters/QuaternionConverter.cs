﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MantaMiddleware.MantaUtilities.JsonConverters
{
	public class QuaternionConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(UnityEngine.Quaternion);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject obj = JObject.Load(reader);
            UnityEngine.Quaternion rtn = new UnityEngine.Quaternion(obj["x"].Value<float>(), obj["y"].Value<float>(), obj["z"].Value<float>(), obj["w"].Value<float>());

            return rtn;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            UnityEngine.Quaternion quaternion = (UnityEngine.Quaternion)value;
            JObject obj = new JObject();
            obj["x"] = quaternion.x;
            obj["y"] = quaternion.y;
            obj["z"] = quaternion.z;
            obj["w"] = quaternion.w;
            obj.WriteTo(writer);
        }
    }
}