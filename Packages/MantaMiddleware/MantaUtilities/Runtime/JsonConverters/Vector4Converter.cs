﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MantaMiddleware.MantaUtilities.JsonConverters
{
	public class Vector4Converter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(UnityEngine.Vector4);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject obj = JObject.Load(reader);
            UnityEngine.Vector4 rtn = new UnityEngine.Vector4(obj["x"].Value<float>(), obj["y"].Value<float>(), obj["z"].Value<float>(), obj["w"].Value<float>());

            return rtn;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            UnityEngine.Vector4 vector4 = (UnityEngine.Vector4)value;
            JObject obj = new JObject();
            obj["x"] = vector4.x;
            obj["y"] = vector4.y;
            obj["z"] = vector4.z;
            obj["w"] = vector4.w;
            obj.WriteTo(writer);
        }
    }
}