﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MantaMiddleware.MantaUtilities.JsonConverters
{
	public class ColorConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(UnityEngine.Color);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject obj = JObject.Load(reader);
            float alpha = 1f;
            if (obj.ContainsKey("a")) alpha = obj["a"].Value<float>();
            UnityEngine.Color rtn = new UnityEngine.Color(obj["r"].Value<float>(), obj["g"].Value<float>(), obj["b"].Value<float>(), alpha);

            return rtn;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            UnityEngine.Color col = (UnityEngine.Color)value;
            JObject obj = new JObject();
            obj["r"] = col.r;
            obj["g"] = col.g;
            obj["b"] = col.b;
            obj["a"] = col.a;
            obj.WriteTo(writer);
        }
    }
}