﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MantaMiddleware.MantaUtilities.JsonConverters
{
	public class Vector2Converter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(UnityEngine.Vector2);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject obj = JObject.Load(reader);
            UnityEngine.Vector2 rtn = new UnityEngine.Vector2(obj["x"].Value<float>(), obj["y"].Value<float>());

            return rtn;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            UnityEngine.Vector2 v2 = (UnityEngine.Vector2)value;
            JObject obj = new JObject();
            obj["x"] = v2.x;
            obj["y"] = v2.y;
            obj.WriteTo(writer);
        }
    }
}