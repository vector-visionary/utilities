﻿using System.Collections.Generic;
using UnityEngine;

namespace MantaMiddleware.Utilities
{
	public static class RandomUtil
	{
		public static T RandomItem<T>(this T[] array)
		{
			if (array == null || array.Length == 0) return default;
			return array[Random.Range(0, array.Length)];
		}
		public static T RandomItem<T>(this List<T> array)
		{
			if (array == null || array.Count == 0) return default;
			return array[Random.Range(0, array.Count)];
		}

		public static Vector3 RandomPositionInCollider(this Collider col)
		{
			//not super efficient but IS super versatile
			for (int attempt=0; attempt < 100; attempt++)
			{
				var thisPos = col.bounds.center + Vector3.Scale(col.bounds.extents, new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)));
				if (col.ClosestPoint(thisPos) == thisPos) return thisPos;
			}
			//fallback if none of the random points work
			return col.bounds.center;
		}
	}
}