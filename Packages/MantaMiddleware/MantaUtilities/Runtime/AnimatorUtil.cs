﻿using UnityEngine;

namespace MantaMiddleware.Utilities
{
	public static class AnimatorUtil
	{
		public static bool HasParameter(this Animator animator, string paramName)
		{
			foreach (AnimatorControllerParameter param in animator.parameters)
			{
				if (param.name == paramName)
					return true;
			}
			return false;
		}

		public static bool HasParameter(this Animator animator, string paramName, AnimatorControllerParameterType paramType)
		{
			foreach (AnimatorControllerParameter param in animator.parameters)
			{
				if (param.name == paramName && param.type == paramType)
					return true;
			}
			return false;
		}
	}
}