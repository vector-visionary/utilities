using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MantaMiddleware.MantaUtilities.JsonConverters
{
    public static class JsonHelpers
    {
        public static event Action<HashSet<JsonConverter>> OnCompileAllConverters;
        public static JsonSerializerSettings GetUnityProjectJsonSerializerSettings()
        {
            var jss = new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate };
            jss.Converters.Add(new Vector2Converter());
            jss.Converters.Add(new Vector3Converter());
            jss.Converters.Add(new Vector4Converter());
            jss.Converters.Add(new ColorConverter());
            jss.Converters.Add(new QuaternionConverter());
            
            //compile converters from client code
            var tempConvList = new HashSet<JsonConverter>();
            OnCompileAllConverters?.Invoke(tempConvList);
            foreach (var converter in tempConvList)
            {
                jss.Converters.Add(converter);
            }
            return jss;
        }
    }
}