﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Profiling;

namespace MantaMiddleware.Utilities
{
	/// <summary>
	/// A set of "displayers" which will be maintained to match a set of items of the given type.
	/// Displayers will be created and destroyed (using the provided delegates) as needed to fit the size of the source data.
	/// When being updated (using <see cref="SetToMatch(IEnumerable{T}, Action{T, IFocusableDisplayer{T}})"/>,
	/// the set will attempt to keep displayers associated with the same source items when possible, to minimize state-switching.
	/// </summary>
	/// <typeparam name="T">The source of the data to be displayed.</typeparam>
	public class MatchedDisplayerSet<T>
	{
		/// <summary>
		/// Creates an instance of a displayer set.
		/// </summary>
		/// <param name="spawner">The method to create or retrieve a new displayer. 
		/// This delegate should return an instantiated and uncommitted, in-scene object with the <see cref="IDisplayer{T}"/> interface.
		/// If this is part of the UI, this spawner should also correctly parent the spawned displayer.
		/// </param>
		/// <param name="remover">The method to destroy or deactivate a displayer that is no longer needed.</param>
		public MatchedDisplayerSet(DisplayerSpawner spawner, DisplayerRemover remover)
		{
			this.spawner = spawner;
			this.remover = remover;
		}
		public delegate UniTask<IDisplayer<T>> DisplayerSpawner();
		private readonly DisplayerSpawner spawner;
		public delegate UniTask DisplayerRemover(IDisplayer<T> displayer);
		private readonly DisplayerRemover remover;

		public bool setTransformIndexBasedOnListOrder = false;

		private Dictionary<T, IDisplayer<T>> existingDisplayers = new Dictionary<T, IDisplayer<T>>();

		private Dictionary<T, IDisplayer<T>> newExistingDisplayers = new Dictionary<T, IDisplayer<T>>();
		private Dictionary<T, IDisplayer<T>> oldExistingDisplayers;
		private HashSet<IDisplayer<T>> unusedDisplayers = new HashSet<IDisplayer<T>>();

		/// <summary>
		/// Sets all displayers belonging to this set to match the provided list, creating or destroying displayers as needed.
		/// </summary>
		/// <param name="listOfMasterObjects">The list of source data items to match.</param>
		public async UniTask SetToMatch(IEnumerable<T> listOfMasterObjects)
		{
			Profiler.BeginSample($"SetToMatch for {typeof(T)}");
			oldExistingDisplayers = existingDisplayers;
			unusedDisplayers.Clear();
			foreach (var d in oldExistingDisplayers.Values)
				unusedDisplayers.Add(d);

			newExistingDisplayers.Clear();
			int currentIndex = 0;
			foreach (var target in listOfMasterObjects)
			{
				if (newExistingDisplayers.ContainsKey(target))
				{
					UnityEngine.Debug.LogWarning($"The list sent to matched displayer contains multiple copies of an object {target}. Skipping.");
				}
				else
				{
					IDisplayer<T> thisDisplayer;
					if (oldExistingDisplayers.TryGetValue(target, out thisDisplayer))
					{
						unusedDisplayers.Remove(thisDisplayer);
					}
					else
					{
						thisDisplayer = await spawner();
					}

					try
					{
						await thisDisplayer.SetToDisplay(target);
					}
					catch (MissingReferenceException)
					{
						// the displayer was destroyed, maybe by a scene being unloaded; if we simply don't keep it in our lists (e.g. not add to newExistingDisplayers) we should be fine in the future
						continue;
					}
					catch (Exception e)
					{
						Debug.LogError($"{e.GetType()} in displayer for {typeof(T)} in next log message:");
						Debug.LogException(e);
					}
					newExistingDisplayers.Add(target, thisDisplayer);

					if (setTransformIndexBasedOnListOrder && thisDisplayer is Component c)
					{
						c.transform.SetSiblingIndex(currentIndex+1);
					}

					currentIndex++;


				}
			}
			existingDisplayers = newExistingDisplayers;
			newExistingDisplayers = oldExistingDisplayers;
			foreach (var unused in unusedDisplayers)
			{
				remover(unused);
			}
			Profiler.EndSample();
		}

		/// <summary>
		/// Updates a single already-existing displayer with new data for an object it's
		/// already displaying. Only possible with reference types; will fail otherwise.
		/// </summary>
		/// <param name="backingDataToUpdate">The data object with the new data to update with.</param>
		/// <returns>True if the object was found and successfully updated; false otherwise.</returns>
		public async UniTask<bool> UpdateSingle(T backingDataToUpdate)
		{
			foreach (var displayerKVP in existingDisplayers)
			{
				if (displayerKVP.Key.GetHashCode() == backingDataToUpdate.GetHashCode())
				{
					await displayerKVP.Value.SetToDisplay(backingDataToUpdate);
					return true;
				}
			}

			return false;
		}

		public IDisplayer<T> GetDisplayerFor(T data)
		{
			foreach (var displayerKVP in existingDisplayers)
			{
				if (displayerKVP.Key.GetHashCode() == data.GetHashCode())
				{
					return displayerKVP.Value;
				}
			}

			return null;
		}
	}

	/// <summary>
	/// An interface required for any displayer to be able to display an item of type <see cref="T"/>.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IDisplayer<T>
	{
		/// <summary>
		/// Sets this displayer to display the data of the given target.
		/// </summary>
		/// <param name="target">The data source.</param>
		UniTask SetToDisplay(T target);
	}
}