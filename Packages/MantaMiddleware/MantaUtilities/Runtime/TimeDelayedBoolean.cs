﻿using System;
using UnityEngine;

namespace MantaMiddleware.Utilities
{
	/// <summary>
	/// A boolean value that can have an "in-between" value for a set period of time when its value is changed.
	/// </summary>
	[Serializable]
	public class TimeDelayedBoolean
	{
		public bool value
		{
			get
			{
				if (Time.time < timeValueLastChanged + delay) return valueDuringDelay;
				return internalValue;
			}
			set
			{
				if (value != internalValue)
				{
					internalValue = value;
					timeValueLastChanged = Time.time;
				}
			}
		}
		public float delay = 0.5f;
		public bool valueDuringDelay = false;
		[SerializeField] private bool internalValue = false;
		[SerializeField] private float timeValueLastChanged = -999f;

		public static implicit operator bool(TimeDelayedBoolean tdb)=>tdb.value;
	}
}