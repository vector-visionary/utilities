﻿using UnityEngine;

namespace MantaMiddleware.Utilities
{

	public static class VectorUtilities
	{
		public static T GetClosest<T>(T[] options, Vector3 origin) where T : Component
		{
			T currentBest = default;
			float currentBestSqDist = 99999f;
			foreach (var thisOption in options)
			{
				float thisSqDist = (thisOption.transform.position - origin).sqrMagnitude;
				if (thisSqDist < currentBestSqDist)
				{
					currentBest = thisOption;
					currentBestSqDist = thisSqDist;
				}
			}
			return currentBest;
		}

		public static Vector3 FlattenAndRetainMagnitude(this Vector3 vector)
		{
			if (vector.y == 0f) return vector;

			float inputMagnitude = vector.magnitude;
			vector.y = 0f;
			vector.Normalize();
			vector *= inputMagnitude;
			return vector;
		}
		public static Vector3 FlattenAndRetainMagnitude(this Vector3 vector, Vector3 up)
		{

			float inputMagnitude = vector.magnitude;
			vector = FlattenAlongAxis(vector, up);
			vector.Normalize();
			vector *= inputMagnitude;
			return vector;
		}

		public static Vector3 FlattenAlongAxis(this Vector3 vector, Vector3 up)
		{
			return Vector3.ProjectOnPlane(vector, up);
		}

		public static Vector3 IsolateAlongAxis(this Vector3 vector, Vector3 up)
		{
			return Vector3.Project(vector, up);
		}
	}
}