using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MantaMiddleware.Utilities
{
    public class DictionarySet<T, U> : IEnumerable<KeyValuePair<T, HashSet<U>>>
    {
        private Dictionary<T, HashSet<U>> internalData = new Dictionary<T, HashSet<U>>();

        public DictionarySet()
        {
            
        }

        public HashSet<U> this[T key]
        {
            get
            {
                if (internalData.TryGetValue(key, out var rtn))
                {
                    return rtn;
                }
                empty.Clear();
                return empty;
            }
        }

        private static readonly HashSet<U> empty = new HashSet<U>();

        public IEnumerable<T> Keys
        {
            get
            {
                foreach (var kvp in internalData)
                {
                    if (kvp.Value.Count > 0) 
                        yield return kvp.Key;
                }
            }
        }
        
        public void Add(T key, U obj)
        {
            if (!internalData.ContainsKey(key))
            {
                internalData.Add(key, new HashSet<U>());
            }
            var hashSet = internalData[key];
            if (!hashSet.Contains(obj)) hashSet.Add(obj);
        }

        public bool Contains(T key, U obj)
        {
            if (!internalData.ContainsKey(key)) return false;
            return (internalData[key].Contains(obj));
        }

        public bool TryGetOneItem(T key, out U rtn)
        {
            rtn = default;
            if (!internalData.ContainsKey(key)) return false;
            if (internalData[key].Count == 0) return false;
            
            rtn = internalData[key].First();
            return true;
        }

        public void ClearKey(T key)
        {
            if (internalData.TryGetValue(key, out var set))
            {
                set.Clear();
            }
        }

        public void Remove(T key, U obj)
        {
            if (internalData.TryGetValue(key, out var set))
            {
                if (set.Contains(obj)) set.Remove(obj);
            }
        }

        public void Clear()
        {
            foreach (var kvp in internalData)
            {
                kvp.Value.Clear();
            }
        }

        public int CountKey(T key)
        {
            if (internalData.TryGetValue(key, out var set))
            {
                return set.Count;
            }

            return 0;
        }

        public IEnumerator<KeyValuePair<T, HashSet<U>>> GetEnumerator()
        {
            foreach (var kvp in internalData)
                yield return kvp;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
