﻿using System.Collections.Generic;
using UnityEngine;

namespace MantaMiddleware.Utilities
{
	public static class TransformUtilities
	{

		/// <summary>
		/// Crawls up the hierarchy of the given transform until it finds the first instance of a component of the given type.
		/// </summary>
		/// <typeparam name="T">The type to search for (should be a Component or an interface implemented by a Component.</typeparam>
		/// <param name="target"></param>
		/// <returns></returns>
		public static T GetComponentAlongParentage<T>(this Transform target) 
		{
			Transform thisParent = target;
			while (thisParent != null)
			{
				T rtn = thisParent.GetComponent<T>();
				if (rtn != null) return rtn;
				thisParent = thisParent.parent;
			}
			return default;
		}

		/// <summary>
		/// Crawls up the hierarchy of the given transform until it finds all instances of a component of the given type.
		/// The array will be ordered with the one nearest to the target first in the array.
		/// </summary>
		/// <typeparam name="T">The type to search for (should be a Component or an interface implemented by a Component.</typeparam>
		/// <param name="target"></param>
		/// <returns></returns>
		public static T[] GetComponentsAlongParentageInOrder<T>(this Transform target)
		{
			List<T> rtn = new List<T>();
			Transform thisParent = target;
			while (thisParent != null)
			{
				var allThisObj = thisParent.GetComponents<T>();
				foreach (var adding in allThisObj)
				{
					rtn.Add(adding);
				}
				
				thisParent = thisParent.parent;
			}
			return rtn.ToArray();
		}
	}
}