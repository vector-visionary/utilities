using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;

namespace MantaMiddleware.Utilities
{
    /// <summary>
    /// A generalized, very easily implemented object pooling system for Unity.
    /// Simply replace all Instantiate calls for the desired object with ObjectPool.Instantiate,
    /// and all Destroy calls for that object with ObjectPool.Destroy.
    /// (All parameters should be compatible with Unity's Instantiate/Destroy methods)
    /// </summary>
    public class ObjectPool
    {
		#region STATIC
		public static ObjectPool GetPoolForPrefab(GameObject prefab)
		{
            if (!poolsByPrefab.ContainsKey(prefab))
            {
                poolsByPrefab.Add(prefab, new ObjectPool(prefab));
            }
            return poolsByPrefab[prefab];
        }

		public static ObjectPool GetPoolForReference(AssetReferenceGameObject reference)
		{
			if (!poolsByReference.ContainsKey(reference))
			{
				poolsByReference.Add(reference, new ObjectPool(reference));
			}

			return poolsByReference[reference];
		}
		private static Dictionary<GameObject, ObjectPool> poolsByPrefab = new Dictionary<GameObject, ObjectPool>();
        private static Dictionary<GameObject, ObjectPool> poolsByInstance = new Dictionary<GameObject, ObjectPool>();

        private static Dictionary<AssetReferenceGameObject, ObjectPool> poolsByReference =
	        new Dictionary<AssetReferenceGameObject, ObjectPool>();

        public static async UniTask<GameObject> Instantiate(AssetReferenceGameObject prefabReference)
        {
	        return await GetPoolForReference(prefabReference).InstantiateOrActivate(null);
        }
        public static async UniTask<GameObject> Instantiate(AssetReferenceGameObject prefabReference, Transform parent)
        {
	        return await GetPoolForReference(prefabReference).InstantiateOrActivate(parent);
        }
        public static async UniTask<GameObject> Instantiate(AssetReferenceGameObject prefabReference, Vector3 position, Quaternion rotation)
        {
	        var rtn = await GetPoolForReference(prefabReference).InstantiateOrActivate(null);
	        rtn.transform.position = position;
	        rtn.transform.rotation = rotation;
	        return rtn;
        }

        public static async UniTask<GameObject> Instantiate(GameObject prefab)
		{
            return await GetPoolForPrefab(prefab).InstantiateOrActivate(null);
		}

        public static async UniTask<GameObject> Instantiate(GameObject prefab, Transform parent)
		{
            return await GetPoolForPrefab(prefab).InstantiateOrActivate(parent);
		}

        public static async UniTask<T> Instantiate<T>(T prefabComponent) where T : MonoBehaviour
		{
            return (await GetPoolForPrefab(prefabComponent.gameObject).InstantiateOrActivate(null)).GetComponent<T>();
		}

        public static async UniTask<T> Instantiate<T>(T prefabComponent, Transform parent) where T : MonoBehaviour
        {
            return (await GetPoolForPrefab(prefabComponent.gameObject).InstantiateOrActivate(parent)).GetComponent<T>();
        }

        public static async UniTask<GameObject> Instantiate(GameObject prefab, Vector3 position, Quaternion rotation)
		{
            var rtn = await GetPoolForPrefab(prefab).InstantiateOrActivate(null);
            rtn.transform.position = position;
            rtn.transform.rotation = rotation;
            return rtn;
        }

        public static async UniTask<T> Instantiate<T>(T prefabComponent, Vector3 position, Quaternion rotation) where T : MonoBehaviour
		{
            var rtn = await GetPoolForPrefab(prefabComponent.gameObject).InstantiateOrActivate(null);
            rtn.transform.position = position;
            rtn.transform.rotation = rotation;
            return rtn.GetComponent<T>();
        }

        public static async UniTask Destroy<T>(T prefabComponent) where T : Component
        {
            if (prefabComponent != null)
            {
                await Destroy(prefabComponent.gameObject);
            }
        }
        public static async UniTask Destroy(GameObject instance)
		{
            if (poolsByInstance.TryGetValue(instance, out var pool))
			{
                await pool.Deactivate(instance);
			}
            else
			{
                Debug.LogWarning($"Tried to destroy {instance.name} using ObjectPool but we don't have a pool for this instance. Using regular Destroy.");
                if (Application.isPlaying)
                {
	                GameObject.Destroy(instance);
                }
                else
                {
	                GameObject.DestroyImmediate(instance);
                }
			}
		}

        /// <summary>
        /// Destroys all inactive instances of this prefab; for here on, any active instances will be destroyed instead of deactivated.
        /// To be called when you expect to be finished spawning new ones.
        /// Can be undone simply by spawning another new one.
        /// </summary>
        /// <param name="prefab"></param>
        public static void ClearPool(GameObject prefab)
		{
            if (poolsByPrefab.TryGetValue(prefab, out var pool))
			{
                pool.Clear();
			}
		}

		#endregion STATIC

		private async UniTask ResetPoolObject(GameObject parent)
		{
            parent.SetActive(true);
            if (hasResettersInHierarchy)
            {
	            HashSet<UniTask> allResetTasks = new HashSet<UniTask>();
	            var resetters = resettersPerInstance[parent];
                foreach (var resetter in resetters)
                {
                    allResetTasks.Add(resetter.PoolStart());
                }

                await UniTask.WhenAll(allResetTasks);
            }
		}

        // CLASS FUNCTIONS
        private AssetReferenceGameObject myAssetReference;

        private Dictionary<GameObject, HashSet<IPoolResetter>> resettersPerInstance =
	        new Dictionary<GameObject, HashSet<IPoolResetter>>();

        private Dictionary<GameObject, HashSet<IPoolDestroyer>> destroyersPerInstance =
	        new Dictionary<GameObject, HashSet<IPoolDestroyer>>();

        private HashSet<AsyncOperationHandle<GameObject>> myAddressableHandles =
	        new HashSet<AsyncOperationHandle<GameObject>>();
        private GameObject myPrefab;
        private Scene homeScene;
        private bool isCleared = false;
        private bool hasResettersInHierarchy;
        private bool hasDestroyersInHierarchy;
        private ObjectPool(GameObject prefab)
        {
            myPrefab = prefab;
            hasResettersInHierarchy = prefab.GetComponentInChildren<IPoolResetter>() != null;
            hasDestroyersInHierarchy = prefab.GetComponentInChildren<IPoolDestroyer>() != null;
        }

        private ObjectPool(AssetReferenceGameObject reference)
        {
	        LoadAssetReference(reference).Forget();
        }

        private async UniTask LoadAssetReference(AssetReferenceGameObject reference)
        {
	        myAssetReference = reference;
	        var loadHandle = Addressables.LoadAssetAsync<GameObject>(reference);
	        myAddressableHandles.Add(loadHandle);
	        await loadHandle;
	        myPrefab = loadHandle.Result;
	        hasResettersInHierarchy = myPrefab.GetComponentInChildren<IPoolResetter>() != null;
        }

        /// <summary>
        /// Prespawns inactively the given number of instances ready to be activated later.
        /// Call this during opportune moments (load scenes, etc) to avoid loading stutters later.
        /// </summary>
        /// <param name="count">The number of instances to spawn.</param>
        public async UniTask Prespawn(int count)
        {
	        var tasks = new HashSet<UniTask>();
            for (int i=0; i<count; i++)
            {
	            tasks.Add(SpawnNewObject(null, false));
            }

            await UniTask.WhenAll(tasks);
        }

        /// <summary>
        /// Prespawns up to the given number of instances ready to be activated later.
        /// Call this during opportune moments (load scenes, etc) to avoid loading stutters later.
        /// </summary>
        /// <param name="maxCount">The maximum number of instances to spawn up to.</param>
        public async UniTask PrespawnToMaximum(int maxCount)
		{
            while (activeInstances.Count + inactiveInstances.Count < maxCount)
			{
                await SpawnNewObject(null, false);
			}
		}

        /// <summary>
        /// Sets the scene into which this prefab will always be spawned,
        /// and optionally migrates existing instances to this scene
        /// (though this migration can cause unpredictable behavior, and it's recommended to set the home scene before creating an object).
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="migrateExistingInstances"></param>
        public void SetHomeScene(Scene scene, bool migrateExistingInstances)
		{
            homeScene = scene;
            if (migrateExistingInstances)
			{
                foreach (var instance in activeInstances)
                {
                    SceneManager.MoveGameObjectToScene(instance, homeScene);
                }
                foreach (var instance in inactiveInstances)
                {
                    SceneManager.MoveGameObjectToScene(instance, homeScene);
                }
            }
        }


        private readonly HashSet<GameObject> activeInstances = new HashSet<GameObject>();
        private readonly Stack<GameObject> inactiveInstances = new Stack<GameObject>();

		private async UniTask<GameObject> InstantiateOrActivate(Transform parent)
		{
			foreach (var myAddressableHandle in myAddressableHandles)
			{
				if (myAddressableHandle.IsValid())
				{
					await myAddressableHandle;
				}
			}

			isCleared = false;
			while (inactiveInstances.Count > 0)
			{
                GameObject thisInstance = inactiveInstances.Pop();
                if (thisInstance == null) continue; //if the pool objects have been destroyed, let's remove the nulls from the pool until we find a real one or empty the list
                activeInstances.Add(thisInstance);
                thisInstance.transform.SetParent(parent, false);
                await ResetPoolObject(thisInstance);
                return thisInstance;
			}

            return await SpawnNewObject(parent, true);
		}

        private async UniTask<GameObject> SpawnNewObject(Transform parent, bool toBeActive)
		{
            Scene returnToScene = SceneManager.GetActiveScene();
            if (homeScene != null && homeScene.isLoaded)
			{
                SceneManager.SetActiveScene(homeScene);
			}
            
            GameObject rtn;
            if (myAssetReference != null)
            {
	            var handle = Addressables.InstantiateAsync(myAssetReference, parent);
	            myAddressableHandles.Add(handle);
	            rtn = await handle;
            }
            else
            {
	            rtn = GameObject.Instantiate(myPrefab, parent);
            }

            rtn.name = myPrefab.name; // Ugh, the "(clone)" thing is seriously irritating
            poolsByInstance.Add(rtn, this);
            if (hasResettersInHierarchy)
            {
	            resettersPerInstance.Add(rtn, new HashSet<IPoolResetter>(rtn.GetComponentsInChildren<IPoolResetter>()));
            }

            if (hasDestroyersInHierarchy)
            {
	            destroyersPerInstance.Add(rtn, new HashSet<IPoolDestroyer>(rtn.GetComponentsInChildren<IPoolDestroyer>()));
            }
            
            if (toBeActive)
            {
                activeInstances.Add(rtn);
                await ResetPoolObject(rtn);
            }
            else
			{
                FinalizeDeactivate(rtn);
			}
            if (homeScene != null && returnToScene.isLoaded)
			{
                SceneManager.SetActiveScene(returnToScene);
			}
            return rtn;
        }

        private async UniTask Deactivate(GameObject instance)
        {
	        var destroyers = destroyersPerInstance[instance];
            var allDestroyTasks = new HashSet<UniTask>();
            foreach (var destroyer in destroyers)
            {
                allDestroyTasks.Add(destroyer.PoolStop());
            }

            await UniTask.WhenAll(allDestroyTasks);
            FinalizeDeactivate(instance);
		}

        private void FinalizeDeactivate(GameObject instance)
		{
            if (isCleared)
            {
                GameObject.Destroy(instance);
            }
            else
            {
                instance.SetActive(false);
                activeInstances.Remove(instance);
                inactiveInstances.Push(instance);
            }
        }

        private void Clear()
		{
            isCleared = true;
            foreach (var instance in inactiveInstances)
			{
                GameObject.Destroy(instance);
			}
            inactiveInstances.Clear();
            foreach (var myAddressableHandle in myAddressableHandles)
            {
	            if (myAddressableHandle.IsValid())
	            {
		            Addressables.Release(myAddressableHandle);
	            }
            }
		}
    }

    /// <summary>
    /// If any of these exist on a pooled object, then <see cref="PoolStart"/> will be called
    /// immediately before activating a pooled object.
    /// </summary>
    public interface IPoolResetter
	{
        UniTask PoolStart();
	}


    /// <summary>
    /// If any of these exist on a pooled object, then <see cref="PoolStop"/> will be called
    /// immediately before deactivating a pooled object.
    /// </summary>
    public interface IPoolDestroyer
    {
        UniTask PoolStop();
    }
}
