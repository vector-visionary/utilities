# Overview

This is a set of common utility classes that many other MantaMiddleware packages use.

# DictionarySet<A, B>

What happens if a Dictionary and HashSet have a baby. Each key in the dictionary is associated with a set of values, rather than just one. This is really just a convenient way to implement Dictionary<A, HashSet< B>> without having to manage the HashSet yourself.

Usage should be self-explanatory if familiar with Dictionary and HashSet. The one possibly unexpected caveat is that if all items in the set are removed, it will return an empty HashSet; if a key doesn't exist, it will *also* return an empty HashSet. This is to allow more consistent usage (e.g. being able to iterate over the values of a key) without needing to care whether a key has ever existed.

---

# MatchedDisplayerSet<T>

A utility to pair "displayers" (which may be UI objects, for example) with backing data, reusing displayers as they are no longer needed, and adding and removing them to match the count of backing data objects.

Pairs will with ObjectPool

## Standard Usage

```csharp
[SerializeField] private YourDisplayerClass myPrefab; // YourDisplayerClass must implement interface IDisplayer<YourDataClass>
private MatchedDisplayerSet<YourDataClass> myDisplayers; // Note that this cannot be initialized in-line for class members
List<YourDataClass> allMyData = new YourDataClass[]{new YourDataClass("foo"), new YourDataClass("bar") }; // This can be any IEnumerable<YourDataClass> derivative

void Awake() {
    myDisplayers = new MatchedDisplayerSet<YourDataClass>(
    // spawner method (must return type YourDisplayerClass)
        () => ObjectPool.Instantiate(myPrefab),
    // remover method (takes the object to be removed)
        (destroying) => ObjectPool.Destroy(destroying)
    );
}

void Update() {
    myDisplayers.SetToMatch(allMyData);
}
```

---

# ObjectPool

ObjectPool is an instance pooling package that is designed to be usable as a drop-in replacement for Unity's object instantiation and destruction functions. It also functions with addressable references, and it has a few additional pool-management functions in addition.

ObjectPool also supports async UniTask functions that react to being spawned and destroyed, including automatically delaying the destruction of the object  until these functions have run their course. ("Destruction" here usually means returning the object to the inactive pool.)

All relevant operations are asynchronous and can be awaited.

If an object is destroyed via means outside of the ObjectPool system (e.g. by calling GameObject.Destroy or by a scene being unloaded), ObjectPool will deal with it gracefully. Obviously the instance would no longer be available for reactivation, but the destroyed reference will be removed from the pool and will not cause extra errors or disruption of functionality.

## Standard usage via static functions
```csharp

GameObject.Instantiate(prefab, position, rotation);
// replace with
ObjectPool.Instantiate(prefab, position, rotation);
// OR
ObjectPool.Instantiate(prefabAssetReference, position, rotation);

// Consider using an async method and awaiting this instantiation,
// since it may take time to load the reference. 
// If you need the instance reference after spawning, you must await it.
var instance = GameObject.Instantiate(prefab);
// replace with
var instance = await ObjectPool.Instantiate(prefab);


// To return the instance to the deactivated-object pool for reuse
Destroy(gameObject);
// replace with
ObjectPool.Destroy(gameObject);

// If you have direct references to components on the object, you can use that:
Destroy(someCollider.gameObject);
// replace with
ObjectPool.Destroy(someCollider);
// Note that this is different from Unity standard "Destroy" behavior on a Component, which will destroy the component itself; this method destroys the GameObject.


// You can directly reference a prefab by a required component type,
// and when instantiated, that component on the instantiated object
// is what will be returned.
var instance = Instantiate(prefabSomeComponentDirectReference);
var instanceComponent = instance.GetComponent<SomeComponent>();
// replace with
var instanceComponent = await ObjectPool.Instantiate(prefabSomeComponentDirectReference);

// When you are done using a pool for a given type of object:
ObjectPool.ClearPool(prefab);
// This will destroy all inactive instances. If any active instances remain,
// they will be really destroyed the next time ObjectPool.Destroy is called on them.
// When there are no instances remaining, the prefab reference will be unloaded/forgotten.
```

## IPoolResetter

If your object has values or objects that need to be reset before the instance is reused (say, setting an enemy's health to 100%), use this interface. The interface can be on **any** component anywhere in the prefab to be valid. When instantiating, all resetters found on the prefab will be awaited before the instance is returned to the code block that requested the instance via ObjectPool.Instantiate.

```
public class EnemyGrunt : MonoBehaviour, IPoolResetter {
   public float totalHealth = 100f;
   public float currentHealth = 100f;
   public async UniTask PoolStart() {
      currentHealth = totalHealth;
   }
}
```


## IPoolDestroyer

The other side of IPoolResetter, the destroyer interface will be called upon anytime the ObjectPool.Destroy method is called. One special feature of this interface is that it will actually delay the destruction (or inactivation) of the instance until the IPoolDestroyer's function has completed. So, for example, you can have an instance shrink to nothing upon destruction; the destruction will wait until the shrinking is complete.

```
public class EnemyGrunt : MonoBehaviour, IPoolResetter, IPoolDestroyer {
   public float totalHealth = 100f;
   public float currentHealth = 100f;
   public async UniTask PoolStart() {
      transform.localScale = Vector3.one;
      currentHealth = totalHealth;
   }
   public async UniTask PoolStop() {
      for (float t = 0f; t < 1f; t += Time.deltaTime) {
         transform.localScale = Vector3.one * (1f - t);
         await UniTask.DelayFrame(1);
      }
   }
}
```

## Pool instance functions (advanced)

There are a few extra functions available by accessing the ObjectPool object directly, rather than via the standard static methods. This pool can be found via:
```
var pool = ObjectPool.GetPoolForPrefab(prefab);
var pool = ObjectPool.GetPoolForReference(prefabAssetReference);
```
If the pool does not yet exist, it will be created; in the case of the reference, this means loading the reference as well.

Once you have the reference, here are some available functions:

```
// Create 10 inactive instances for fast activation later
pool.Prespawn(10);

// Ensure that 10 instances total (including inactive) exist
pool.PrespawnToMaximum(10);

// Set the scene into which these instances should spawn, 
// and move existing instances to that scene. 
// (If the scene is unloaded, these instances will be destroyed.
// If that happens, the pool will notice they're destroyed and
// remove them from its reference list.)
pool.SetHomeScene(someScene, true);
```
# RandomUtil

A couple of simple convenience functions for randomization.

**RandomItem**: Given an array, this will return a random item from that array. If the array is null or empty, this will gracefully return null.

**RandomPositionInCollider**: Generates a random position that is contained within a Collider of any type/shape. Not efficient, just convenient.

# RangedLerpableSet

Allows you to create and lerp through a "curve" containing many points of data. This is not too different from something like an AnimationCurve, but the lerping function and the Type being lerped between can both be fully customized.

# TimeDelayedBoolean

A boolean value that, when changed, returns an interim value instead for a set period of time.

# VectorUtilities

Some convenient methods for easily dealing with certain Vector3 operations. In some cases, these may just be plain-English rebrandings of the confusingly-named mathematical operations.