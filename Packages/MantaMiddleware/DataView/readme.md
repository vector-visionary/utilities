# DataView

The DataView system is a way of matching data class to UI objects without assigning references manually. In principal, the data class is tagged with attributes (primarily **DataViewProp**). UI elements are given **DataViewFields**, which are assigned to one of these "props" by name.

## Terminology

### Prop

A "prop" is part of the data class - a piece of information you wish to display in the UI. It is short for "property", but the term "prop" is used throughout the system to distinguish is from C# properties, primarily because it can be a property, field, or method.

### Field

A "field" is the UI element that displays data. Every field has the DataViewField component. (This term is not related to the C# term field.) A field has one or more connectors that link the data to the actual UI; the DataViewField class's primary job is to manage these connectors.

### Connector

A connector is the final "branch" of the system, forming the final connection from a field to the UI element displaying its data.

### Provider

A provider is a controller class (one you'll create) that is responsible for telling the DataViewLink what information is being shown. Related, a "sample provider" provides sample data that can be used during development of the UI elements to ensure that the UI responds to data in an expected way.

### Data Source

The class which holds the actual data being displayed. Important field, properties, and methods in this class should be marked with attributes such as DataViewProp to expose them to the DataView system's classes.


## Usage

TODO

## Attributes

TODO

## Classes

TODO

