# DataView Version History


## 1.0

* Initial implementation

# Future Roadmap

## 1.x

* More field connectors, to bring parity with legacy PropertyLister system
* Bugfixes
* Type adapters (including UnitsNet support)
* A direct "SetDataSource" method without needing to be a IDataProvider

## 2.x

* Implement IterateProperties and generic setting
* Implement addressing of sub-values (e.g. prop address can be PositionData.x, assuming x is a prop within PositionData)
* Optionally support non-DataViewProp tagged properties