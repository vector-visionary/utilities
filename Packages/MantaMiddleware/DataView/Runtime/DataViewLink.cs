using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Utilities;
using UnityEngine;

namespace MantaMiddleware.DataView
{
    public class DataViewLink : MonoBehaviour
    {
        private void Awake()
        {
            BuildProvidersList();
            BuildFieldsList();
        }

        [ExecuteInEditMode]
        private void Update()
        {
            if (!Application.isPlaying)
            {
                BuildProvidersList();
                BuildSampleProvidersList();
                BuildFieldsList();
            }
            UpdateBlockingState();
        }

        private float providersLastGeneratedAt = -99999f;

        private void VerifyProvidersList()
        {
            if (Time.realtimeSinceStartup > providersLastGeneratedAt + 5f)
            {
                BuildProvidersList();
            }
        }

        private void BuildProvidersList()
        {
            providersLastGeneratedAt = Time.realtimeSinceStartup;
            _allProviders.Clear();
            var providerArray = GetComponents<IDataProvider>();

            foreach (var provider in providerArray)
            {
                foreach (var dataSourceType in provider.GetDataSourceTypes())
                {
                    _allProviders.Add(provider, DataViewUtilities.GetDataSourceInformation(dataSourceType));
                }

                if (Application.isPlaying)
                {
                    provider.RegisterDataUpdateTrigger(RequestUpdateAllProviders);
                }
            }
        }

        private void BuildSampleProvidersList()
        {
            _allSampleDataProviders.Clear();

            var sampleProvidersArray = GetComponents<ISampleDataProvider>();
            foreach (var provider in sampleProvidersArray)
            {
                foreach (var dataSourceType in provider.GetDataSourceTypes())
                {
                    _allSampleDataProviders.Add(provider, DataViewUtilities.GetDataSourceInformation(dataSourceType));
                }
            }
        }

        private void BuildFieldsList()
        {
            _allFields.Clear();
            foreach (var field in GetComponentsInChildren<DataViewField>(true))
            {
                if (field.ParentLink == this)
                {
                    foreach (var propAddress in field.GetRequestedPropAddresses())
                    {
                        if (!DoesPropExist(propAddress))
                        {
                            Debug.LogError($"MarkErrorOnField({field}, {propAddress}, OnFindingProps, NullReferenceException)");
                            MarkErrorOnField(field, propAddress, FieldErrorPhase.OnFindingProps,
                                new NullReferenceException());
                        }
                        else
                        {
                            _allFields.Add(propAddress, field);
                        }
                    }
                }
            }
        }

        private readonly HashSet<DataViewField> _fieldsToFinalize = new HashSet<DataViewField>();

        private async UniTask RequestUpdateAllProviders()
        {
            //await AwaitCurrentBlockingTasks();
            //AddBlockingTask(UpdateAllProviders());
            UpdateAllProviders();
        }

        private HashSet<UniTask> blockingTasks = new HashSet<UniTask>();

        public void AddBlockingTask(UniTask task)
        {
            blockingTasks.Add(task);
        }
        public async UniTask AwaitCurrentBlockingTasks()
        {
            await UniTask.WhenAll(blockingTasks);
            blockingTasks.Clear();
        }


        private bool wasBlocked = false;

        private void UpdateBlockingState()
        {
            return; // TODO: fix that multi-blocking issue
            blockingTasks.RemoveWhere((task => task.Status != UniTaskStatus.Pending));
            bool blocked = blockingTasks.Count > 0;
            Debug.Log($"Changing blocked state from {wasBlocked} to {blocked} {blockingTasks.Count}");
            if (blocked != wasBlocked)
            {
                foreach (var fieldsKVP in _allFields)
                {
                    foreach (var field in fieldsKVP.Value)
                    {
                        field.SetBusyState(blocked);
                    }
                }
            }

            wasBlocked = blocked;
        }

        internal IEnumerable<IBusyStateResponder> GetUniversalBlockingResponders()
        {
            foreach (var responder in GetComponents<IBusyStateResponder>()) yield return responder;
        }

        private async UniTask UpdateAllProviders()
        {
            var allTasks = new HashSet<UniTask>();
            _fieldsToFinalize.Clear();
            foreach (var provider in _allProviders.Keys)
            {
                var data = provider.GetData();
                foreach (var sourceType in _allProviders[provider])
                {
                    if (sourceType.DataSourceType == data.GetType())
                    {
                        foreach (var propInfo in sourceType.AllProps)
                        {
                            allTasks.Add(UpdateFieldsWithData(propInfo, sourceType, data));
                        }
                    }
                }
            }

            foreach (var field in _fieldsToFinalize)
            {
                allTasks.Add(field.FinalizeSetting());
            }

            await UniTask.WhenAll(allTasks);
        }

        public async UniTask UpdateSingleProvider(IDataProvider provider)
        {
            var allTasks = new HashSet<UniTask>();
            _fieldsToFinalize.Clear();

            var data = provider.GetData();
            foreach (var sourceType in _allProviders[provider])
            {
                if (sourceType.DataSourceType == data.GetType())
                {
                    foreach (var propInfo in sourceType.AllProps)
                    {
                        allTasks.Add(UpdateFieldsWithData(propInfo, sourceType, data));
                    }
                }
            }

            foreach (var field in _fieldsToFinalize)
            {
                allTasks.Add(field.FinalizeSetting());
            }

            await UniTask.WhenAll(allTasks);
        }

        public async UniTask UpdateWithData(object data)
        {
            var allTasks = new HashSet<UniTask>();
            _fieldsToFinalize.Clear();

            foreach (var provider in _allProviders)
            {
                foreach (var sourceType in provider.Value)
                {
                    if (sourceType.DataSourceType == data.GetType())
                    {
                        foreach (var propInfo in sourceType.AllProps)
                        {
                            allTasks.Add(UpdateFieldsWithData(propInfo, sourceType, data));
                        }
                    }
                }
            }

            foreach (var field in _fieldsToFinalize)
            {
                allTasks.Add(field.FinalizeSetting());
            }

            await UniTask.WhenAll(allTasks);
            
        }

        internal void UpdateWithSampleData(object data)
        {
            BuildFieldsList();
            _fieldsToFinalize.Clear();
            foreach (var provider in _allSampleDataProviders.Keys)
            {
                foreach (var sourceType in _allSampleDataProviders[provider])
                {
                    if (data == null || sourceType.DataSourceType == data.GetType())
                    {
                        foreach (var propInfo in sourceType.AllProps)
                        {
                            UpdateFieldsWithData(propInfo, sourceType, data);
                        }
                    }
                }
            }

            foreach (var field in _fieldsToFinalize)
            {
                field.FinalizeSetting();
            }
        }

        private async UniTask UpdateFieldsWithData(DataSourceTypeInformation.DataSourcePropInformation prop,
            DataSourceTypeInformation sourceType, object data)
        {
            var allTasks = new HashSet<UniTask>();
            foreach (var field in _allFields[prop.PropAddress])
            {
                if (!_fieldsToFinalize.Contains(field)) _fieldsToFinalize.Add(field);
                prop.SetPostChangeAction((propInfo) =>
                {
                    RunPostChangeAction(data, sourceType, propInfo);
                });
                try
                {
                    if (data != null)
                    {
                        allTasks.Add(field.SetData(prop.PropAddress, prop.GetValueOnDataObject(data)));
                    }
                    else
                    {
                        allTasks.Add(field.SetNullData(prop.PropAddress));
                    }
                }
                catch (Exception e)
                {
                    MarkErrorOnField(field, prop.PropAddress, FieldErrorPhase.OnSettingData, e);
                }

                foreach (var connector in field.Connectors)
                {
                    if (connector is IEditableConnector editable)
                    {
                        editable.SetDataCallback(async (o) =>
                        {
                            try
                            {
                                prop.SetValueOnDataObject(data, o);
                            }
                            catch (Exception e)
                            {
                                MarkErrorOnField(field, prop.PropAddress, FieldErrorPhase.OnUserInput, e);
                            }
                        });
                    }

                    foreach (var deco in prop.AllDecorations)
                    {
                        connector.ApplyDecoration(deco);
                    }
                }

                await UniTask.WhenAll(allTasks);
            }
        }

        private void RunPostChangeAction(object data, DataSourceTypeInformation typeInfo, DataSourceTypeInformation.DataSourcePropInformation propInfo)
        {
            foreach (var method in typeInfo.AllPostChangeMethods)
            {
                method.Invoke(data, null);
            }

            if (propInfo.RefreshFullDataViewAfterSet)
            {
                UpdateAllProviders();
            }

        }

        private DictionarySet<IDataProvider, DataSourceTypeInformation> AllProviders
        {
            get
            {
                if (_allProviders == null) BuildProvidersList();
                return _allProviders;
            }
        }

        private readonly DictionarySet<IDataProvider, DataSourceTypeInformation> _allProviders =
            new DictionarySet<IDataProvider, DataSourceTypeInformation>();

        private readonly DictionarySet<ISampleDataProvider, DataSourceTypeInformation> _allSampleDataProviders =
            new DictionarySet<ISampleDataProvider, DataSourceTypeInformation>();

        private readonly DictionarySet<string, DataViewField> _allFields = new DictionarySet<string, DataViewField>();

        public IEnumerable<DataSourceTypeInformation.DataSourcePropInformation> GetPropInfosConvertibleFrom<T>()
        {
            foreach (var propInfo in GetAllPropInfos())
            {
                if (propInfo.PropType.IsAssignableFrom(typeof(T)))
                {
                    yield return propInfo;
                }
            }
        }

        public IEnumerable<DataSourceTypeInformation.DataSourcePropInformation> GetPropInfosConvertibleTo<T>()
        {
            foreach (var propInfo in GetAllPropInfos())
            {
                if (typeof(T).IsAssignableFrom(propInfo.PropType))
                {
                    yield return propInfo;
                }
            }
        }

        public IEnumerable<DataSourceTypeInformation.DataSourcePropInformation> GetPropInfosOfExactType<T>()
        {
            foreach (var propInfo in GetAllPropInfos())
            {
                if (typeof(T) == propInfo.PropType)
                {
                    yield return propInfo;
                }
            }
        }


        public IEnumerable<DataSourceTypeInformation.DataSourcePropInformation> GetAllPropInfos()
        {
            VerifyProvidersList();
            foreach (var kvp in AllProviders)
            {
                foreach (var dataSourceInfo in kvp.Value)
                {
                    foreach (var propInfo in dataSourceInfo.AllProps)
                    {
                        yield return propInfo;
                    }
                }
            }
        }

        public DataSourceTypeInformation.DataSourcePropInformation GetPropInfo(string propAddress)
        {
            if (string.IsNullOrEmpty(propAddress)) return null;

            foreach (var prop in GetAllPropInfos())
            {
                if (prop.PropAddress == propAddress)
                {
                    return prop;
                }
            }

            return null;
        }


        public bool DoesPropExist(string propAddress)
        {
            return GetPropInfo(propAddress) != null;
        }

        public Type GetTypeOfProp(string propAddress)
        {
            var propInfo = GetPropInfo(propAddress);
            if (propInfo == null) return typeof(object);
            return propInfo.PropType;
        }


        private readonly HashSet<string> _sampleDataNamesScratchSpace = new HashSet<string>();

        public string[] GetAvailableSampleDataNames()
        {
            BuildSampleProvidersList();
            _sampleDataNamesScratchSpace.Clear();
            foreach (var sampleProvider in _allSampleDataProviders.Keys)
            {
                foreach (var sampleSetName in sampleProvider.GetSampleDataSetNames())
                {
                    if (!_sampleDataNamesScratchSpace.Contains(sampleSetName))
                        _sampleDataNamesScratchSpace.Add(sampleSetName);
                }
            }

            var rtn = _sampleDataNamesScratchSpace.ToArray();
            _sampleDataNamesScratchSpace.Clear();
            return rtn;
        }

        private string _lastUsedSampleName = null;

        public void UpdateSampleData()
        {
            SetSampleDataName(_lastUsedSampleName);
        }

        public void SetSampleDataName(string sampleName)
        {
            _lastUsedSampleName = sampleName;
            _errorInfo.Clear();
            foreach (var sampleDataProvider in _allSampleDataProviders.Keys)
            {
                if (!string.IsNullOrEmpty(sampleName))
                {
                    var dataSet = sampleDataProvider.GetSampleDataSet(sampleName);
                    UpdateWithSampleData(dataSet);
                }
                else
                {
                    UpdateWithSampleData(null);
                }
            }
        }

        public object GetSampleDataByName(string sampleName)
        {
            foreach (var sampleDataProvider in _allSampleDataProviders.Keys)
            {
                if (!string.IsNullOrEmpty(sampleName))
                {
                    var dataSet = sampleDataProvider.GetSampleDataSet(sampleName);
                    return dataSet;
                }
            }

            return null;
        }


        public enum FieldErrorPhase
        {
            OnSettingData,
            OnUserInput,
            OnFindingProps
        }

        private readonly DictionarySet<DataViewField, FieldErrorInformation> _errorInfo =
            new DictionarySet<DataViewField, FieldErrorInformation>();

        public void MarkErrorOnField(DataViewField field, string propName, FieldErrorPhase phase, Exception exception, DataViewFieldConnectorBase connector = null)
        {
            var newErrorInfo = new FieldErrorInformation()
            {
                field = field,
                connector = connector,
                exception = exception,
                phase = phase,
                propAddress = propName,
                stacktrace = Environment.StackTrace
            };
            _errorInfo.Add(field, newErrorInfo);
            field.SetError(newErrorInfo);
        }

        public bool DoesFieldHaveErrors(DataViewField field)
        {
            return _errorInfo[field] != null;
        }

        public IEnumerable<FieldErrorInformation> GetErrorInfosForField(DataViewField field)
        {
            if (_errorInfo[field] != null)
            {
                foreach (var err in _errorInfo[field])
                {
                    yield return err;
                }
            }
        }


        public IEnumerable<DataViewField> GetFieldsForProp(string propAddress)
        {
            BuildFieldsList();
            var theseFields = _allFields[propAddress];
            if (theseFields == null || theseFields.Count == 0)
            {
                throw new NoFieldsException();
            }

            foreach (var field in _allFields[propAddress])
            {
                yield return field;
            }
        }
    }

    public class NoFieldsException : Exception
    {
    }

    public struct FieldErrorInformation
    {
        public Exception exception;
        public string stacktrace;
        public DataViewLink.FieldErrorPhase phase;
        public string propAddress;
        public DataViewField field;
        public DataViewFieldConnectorBase connector;

        public override int GetHashCode()
        {
            return HashCode.Combine(exception.GetType(), phase, propAddress);
        }
    }
}