using MantaMiddleware.Utilities;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace MantaMiddleware.DataView
{
    public class ShowSpinnerWidgetWhenBusy : MonoBehaviour, IBusyStateResponder
    {
        public AssetReferenceGameObject spinnerPrefab;
        
        public void SetState(DataViewField dataViewField, bool state)
        {
            ObjectPool.Instantiate(spinnerPrefab, transform);
        }
    }
}