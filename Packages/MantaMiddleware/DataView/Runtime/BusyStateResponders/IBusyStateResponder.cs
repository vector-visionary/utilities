namespace MantaMiddleware.DataView
{
    public interface IBusyStateResponder
    {
        public void SetState(DataViewField dataViewField, bool state);
    }
}