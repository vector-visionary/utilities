using System.Collections.Generic;
using MantaMiddleware.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    public class DisableInteractableWhenBusy : MonoBehaviour, IBusyStateResponder
    {
        private static Dictionary<DataViewField, Selectable[]> selectablesByField =
            new Dictionary<DataViewField, Selectable[]>();
        
        public void SetState(DataViewField dataViewField, bool busy)
        {
            if (!selectablesByField.ContainsKey(dataViewField))
            {
                selectablesByField.Add(dataViewField, dataViewField.GetComponents<Selectable>());
            }

            foreach (var selectable in selectablesByField[dataViewField])
            {
                selectable.interactable = !busy;
            }
        }
    }
}