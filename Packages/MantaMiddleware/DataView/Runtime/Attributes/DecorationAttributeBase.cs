using System;

namespace MantaMiddleware.DataView
{
    public abstract class DecorationAttributeBase : Attribute
    {
    }
    
    public class RangeDecorationAttribute : DecorationAttributeBase
    {
        public readonly float min;
        public readonly float max;

        public RangeDecorationAttribute(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
    }

}