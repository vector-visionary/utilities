using System;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    public abstract class DataViewAttributeBase : Attribute
    {
        public string GroupName => _groupName;
        private readonly string _groupName;
        public string PropName => _propName;
        private readonly string _propName;


        public DataViewAttributeBase(string groupName, string propName)
        {
            _groupName = groupName;
            _propName = propName;
        }

        public bool RefreshFullDataViewAfterSet { get; set; } = false;
    }
    
    [AttributeUsage((AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method))]
    public class DataViewPropAttribute : DataViewAttributeBase
    {
        public DataViewPropAttribute() : base(null, null) {}
        public DataViewPropAttribute(string propName) : base(null, propName) {}
        public DataViewPropAttribute(string groupName, string propName) : base(groupName, propName) {}
    }

    public class DataViewPropGroupAttribute : DataViewAttributeBase
    {
        public DataViewPropGroupAttribute() : base(null, null) {}
        public DataViewPropGroupAttribute(string groupName) : base(groupName, null) {}
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class DataViewPostChangeActionAttribute : Attribute
    {
    }

    [AttributeUsage((AttributeTargets.Class), AllowMultiple = true)]
    public class SupportsDataConvertibleFromAttribute : Attribute
    {
        public Type SupportedType => _type;
        private readonly Type _type;

        public SupportsDataConvertibleFromAttribute(Type type)
        {
            _type = type;
        }

        public bool Supports(Type propType)
        {
            return propType.IsAssignableFrom(_type);
        }
    }

    [AttributeUsage((AttributeTargets.Class))]
    public class SupportsDataConvertibleToAttribute : Attribute
    {
        public Type SupportedType => _type;
        private readonly Type _type;

        public SupportsDataConvertibleToAttribute(Type type)
        {
            _type = type;
        }
        public bool Supports(Type propType)
        {
            if (_type == typeof(string)) return true; // we always have .ToString()
            
            return _type.IsAssignableFrom(propType);
        }
    }

    [AttributeUsage((AttributeTargets.Class), AllowMultiple = true)]
    public class RequireOneOfAttribute : Attribute
    {
        private readonly bool _includeChildren = false;
        private readonly Type[] _types;

        public RequireOneOfAttribute(params Type[] types)
        {
            _types = types;
        }
        
        public RequireOneOfAttribute(bool includeChildren, params Type[] types)
        {
            _includeChildren = includeChildren;
            _types = types;
        }

        public bool MeetsRequirements(GameObject gameObject)
        {
            foreach (var type in _types)
            {
                if (_includeChildren)
                {
                    if (gameObject.GetComponentInChildren(type)) return true;
                }
                else
                {
                    if (gameObject.GetComponent(type)) return true;
                }

            }

            return false;
        }
    }

    [AttributeUsage((AttributeTargets.Class), AllowMultiple = true)]
    public class RequirePropDecorationAttribute : Attribute
    {
        private readonly Type _decorationType;

        public RequirePropDecorationAttribute(Type decorationType)
        {
            _decorationType = decorationType;
        }

        public bool MeetsRequirements(DataSourceTypeInformation.DataSourcePropInformation propInfo)
        {
            foreach (var deco in propInfo.AllDecorations)
            {
                if (deco.GetType() == _decorationType) return true;
            }

            return false;
        }
    }

}