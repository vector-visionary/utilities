using System;
using System.Collections.Generic;

namespace MantaMiddleware.DataView
{
    public interface ISampleDataProvider
    {
        public IEnumerable<string> GetSampleDataSetNames();
        public object GetSampleDataSet(string dataSetName);
        IEnumerable<Type> GetDataSourceTypes();
    }
}