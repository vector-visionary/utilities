using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace MantaMiddleware.DataView
{
    public interface IEditableConnector
    {
        public void SetDataCallback(Func<object, UniTask> callback);
        public IEnumerable<Type> GetSupportedTypes();
    }
    public interface IEditableConnector<T> : IEditableConnector 
    {
        public void SetDataCallback(Func<T, UniTask> callback);
    }

}