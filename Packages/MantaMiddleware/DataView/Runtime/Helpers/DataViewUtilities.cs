using System;
using System.Collections.Generic;
using UnityEngine;

namespace MantaMiddleware.DataView
{
    public static class DataViewUtilities
    {
        public static DataSourceTypeInformation GetDataSourceInformation(Type dataSourceType)
        {
            if (!knownDataSourceTypes.ContainsKey(dataSourceType))
            {
                knownDataSourceTypes.Add(dataSourceType,
                    new DataSourceTypeInformation(dataSourceType));
            }

            return knownDataSourceTypes[dataSourceType];
        }

        private static Dictionary<Type, DataSourceTypeInformation> knownDataSourceTypes =
            new Dictionary<Type, DataSourceTypeInformation>();


        private static Dictionary<Transform, DataViewLink> parentLinksCache = new Dictionary<Transform, DataViewLink>();
        public static DataViewLink GetFirstLink(Transform transform)
        {
            if (Application.isPlaying && parentLinksCache.TryGetValue(transform, out var rtn))
            {
                return rtn;
            }

            var origTransform = transform;
            while (transform != null)
            {
                rtn = transform.GetComponent<DataViewLink>();
                if (rtn != null)
                {
                    if (Application.isPlaying)
                    {
                        parentLinksCache.Add(origTransform, rtn);
                    }
                    return rtn;
                }

                transform = transform.parent;
            }

            if (Application.isPlaying)
            {
                parentLinksCache.Add(origTransform, null);
            }
            return null;
        }

        public static string[] AllToStrings<T>(T[] array)
        {
            if (array == null) return null;
            
            var rtn = new string[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] is Type type)
                {
                    rtn[i] = type.Name;
                }
                else
                {
                    rtn[i] = array[i].ToString();
                }
            }
            return rtn;
        }

        public static IEnumerable<string> AsPropAddresses(
            this IEnumerable<DataSourceTypeInformation.DataSourcePropInformation> infos)
        {
            foreach (var info in infos)
            {
                yield return info.PropAddress;
            }
        }

        public static string GetFriendlyErrorMessage(DataViewLink link, DataViewField field, FieldErrorInformation err)
        {
            string fieldName = "NULL";
            if (err.field != null) fieldName = err.field.name;
            string propName = err.propAddress;
            if (err.phase == DataViewLink.FieldErrorPhase.OnFindingProps)
            {
                if (string.IsNullOrEmpty(err.propAddress))
                { 
                    return $"{fieldName}: Property address is not set";
                }
                else
                {
                    return $"{fieldName}: Could not find prop with address {err.propAddress}";
                }
            }
            if (err.exception is InvalidCastException ice)
            {
                string expectedTypes = string.Concat(GetSupportedTypesOf(field, err.phase == DataViewLink.FieldErrorPhase.OnUserInput, err.phase == DataViewLink.FieldErrorPhase.OnSettingData));
                foreach (var connector in field.Connectors)
                {
                    return
                        $"{fieldName}: Could not convert {err.propAddress} ({link.GetTypeOfProp(err.propAddress).Name}) for {connector.GetType().Name} which expects one of: {expectedTypes}";
                }
            }
            return $"{fieldName} {propName}: {err.exception.GetType().ToString()}";
        }

        private static IEnumerable<object> GetSupportedTypesOf(DataViewField field, bool convertFrom, bool convertTo)
        {
            foreach (var connector in field.Connectors)
            {
                var attributes = connector.GetType().GetCustomAttributes(true);
                foreach (var attr in attributes)
                {
                    if (convertTo && attr is SupportsDataConvertibleToAttribute convTo)
                    {
                        yield return convTo.SupportedType;
                    }

                    if (convertFrom && attr is SupportsDataConvertibleToAttribute convFrom)
                    {
                        yield return convFrom.SupportedType;
                    }
                }
            }
        }

        public static bool CanConvert(Type fromType, Type toType)
        {
            if (toType == typeof(string) && fromType != typeof(Action)) return true;
            if (toType.IsAssignableFrom(fromType)) return true;
            if (TypeAdapterCatalog.Catalog.GetConverter(fromType, toType) != null) return true;
            return false;
        }


    }
}