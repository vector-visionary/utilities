using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace MantaMiddleware.DataView
{
    public class DataSourceTypeInformation
    {
        public Type DataSourceType => _dataSourceType;
        private Type _dataSourceType;

        public DataSourceTypeInformation(Type dataSourceType)
        {
            _dataSourceType = dataSourceType;
            var members = dataSourceType.GetMembers();
            foreach (var member in members)
            {
                DataSourcePropInformation memberPropInfo = null;
                HashSet<DecorationAttributeBase> decorations = null;
                foreach (var attribute in member.GetCustomAttributes())
                {
                    if (attribute is DataViewPropAttribute propAttr)
                    {
                        memberPropInfo = new DataSourcePropInformation(member, propAttr);
                        GetGroup(propAttr.GroupName).allProps.Add(memberPropInfo);
                    }
                    
                    if (attribute is DataViewPostChangeActionAttribute)
                    {
                        AllPostChangeMethods.Add((MethodInfo)member);
                    }


                    if (attribute is DataViewPropGroupAttribute groupAttr)
                    {
                        //var group = GetGroupByAttribute(groupAttr, member);
                        // TODO
                    }

                    if (attribute is DecorationAttributeBase deco)
                    {
                        if (decorations == null) decorations = new HashSet<DecorationAttributeBase>();
                        decorations.Add(deco);
                    }
                }

                if (memberPropInfo != null)
                {
                    memberPropInfo.SetDecorations(decorations);
                }
            }

            var methods = dataSourceType.GetMethods();
            foreach (var method in methods)
            {
                foreach (var attribute in method.GetCustomAttributes())
                {
                    if (attribute is DataViewPropAttribute buttonAttr && method.GetParameters().Length == 0) // TODO: allow invoking a method with default params too?
                    {
                        GetGroup(buttonAttr.GroupName).allProps.Add(new DataSourcePropInformation(method, buttonAttr));
                    }

                }
            }
        }

        public IEnumerable<DataSourcePropInformation> AllProps
        {
            get
            {
                foreach (var group in allPropGroups.Values)
                {
                    foreach (var prop in group.allProps)
                    {
                        yield return prop;
                    }
                }
            }
        }

        private Dictionary<string, DataSourceGroupInformation> allPropGroups;
        public readonly HashSet<MethodInfo> AllPostChangeMethods = new HashSet<MethodInfo>();
        private DataSourceGroupInformation defaultGroup;

        // TODO: finish this
        /*
        public DataSourceGroupInformation GetGroupByAttribute(DataViewPropGroupAttribute groupAttribute, MemberInfo memberInfo)
        {
            if (groupAttribute == null) return GetGroup(null);
            DataSourceGroupInformation rtn = null;
            string usingGroupName = null;

            if (groupAttribute.GroupName == null) usingGroupName = memberInfo.Name;
            GetGroup(usingGroupName);
        }*/
        public DataSourceGroupInformation GetGroup(string name)
        {
            if (allPropGroups == null)
            {
                allPropGroups = new Dictionary<string, DataSourceGroupInformation>();
                defaultGroup = new DataSourceGroupInformation(null);
                allPropGroups.Add(string.Empty, defaultGroup);
            }

            if (string.IsNullOrEmpty(name))
            {
                return defaultGroup;
            }

            if (!allPropGroups.TryGetValue(name, out var group))
            {
                group = new DataSourceGroupInformation(name);
                allPropGroups.Add(name, group);
            }

            return group;
        }
        public class DataSourceGroupInformation
        {
            public string GroupName { get; }

            public DataSourceGroupInformation(string groupName)
            {
                GroupName = groupName;
            }
            
            public List<DataSourcePropInformation> allProps = new List<DataSourcePropInformation>();
        }
        public class DataSourcePropInformation
        {

            public string PropName
            {
                get
                {
                    if (_propAttr.PropName != null) return _propAttr.PropName;
                    if (_memberInfo != null) return _memberInfo.Name;
                    return "Unknown";
                }
            }

            public string GroupName
            {
                get
                {
                    if (_propAttr.GroupName != null) return _propAttr.GroupName;
                    return null;
                }
            }

            public string PropAddress
            {
                get
                {
                    if (!string.IsNullOrEmpty(GroupName))
                        return $"{GroupName}.{PropName}";
                    else
                        return PropName;
                }
            }

            public bool IsProperty => _memberInfo is PropertyInfo;
            public bool IsField => _memberInfo is FieldInfo;
            public bool IsMethod => _memberInfo is MethodInfo;
            public bool RefreshFullDataViewAfterSet => _propAttr.RefreshFullDataViewAfterSet;

            public IEnumerable<DecorationAttributeBase> AllDecorations
            {
                get
                {
                    if (decorations != null)
                    {
                        foreach (var deco in decorations)
                        {
                            yield return deco;
                        }
                    }
                }
            }

            private HashSet<DecorationAttributeBase> decorations = null;
            
            public Type PropType
            {
                get
                {
                    if (IsField) return ((FieldInfo)_memberInfo).FieldType;
                    if (IsProperty) return ((PropertyInfo)_memberInfo).PropertyType;
                    if (IsMethod) return typeof(Action);
                    return null;
                }
            }

            private readonly MemberInfo _memberInfo;
            private readonly DataViewPropAttribute _propAttr;

            public DataSourcePropInformation(MemberInfo memberInfo,DataViewPropAttribute propAttr)
            {
                _memberInfo = memberInfo;
                _propAttr = propAttr;
            }

            public object GetValueOnDataObject(object data)
            {
                if (data == null) return null;
                if (IsField)
                {
                    return ((FieldInfo)_memberInfo).GetValue(data);
                }

                if (IsProperty)
                {
                    return ((PropertyInfo)_memberInfo).GetValue(data);
                }

                if (IsMethod)
                {
                    if (((MethodInfo)_memberInfo).ReturnType == typeof(UniTask))
                    {
                        return (Func<UniTask>)(async () =>
                        {
                            var unitask = (UniTask)((MethodInfo)_memberInfo).Invoke(data, null);
                            await unitask;
                            await RunPostChangeActions();
                        });

                    }
                    else if (((MethodInfo)_memberInfo).ReturnType == typeof(IEnumerator))
                    {
                        return (Func<UniTask>)(async () =>
                        {
                            var ienum = (IEnumerator)((MethodInfo)_memberInfo).Invoke(data, null);
                            await ienum;
                            await RunPostChangeActions();
                        });
                    }
                    else
                    {
                        return (Action)(() =>
                        {
                            ((MethodInfo)_memberInfo).Invoke(data, null);
                            RunPostChangeActions();
                        });
                    }
                }

                return null;
            }

            public void SetValueOnDataObject(object data, object newValue)
            {
                if (data == null) return;
                if (IsField)
                {
                    ((FieldInfo)_memberInfo).SetValue(data, newValue);
                }

                if (IsProperty)
                {
                    ((PropertyInfo)_memberInfo).SetValue(data, newValue);
                }

                RunPostChangeActions();
            }

            private Action<DataSourcePropInformation> postChangeAction = null;
            public void SetPostChangeAction(Action<DataSourcePropInformation> postChangeAction)
            {
                this.postChangeAction = postChangeAction;
            }

            private UniTask RunPostChangeActions()
            {
                if (postChangeAction != null) postChangeAction(this);
                return UniTask.CompletedTask;
            }

            public void SetDecorations(HashSet<DecorationAttributeBase> decorations)
            {
                this.decorations = decorations;
            }
        }


    }
}