using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace MantaMiddleware.DataView
{
    public interface IDataProvider
    {
        public UniTask RegisterDataUpdateTrigger(Func<UniTask> updateMethod);
        public object GetData();
        IEnumerable<Type> GetDataSourceTypes();
    }
}