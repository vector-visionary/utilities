using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace MantaMiddleware.DataView
{
    public class DataViewField : MonoBehaviour
    {
        public DataViewLink ParentLink => DataViewUtilities.GetFirstLink(transform);

        public IEnumerable<DataViewFieldConnectorBase> Connectors
        {
            get
            {
                foreach (var connector in GetComponents<DataViewFieldConnectorBase>()) yield return connector;
            }
        }

        public IEnumerable<FieldErrorResponderBase> ErrorResponders
        {
            get
            {
                foreach (var errResponder in GetComponents<FieldErrorResponderBase>()) yield return errResponder;
            }
        }

        public void SetError(FieldErrorInformation errorInfo)
        {
            foreach (var errResponder in ErrorResponders)
            {
                errResponder.Respond(errorInfo);
            }
        }

        public void ClearError()
        {
            foreach (var errResponder in ErrorResponders)
            {
                errResponder.ClearError();
            }
        }

        private static readonly HashSet<string> DuplicateAddressCheckScratchSpace = new HashSet<string>(); 
        public IEnumerable<string> GetRequestedPropAddresses()
        {
            DuplicateAddressCheckScratchSpace.Clear();
            foreach (var connector in Connectors)
            {
                connector.Initialize();
                foreach (var address in connector.GetRequestedPropAddresses())
                {
                    if (!DuplicateAddressCheckScratchSpace.Contains(address))
                    {
                        DuplicateAddressCheckScratchSpace.Add(address);
                        yield return address;
                    }
                }
            }
        }

        public async UniTask SetData(string propAddress, object newValue)
        {
            var allTasks = new HashSet<UniTask>();
            foreach (var connector in Connectors)
            {
                allTasks.Add(connector.SetData(propAddress, newValue));
            }

            WatchTaskForBusyState(UniTask.WhenAll(allTasks));
            await UniTask.WhenAll(allTasks);
        }

        public async UniTask SetNullData(string propAddress)
        {
            var allTasks = new HashSet<UniTask>();
            foreach (var connector in Connectors)
            {
                allTasks.Add(connector.SetData(propAddress, null));
            }

            await WatchTaskForBusyState(UniTask.WhenAll(allTasks));
        }

        public async UniTask FinalizeSetting()
        {
            var allTasks = new HashSet<UniTask>();
            foreach (var connector in Connectors)
            {
                allTasks.Add(connector.FinalizeDataSetting());
            }

            await UniTask.WhenAll(allTasks);
        }

        public bool HasConnectorOfType(Type connectorType)
        {
            foreach (var existingConnector in Connectors)
            {
                if (existingConnector.GetType() == connectorType)
                {
                    return true;
                }
            }
            return false;
        }
        
        public IEnumerable<IBusyStateResponder> BusyStateResponders
        {
            get
            {
                foreach (var busyResponder in GetComponents<IBusyStateResponder>()) yield return busyResponder;
                foreach (var busyResponder in ParentLink.GetUniversalBlockingResponders()) yield return busyResponder;
            }
        }


        internal void SetBusyState(bool busy)
        {
            foreach (var stateResponder in BusyStateResponders)
            {
                stateResponder.SetState(this, busy);
            }
        }

        public async UniTask WatchTaskForBusyState(UniTask task, bool blocksLinkUpdate = true)
        {
            if (blocksLinkUpdate) ParentLink.AddBlockingTask(task);
            if (task.Status == UniTaskStatus.Pending)
            {
                SetBusyState(true);
                await task;
                SetBusyState(false);
            }
        }
    }
}