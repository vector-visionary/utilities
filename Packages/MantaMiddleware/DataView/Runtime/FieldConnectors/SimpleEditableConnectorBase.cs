using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace MantaMiddleware.DataView
{
    public abstract class SimpleEditableConnectorBase<T> : SimpleConnectorBase<T>, IEditableConnector<T>
    {
        protected Func<T, UniTask> savedCallback;
        public virtual void SetDataCallback(Func<T, UniTask> callback)
        {
            savedCallback = callback;
        }

        public virtual void SetDataCallback(Func<object, UniTask> callback)
        {
            savedCallback = async (t) => await callback(t);
        }

        public void HandleNewValueInstant(T newValue)
        {
            HandleNewValue(newValue);
        }
        public async virtual UniTask HandleNewValue(T newValue)
        {
            if (savedCallback != null)
            {
                await savedCallback(newValue);
            }
        }

        public IEnumerable<Type> GetSupportedTypes()
        {
            yield return typeof(T);
        }

        public override (bool, bool) SupportsPropType(DataSourceTypeInformation.DataSourcePropInformation prop)
        {
            return (DataViewUtilities.CanConvert(prop.PropType, typeof(T)), DataViewUtilities.CanConvert(typeof(T), prop.PropType));
        }

    }
}