using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace MantaMiddleware.DataView
{
    [SupportsDataConvertibleFrom(typeof(bool))]
    public class ObjectActivationConnector : SimpleConnectorBase<bool>
    {
        public bool invertValue = false;
        public override UniTask SetData(bool newValue)
        {
            gameObject.SetActive(newValue ^ invertValue);
            return UniTask.CompletedTask;
        }
    }
}