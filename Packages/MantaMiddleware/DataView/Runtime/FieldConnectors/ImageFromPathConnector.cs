using Cysharp.Threading.Tasks;
using MantaMiddleware.Utilities;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireComponent(typeof(Image))]
    [SupportsDataConvertibleFrom(typeof(string))]
    public class ImageFromPathConnector : SimpleConnectorBase<string>
    {
        private Image _image;
        public override void CommitFieldSetup()
        {
            _image = GetComponent<Image>();
            base.CommitFieldSetup();
        }

        public override async UniTask SetData(string newValue)
        {
            Texture.allowThreadedTextureCreation = true;
            var loadTask = UnityWebRequestTexture.GetTexture(newValue);
            while (!loadTask.isDone)
            {
                await UniTask.NextFrame();
            }

            if (loadTask.result == UnityWebRequest.Result.Success)
            {
                Texture2D myTexture = ((DownloadHandlerTexture)loadTask.downloadHandler).texture;
                // TODO: is it possible to await for threaded texture creation here?
                _image.sprite = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), Vector2.zero, 1f, 0, SpriteMeshType.FullRect);
            }
            else
            {
                //TODO set error here
            }

        }
    }
}