using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [SupportsDataConvertibleTo(typeof(bool))] 
    [RequireComponent(typeof(Selectable))]
    public class UIInteractabilityConnector : SimpleConnectorBase<bool>
    {
        public override void CommitFieldSetup()
        {
            base.CommitFieldSetup();
            selectable = GetComponent<Selectable>();
        }

        private Selectable selectable;
        public bool invertValue = false;
        public override UniTask SetData(bool newValue)
        {
            selectable.interactable = newValue ^ invertValue;
            return UniTask.CompletedTask;
        }
    }
}