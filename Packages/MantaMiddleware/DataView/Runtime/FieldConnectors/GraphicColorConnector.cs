using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView.Editor
{
    [RequireComponent(typeof(Graphic))]
    [SupportsDataConvertibleFrom(typeof(Color))]
    public class GraphicColorConnector : SimpleConnectorBase<Color>
    {
        private Graphic _graphic;
        public override void CommitFieldSetup()
        {
            _graphic = GetComponent<Graphic>();
            base.CommitFieldSetup();
        }

        public override UniTask SetData(Color newValue)
        {
            _graphic.color = newValue;
            if (!Application.isPlaying)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
            }
            return UniTask.CompletedTask;
        }
    }
}