using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MantaMiddleware.DataView
{
    public abstract class DataViewFieldConnectorBase : MonoBehaviour
    {
        public virtual bool SupportsDisplay => true;
        public virtual bool SupportsEditing => false;

        public DataViewField ParentField
        {
            get
            {
                if (_parentField == null) _parentField = GetComponent<DataViewField>();
                return _parentField;
            }
        }

        private DataViewField _parentField;

        public abstract IEnumerable<string> GetRequestedPropAddresses();

        public abstract UniTask SetData(string propAddress, object newValue);
        public abstract UniTask FinalizeDataSetting();

        private void Awake()
        {
            Initialize();
        }

        public void Initialize()
        {
            if (!isAlreadyCommittedRuntime)
            {
                CommitFieldSetup();
                if (Application.isPlaying) isAlreadyCommittedRuntime = true;
            }
        }
        public virtual void CommitFieldSetup() { }
        private bool isAlreadyCommittedRuntime = false;

        public virtual IEnumerable<Component> HideComponentsOnConnection()
        {
            yield return this;
        }

        public abstract (bool, bool) SupportsPropType(DataSourceTypeInformation.DataSourcePropInformation prop);

        public virtual void ApplyDecoration(DecorationAttributeBase deco)
        {
        }
    }
}