using Cysharp.Threading.Tasks;
using UnityEngine;

namespace MantaMiddleware.DataView
{
    [SupportsDataConvertibleFrom(typeof(Vector3))]
    public class ObjectPositionConnector : SimpleConnectorBase<Vector3>
    {
        public float movementSpeed = -1f;
        public override async UniTask SetData(Vector3 newValue)
        {
            if (movementSpeed > 0f && Application.isPlaying)
            {
                Vector3 pos = transform.position;
                while (pos != newValue)
                {
                    pos = Vector3.MoveTowards(pos, newValue, Time.deltaTime);
                    transform.position = pos;
                    await UniTask.DelayFrame(1);
                }
            }

            transform.position = newValue;
        }
    }
}