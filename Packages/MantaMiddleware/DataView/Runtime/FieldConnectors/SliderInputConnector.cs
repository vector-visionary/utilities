using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireOneOf(typeof(Slider))]
    [RequirePropDecoration(typeof(RangeDecorationAttribute))]
    [SupportsDataConvertibleFrom(typeof(float))] [SupportsDataConvertibleTo(typeof(float))]
    public class SliderInputConnector : SimpleEditableConnectorBase<float>
    {
        private Slider slider;

        public override void CommitFieldSetup()
        {
            base.CommitFieldSetup();
            slider = GetComponent<Slider>();
            slider.onValueChanged.RemoveListener(HandleNewValueInstant);
            slider.onValueChanged.AddListener(HandleNewValueInstant);
        }

        public override UniTask SetData(float newValue)
        {
            slider.SetValueWithoutNotify(newValue);
            return UniTask.CompletedTask;
        }

        public override void ApplyDecoration(DecorationAttributeBase deco)
        {
            if (deco is RangeDecorationAttribute rangeDeco)
            {
                slider.minValue = rangeDeco.min;
                slider.maxValue = rangeDeco.max;
            }
        }
    }
}