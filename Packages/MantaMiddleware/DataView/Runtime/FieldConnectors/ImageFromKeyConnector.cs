using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireComponent(typeof(Image))]
    [SupportsDataConvertibleFrom(typeof(string))]
    public class ImageFromKeyConnector : SimpleConnectorBase<string>
    {
        //public string[] AdditionalKeys;
        
        private Image _image;
        public override void CommitFieldSetup()
        {
            _image = GetComponent<Image>();
            base.CommitFieldSetup();
        }

        public override async UniTask SetData(string newValue)
        {
            var loadTask = Addressables.LoadAssetAsync<Sprite>(newValue);
            while (!loadTask.IsDone)
            {
                await UniTask.NextFrame();
            }

            _image.sprite = loadTask.Result;
        }
    }
}