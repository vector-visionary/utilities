using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine.Serialization;

namespace MantaMiddleware.DataView
{
    public abstract class SimpleConnectorBase<T> : DataViewFieldConnectorBase
    {
        public override IEnumerable<string> GetRequestedPropAddresses()
        {
            if (propAddress != null)
                yield return propAddress;
        }

        public string propAddress = null;

        public sealed override UniTask SetData(string propAddress, object newValue)
        {
            if (this.propAddress == propAddress)
            {
                SetData((T)newValue);
            }
            return UniTask.CompletedTask;
        }

        public abstract UniTask SetData(T newValue);

        public sealed override UniTask FinalizeDataSetting()
        {
            // No finalizing needed; just do it in SetData
            return UniTask.CompletedTask;
        }

        public override (bool, bool) SupportsPropType(DataSourceTypeInformation.DataSourcePropInformation prop)
        {
            return (DataViewUtilities.CanConvert(prop.PropType, typeof(T)), false);
        }
    }
}