using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireComponent(typeof(Image))]
    [SupportsDataConvertibleFrom(typeof(Sprite))]
    public class ImageSpriteConnector : SimpleConnectorBase<Sprite>
    {
        private Image _image;
        public override void CommitFieldSetup()
        {
            _image = GetComponent<Image>();
            base.CommitFieldSetup();
        }

        public override UniTask SetData(Sprite newValue)
        {
            _image.sprite = newValue;
            if (!Application.isPlaying)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
            }
            return UniTask.CompletedTask;
        }
    }
}