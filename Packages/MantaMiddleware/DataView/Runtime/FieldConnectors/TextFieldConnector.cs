using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Cysharp.Threading.Tasks;
using UnityEngine;
using TMPro;
using UnityEditor;
using UnityEngine.Serialization;
using UnityEngine.UI;


namespace MantaMiddleware.DataView
{
    [RequireOneOf(typeof(Text), typeof(TextMeshProUGUI))]
    [SupportsDataConvertibleTo(typeof(string))]
    public class TextFieldConnector : DataViewFieldConnectorBase
    {
        public enum DataViewStringConstructionType
        {
            Simple, // A single value, with an optional format indicator like #.0
            StringConstructor // A complex format string with perhaps multiple prop references
        }

        public DataViewField DataViewField
        {
            get
            {
                if (_dataViewField == null) _dataViewField = GetComponent<DataViewField>();
                return _dataViewField;
            }
        }

        private DataViewField _dataViewField;

        private TMP_Text _text_tmp;
        private Text _text;

        public DataViewStringConstructionType StringConstructionType = DataViewStringConstructionType.Simple;
        public string customStringFormat = "The number value is {FakeFloat:#.0};";
        [FormerlySerializedAs("simpleModePropName")] [FormerlySerializedAs("simpleModeFieldName")] public string simpleModePropAddress = "FakeFloat";
        public string simpleModeFormat = "#.0";

        private string formatString = null;
        private Dictionary<string, int> propAddressesToFormatIndex = new Dictionary<string, int>();

        public string showValueForNull = "-";

        public override IEnumerable<Component> HideComponentsOnConnection()
        {
            yield return GetComponent<TMP_Text>();
            yield return GetComponent<Text>();
            foreach (var c in base.HideComponentsOnConnection()) yield return c;
        }

        public override (bool, bool) SupportsPropType(DataSourceTypeInformation.DataSourcePropInformation prop)
        {
            return (true, DataViewUtilities.CanConvert(typeof(string), prop.PropType));
        }

        public override void CommitFieldSetup()
        {
            base.CommitFieldSetup();
            InitializeStringFormat();
            _text = GetComponent<Text>();
            _text_tmp = GetComponent<TMP_Text>();
        }

        private static List<string> _propAddressesScratchSpace = new List<string>();
        public void InitializeStringFormat()
        {
            _propAddressesScratchSpace.Clear();
            switch (StringConstructionType)
            {
                case DataViewStringConstructionType.StringConstructor:
                    var temp = new FormattedString(customStringFormat);
                    formatString = temp.formatString;
                    _propAddressesScratchSpace.AddRange(temp.inputFieldNames);
                    break;
                case DataViewStringConstructionType.Simple:
                    _propAddressesScratchSpace.Add(simpleModePropAddress);
                    if (!string.IsNullOrEmpty(simpleModeFormat))
                    {
                        formatString = $"{{0:{simpleModeFormat}}}";
                    }
                    else
                    {
                        formatString = "{0}";
                    }

                    break;
            }

            propAddressesToFormatIndex.Clear();
            for (int i = 0; i < _propAddressesScratchSpace.Count; i++)
            {
                propAddressesToFormatIndex.Add(_propAddressesScratchSpace[i], i);
            }
            _propAddressesScratchSpace.Clear();
        }

        public override IEnumerable<string> GetRequestedPropAddresses()
        {
            foreach (var address in propAddressesToFormatIndex.Keys)
            {
                yield return address;
            }
        }


        private object[] valuesSetting = null;
        public override UniTask SetData(string propAddress, object newValue)
        {
            if (newValue == null) newValue = showValueForNull;
            
            if (valuesSetting == null || valuesSetting.Length < propAddressesToFormatIndex.Count)
            {
                valuesSetting = new object[propAddressesToFormatIndex.Count];
            }
            if (propAddressesToFormatIndex.TryGetValue(propAddress, out var index))
            {
                valuesSetting[index] = newValue;
            }
            return UniTask.CompletedTask;
        }

        public override UniTask FinalizeDataSetting()
        {
            try
            {
                string formattedString = string.Format(formatString, valuesSetting);

                if (_text != null) _text.text = formattedString;
                if (_text_tmp != null)
                {
                    _text_tmp.text = formattedString;
                }

                EditorApplication.QueuePlayerLoopUpdate();
            }
            catch (FormatException e)
            {
                ParentField.ParentLink.MarkErrorOnField(ParentField, string.Concat(valuesSetting), DataViewLink.FieldErrorPhase.OnSettingData, e, this);
            }
            return UniTask.CompletedTask;
        }
    }

    public struct FormattedString
    {
        public FormattedString(string input)
        {
            string pattern = @"{(.*?)}";
            RegexOptions options = RegexOptions.Multiline;
            List<string> fieldNames = new List<string>();
            StringBuilder sb = new StringBuilder(0, input.Length);
            int lastIndex = 0;
            foreach (Match m in Regex.Matches(input, pattern, options))
            {
                sb.Append(input.Substring(lastIndex, m.Groups[1].Index - lastIndex));
                var fieldString = m.Groups[1].Value;
                int fieldNameEndIndex = fieldString.IndexOf(":");
                if (fieldNameEndIndex < 0) fieldNameEndIndex = fieldString.Length;

                string thisFieldName = fieldString.Substring(0, fieldNameEndIndex);
                if (!fieldNames.Contains(thisFieldName))
                {
                    fieldNames.Add(thisFieldName);
                }
                sb.Append(fieldNames.IndexOf(thisFieldName));
                
                lastIndex = m.Groups[1].Index + fieldNameEndIndex;
            }

            sb.Append(input.Substring(lastIndex));

            formatString = sb.ToString();
            inputFieldNames = fieldNames.ToArray();
        }

        public string[] inputFieldNames;
        public string formatString;
    }

}