using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireOneOf(typeof(InputField), typeof(TMP_InputField))]
    [SupportsDataConvertibleFrom(typeof(string))] [SupportsDataConvertibleTo(typeof(string))]
    public class TextInputConnector : SimpleEditableConnectorBase<string>
    {
        private InputField _inputField;
        private TMP_InputField _tmpInputField;
        public enum WhenToTrigger { OnValueChanged, OnEndEdit, OnSubmit }

        public WhenToTrigger whenToTrigger = WhenToTrigger.OnSubmit;

        public override void CommitFieldSetup()
        {
            base.CommitFieldSetup();
            _inputField = GetComponent<InputField>();
            _tmpInputField = GetComponent<TMP_InputField>();

            if (_inputField != null)
            {
                switch (whenToTrigger)
                {
                    case WhenToTrigger.OnSubmit:
                        _inputField.onSubmit.RemoveListener(HandleNewValueInstant);
                        _inputField.onSubmit.AddListener(HandleNewValueInstant);
                        break;
                    case WhenToTrigger.OnEndEdit:
                        _inputField.onEndEdit.RemoveListener(HandleNewValueInstant);
                        _inputField.onEndEdit.AddListener(HandleNewValueInstant);
                        break;
                    case WhenToTrigger.OnValueChanged:
                        _inputField.onValueChanged.RemoveListener(HandleNewValueInstant);
                        _inputField.onValueChanged.AddListener(HandleNewValueInstant);
                        break;
                }
            }
            if (_tmpInputField != null)
            {
                switch (whenToTrigger)
                {
                    case WhenToTrigger.OnSubmit:
                        _tmpInputField.onSubmit.RemoveListener(HandleNewValueInstant);
                        _tmpInputField.onSubmit.AddListener(HandleNewValueInstant);
                        break;
                    case WhenToTrigger.OnEndEdit:
                        _tmpInputField.onEndEdit.RemoveListener(HandleNewValueInstant);
                        _tmpInputField.onEndEdit.AddListener(HandleNewValueInstant);
                        break;
                    case WhenToTrigger.OnValueChanged:
                        _tmpInputField.onValueChanged.RemoveListener(HandleNewValueInstant);
                        _tmpInputField.onValueChanged.AddListener(HandleNewValueInstant);
                        break;
                }
            }
        }

        public override UniTask SetData(string newValue)
        {
            if (_inputField != null) _inputField.text = newValue;
            if (_tmpInputField != null) _tmpInputField.text = newValue;
            return UniTask.CompletedTask;
        }

        public override IEnumerable<Component> HideComponentsOnConnection()
        {
            yield return GetComponent<InputField>();
            yield return GetComponent<TMP_InputField>();
            yield return GetComponent<Image>();
        }
    }
}