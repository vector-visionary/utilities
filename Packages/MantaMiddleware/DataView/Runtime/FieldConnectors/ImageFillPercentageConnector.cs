using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireComponent(typeof(Image))]
    [SupportsDataConvertibleFrom(typeof(float))]
    public class ImageFillPercentageConnector : SimpleConnectorBase<float>
    {
        private Image _image;
        public override void CommitFieldSetup()
        {
            _image = GetComponent<Image>();
            base.CommitFieldSetup();
        }

        public override UniTask SetData(float newValue)
        {
            _image.fillAmount = newValue;
            if (!Application.isPlaying)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
            }
            return UniTask.CompletedTask;
        }
    }
}