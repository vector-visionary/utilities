using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireOneOf(typeof(Button))]
    [SupportsDataConvertibleFrom(typeof(Action))]
    [SupportsDataConvertibleFrom(typeof(Func<UniTask>))]
    public class ButtonFieldConnector : DataViewFieldConnectorBase
    {
        private Button _button;

        public override void CommitFieldSetup()
        {
            base.CommitFieldSetup();
            _button = GetComponent<Button>();
            _button.OnClickAsAsyncEnumerable().Subscribe(async _ =>
            {
                await HandleButtonClick();
            });
        }

        private async UniTask HandleButtonClick()
        {
            if (unitaskCallback != null)
            {
                await ParentField.WatchTaskForBusyState(unitaskCallback());
            }
            else if (regularActionCallback != null)
            {
                regularActionCallback();
            }
        }

        public string propAddress = null;
        private Action regularActionCallback;
        private Func<UniTask> unitaskCallback;


        public override IEnumerable<string> GetRequestedPropAddresses()
        {
            if (propAddress != null) yield return propAddress;
        }

        public override UniTask SetData(string propAddress, object newValue)
        {
            if (this.propAddress == propAddress)
            {
                regularActionCallback = newValue as Action;
                unitaskCallback = newValue as Func<UniTask>;
            }
            return UniTask.CompletedTask;
        }

        public override UniTask FinalizeDataSetting()
        {
            return UniTask.CompletedTask;
        }


        public override (bool, bool) SupportsPropType(DataSourceTypeInformation.DataSourcePropInformation prop)
        {
            return (prop.IsMethod || prop.PropType == typeof(Action), false);
        }

        public override IEnumerable<Component> HideComponentsOnConnection()
        {
            yield return GetComponent<Button>();
        }
    }
}