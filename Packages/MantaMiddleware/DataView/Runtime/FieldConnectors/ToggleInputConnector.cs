using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireOneOf(typeof(Toggle))]
    [SupportsDataConvertibleFrom(typeof(bool))] [SupportsDataConvertibleTo(typeof(bool))]
    public class ToggleInputConnector : SimpleEditableConnectorBase<bool>
    {
        private Toggle toggle;
        public override void CommitFieldSetup()
        {
            base.CommitFieldSetup();
            toggle = GetComponent<Toggle>();
            toggle.onValueChanged.RemoveListener(HandleNewValueInstant);
            toggle.onValueChanged.AddListener(HandleNewValueInstant);
        }

        public override UniTask SetData(bool newValue)
        {
            toggle.SetIsOnWithoutNotify(newValue);
            return UniTask.CompletedTask;
        }

        public override IEnumerable<Component> HideComponentsOnConnection()
        {
            yield return GetComponent<Toggle>();
        }
    }
}