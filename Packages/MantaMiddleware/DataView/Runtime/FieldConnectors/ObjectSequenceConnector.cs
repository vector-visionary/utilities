using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Utilities;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace MantaMiddleware.DataView
{
    [SupportsDataConvertibleFrom(typeof(int))]
    public class ObjectSequenceConnector : SimpleConnectorBase<int>
    {
        public AssetReferenceGameObject objectInSequence;
        private List<GameObject> sequence = new List<GameObject>();


        public override async UniTask SetData(int newValue)
        {
            while (sequence.Count < newValue)
            {
                sequence.Add(await ObjectPool.Instantiate(objectInSequence, transform));
            }

            while (sequence.Count > newValue)
            {
                var removing = sequence[^1];
                sequence.RemoveAt(sequence.Count-1);
                await ObjectPool.Destroy(removing);
            }
        }
    }
}