using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MantaMiddleware.DataView
{
    public abstract class FieldErrorResponderBase : MonoBehaviour
    {
        public abstract void Respond(FieldErrorInformation errorInfo);
        public abstract void ClearError();
    }
}
