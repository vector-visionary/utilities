using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView
{
    [RequireComponent(typeof(Graphic))]
    public class ColorHighlightErrorResponder : FieldErrorResponderBase
    {
        private Graphic _graphic;
        public Color ErrorColor = Color.red;
        private Color DefaultColor = Color.white;

        private void Awake()
        {
            _graphic = GetComponent<Graphic>();
            DefaultColor = _graphic.color;
        }

        public override void Respond(FieldErrorInformation errorInfo)
        {
            _graphic.color = ErrorColor;
        }

        public override void ClearError()
        {
            _graphic.color = DefaultColor;
        }
    }
}