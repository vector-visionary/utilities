using System;
using System.Collections.Generic;
using System.Linq;
using MantaMiddleware.DataView.Editor;

namespace MantaMiddleware.DataView
{
    public class TypeAdapterCatalog : IAdapterCatalog
    {
        public static TypeAdapterCatalog Catalog
        {
            get
            {
                if (_catalog == null)
                {
                    _catalog = new TypeAdapterCatalog();
                }
                return _catalog;
            }
        }

        private static TypeAdapterCatalog _catalog;

        private TypeAdapterCatalog()
        {
            allAdapters = new Dictionary<Type, Dictionary<Type, TypeAdapterBase>>();
            var adapterTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(domainAssembly => domainAssembly.GetTypes())
                .Where(type => typeof(TypeAdapterBase).IsAssignableFrom(type)
                ).ToArray();
            foreach (var adapterType in adapterTypes)
            {
                if (!adapterType.IsAbstract)
                {
                    TypeAdapterBase newAdapter = (TypeAdapterBase)Activator.CreateInstance(adapterType);
                    newAdapter.RegisterAdapter(this);
                }
            }
        }
        
        public void Register(Type from, Type to, TypeAdapterBase adapter)
        {
            if (!allAdapters.TryGetValue(from, out var toDict))
            {
                toDict = new Dictionary<Type, TypeAdapterBase>();
                allAdapters.Add(from, toDict);
            }

            if (!toDict.ContainsKey(to))
            {
                toDict.Add(to, adapter);
            }
        }

        private Dictionary<Type, Dictionary<Type, TypeAdapterBase>> allAdapters;

        public TypeAdapterBase GetConverter(Type fromType, Type toType)
        {
            if (allAdapters.TryGetValue(fromType, out var toDict))
            {
                if (toDict.TryGetValue(toType, out var rtn))
                {
                    return rtn;
                }
            }

            return null;
        }
    }
    
    public interface IAdapterCatalog
    {
        void Register(Type from, Type to, TypeAdapterBase adapter);
    }
    public abstract class TypeAdapterBase
    {
        public abstract void RegisterAdapter(IAdapterCatalog catalog);
    }

    public abstract class TypeAdapter<TFrom, TTo> : TypeAdapterBase
    {
        public abstract (bool, TTo) Convert(TFrom from);
        public override void RegisterAdapter(IAdapterCatalog catalog)
        {
            catalog.Register(typeof(TFrom), typeof(TTo), this);
        }

        public virtual bool AllowChaining => false;
    }
}