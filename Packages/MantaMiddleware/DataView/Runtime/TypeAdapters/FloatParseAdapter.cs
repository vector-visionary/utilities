using System;
using UnitsNet;

namespace MantaMiddleware.DataView
{
    public class FloatParseAdapter : TypeAdapter<string, float>
    {
        public override (bool, float) Convert(string from)
        {
            bool success = float.TryParse(from, out var rtn);
            return (success, rtn);
        }
    }

    public class IntParseAdapter : TypeAdapter<string, int>
    {
        public override (bool, int) Convert(string from)
        {
            bool success = int.TryParse(from, out var rtn);
            return (success, rtn);
        }
    }

    public class DoubleParseAdapter : TypeAdapter<string, double>
    {
        public override (bool, double) Convert(string from)
        {
            bool success = double.TryParse(from, out var rtn);
            return (success, rtn);
        }
    }

    public class LengthFloatAdapter : TypeAdapter<Length, double>
    {
        public override (bool, double) Convert(Length from)
        {
            return (true, from.Meters);
        }
    }

}