using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Graphs;
using UnityEngine;

namespace MantaMiddleware.DataView.Editor
{
    [CustomEditor(typeof(DataViewLink))]
    public class DataViewLinkEditor : UnityEditor.Editor
    {
        private static Dictionary<DataViewLink, int> sampleIndexByLink = new Dictionary<DataViewLink, int>();
        private static Dictionary<DataViewLink, string> sampleNameByLink = new Dictionary<DataViewLink, string>();
        public override void OnInspectorGUI()
        {
            var dataViewLink = target as DataViewLink;
            EditorGUILayout.Space();
            ShowSampleSelection(dataViewLink);
            EditorGUILayout.Space();
            ShowPropInfos(dataViewLink);
        }
        



        private Dictionary<string, bool> foldoutExpanded = new Dictionary<string, bool>();
        private void ShowPropInfos(DataViewLink dataViewLink)
        {
            GUILayout.Label("All Source Properties");
            EditorGUI.indentLevel++;
            object thisSampleData = null;
            if (sampleNameByLink.TryGetValue(dataViewLink, out var sampleName))
            { 
                thisSampleData = dataViewLink.GetSampleDataByName(sampleName);
            }
            else
            {
                thisSampleData = null;
            }

            var orphanedFields = new HashSet<DataViewField>(dataViewLink.GetComponentsInChildren<DataViewField>(true));

            foreach (var prop in dataViewLink.GetAllPropInfos())
            {
                string labelName = $"{prop.PropAddress} ({prop.PropType.Name})";
                object propValue = prop.GetValueOnDataObject(thisSampleData);
                
                if (!foldoutExpanded.ContainsKey(prop.PropAddress)) foldoutExpanded.Add(prop.PropAddress, false);
                
                EditorGUILayout.Space();
                GUILayout.BeginHorizontal();
                foldoutExpanded[prop.PropAddress] = EditorGUILayout.Foldout(foldoutExpanded[prop.PropAddress], "");
                EditorGUILayout.PrefixLabel(labelName);
                GUI.enabled = false;
                if (typeof(Object).IsAssignableFrom(prop.PropType))
                {
                    EditorGUILayout.ObjectField((UnityEngine.Object)propValue, prop.PropType, true);
                }
                else if (propValue == null)
                {
                    EditorGUILayout.TextField("NULL");
                }
                else
                {
                    EditorGUILayout.TextField(propValue.ToString());
                }
                GUI.enabled = true;
                GUILayout.EndHorizontal();
                if (foldoutExpanded[prop.PropAddress])
                {
                    ShowFieldInfos(dataViewLink, prop);
                }

                try
                {
                    foreach (var field in dataViewLink.GetFieldsForProp(prop.PropAddress))
                    {
                        if (orphanedFields.Contains(field)) orphanedFields.Remove(field);
                    }
                }
                catch (NoFieldsException nfe)
                {
                    EditorGUILayout.HelpBox($"No fields found for {prop.PropAddress}", MessageType.Warning);
                }

                EditorGUILayout.Space();
            }

            EditorGUI.indentLevel--;

            if (orphanedFields.Count > 0)
            {
                EditorGUILayout.HelpBox("The following fields have no valid property assigned to them:", MessageType.Warning);
                foreach (var field in orphanedFields)
                {
                    ShowFieldInfo(dataViewLink, field);
                }
            }
        }
        
        private void ShowFieldInfos(DataViewLink dataViewLink, DataSourceTypeInformation.DataSourcePropInformation prop)
        {
            EditorGUI.indentLevel++;
            try
            {
                foreach (var field in dataViewLink.GetFieldsForProp(prop.PropName))
                {
                    ShowFieldInfo(dataViewLink, field);
                }
            }
            catch (NoFieldsException nfe)
            {

            }

            EditorGUI.indentLevel--;

        }

        private void ShowFieldInfo(DataViewLink dataViewLink, DataViewField field)
        {
            bool hasErrors = dataViewLink.DoesFieldHaveErrors(field);
            if (hasErrors) EditorGUILayout.Space();
            foreach (var connector in field.Connectors)
            {
                EditorGUILayout.ObjectField(connector, connector.GetType(), true);
            }

            if (hasErrors)
            {
                int countErrs = 0;
                int maxErrs = 2;
                foreach (var err in dataViewLink.GetErrorInfosForField(field))
                {
                    countErrs++;
                    if (countErrs > maxErrs)
                    {
                        continue;
                    }
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.HelpBox(DataViewUtilities.GetFriendlyErrorMessage(dataViewLink, field, err), MessageType.Error);
                    if (GUILayout.Button("?"))
                    {
                        Debug.LogError($"{err.exception.Message}\n{err.stacktrace}");
                    }
                    GUILayout.EndHorizontal();
                }

                if (countErrs > maxErrs)
                {
                    EditorGUILayout.HelpBox($"There were {countErrs - maxErrs} more errors on this field.", MessageType.Warning);
                    ;
                }
                EditorGUILayout.Space();
            }
        }

        public static void ShowSampleSelection(DataViewLink targetLink)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Sample Data");
            List<string> samplesAvailable = new List<string>();
            samplesAvailable.Add("None");
            string[] samplesFromLink = targetLink.GetAvailableSampleDataNames();
            if (samplesFromLink == null || samplesFromLink.Length == 0)
            {
                GUI.enabled = false;
            }
            else
            {
                samplesAvailable.AddRange(samplesFromLink);
            }

            if (!sampleIndexByLink.TryGetValue(targetLink, out int oldIndex))
            {
                sampleIndexByLink.Add(targetLink, 0);
                oldIndex = 0;
            }
            // safeguards, just in case
            if (oldIndex < 0) oldIndex = 0;
            if (oldIndex >= samplesAvailable.Count) oldIndex = samplesAvailable.Count - 1; 
            
            int newIndex = EditorGUILayout.Popup(oldIndex, samplesAvailable.ToArray());

            if (oldIndex != newIndex)
            {

                string sampleName = newIndex == 0 ? null : samplesAvailable[newIndex];
                targetLink.SetSampleDataName(sampleName);

                sampleIndexByLink[targetLink] = newIndex;
                if (!sampleNameByLink.ContainsKey(targetLink)) sampleNameByLink.Add(targetLink, sampleName); 
                else sampleNameByLink[targetLink] = sampleName;
            }
            GUILayout.EndHorizontal();
            GUI.enabled = true;

        }
    }
}