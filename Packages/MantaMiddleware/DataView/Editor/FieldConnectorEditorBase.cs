using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MantaMiddleware.DataView.Editor
{
    public abstract class FieldConnectorEditorBase
    {
        public abstract void OnInspectorGUI(DataViewFieldConnectorBase targetFieldActiveConnector);
    }
    public abstract class FieldConnectorEditorBase<T> : FieldConnectorEditorBase where T : class
    {
        private SerializedObject _serializedObject;
        private Dictionary<string, SerializedProperty> propertyDictionary; // not initialized here because most editors don't use it
        private DataViewFieldConnectorBase _target;

        protected SerializedProperty GetSerializedProperty(string propertyName)
        {
            if (propertyDictionary == null) propertyDictionary = new Dictionary<string, SerializedProperty>();
            else if (propertyDictionary.TryGetValue(propertyName, out var rtn))
            {
                return rtn;
            }
            var property = _serializedObject.FindProperty(propertyName);
            propertyDictionary.Add(propertyName, property);
            return property;
        }

        public sealed override void OnInspectorGUI(DataViewFieldConnectorBase targetFieldActiveConnector)
        {
            if (_target != targetFieldActiveConnector)
            {
                _target = targetFieldActiveConnector;
                _serializedObject = new SerializedObject(_target);
            }
            OnInspectorGUI(targetFieldActiveConnector as T);
        }

        public virtual void OnInspectorGUI(T connector)
        {
            GUILayout.Label($"Inspector for {typeof(T)} has not been implemented.");
        }
    }

}