using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace MantaMiddleware.DataView.Editor
{
    public static class DataViewEditorUtilities
    {
        // CONNECTORS
        
        internal static IEnumerable<Type> GetConnectorsSupportingObjectAndData(GameObject obj, DataSourceTypeInformation.DataSourcePropInformation propInfo)
        {
            if (connectorTypesList == null)
            {
                connectorTypesList = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(domainAssembly => domainAssembly.GetTypes())
                    .Where(type => typeof(DataViewFieldConnectorBase).IsAssignableFrom(type)
                    ).ToArray();
            }

            foreach (var cType in connectorTypesList)
            {
                if (DataViewEditorUtilities.DoesConnectorTypeSupportObjectAndData(cType, obj, propInfo))
                {
                    yield return cType;
                }
            }
        }

        private static bool DoesConnectorTypeSupportObjectAndData(Type cType, GameObject gameObject, DataSourceTypeInformation.DataSourcePropInformation propInfo)
        {
            bool didFindSupportedType = false;
            // have to crawl up the class hierarchy
            var cTypeBase = cType;
            while (cTypeBase != null && cTypeBase != typeof(DataViewFieldConnectorBase))
            {
                foreach (var attr in cTypeBase.GetCustomAttributes())
                {
                    if (attr is RequireOneOfAttribute requireComponent)
                    {
                        if (!requireComponent.MeetsRequirements(gameObject))
                        {
                            return false;
                        }
                    }

                    if (attr is RequirePropDecorationAttribute requirePropDeco)
                    {
                        if (!requirePropDeco.MeetsRequirements(propInfo))
                        {
                            return false;
                        }
                    }

                    if (attr is SupportsDataConvertibleFromAttribute supFromAttr)
                    {
                        if (supFromAttr.Supports(propInfo.PropType)) didFindSupportedType = true;
                    }

                    if (attr is SupportsDataConvertibleToAttribute supToAttr)
                    {
                        if (supToAttr.Supports(propInfo.PropType)) didFindSupportedType = true;
                    }
                }
                cTypeBase = cTypeBase.BaseType;
            }

            return didFindSupportedType;
        }

        private static Type[] connectorTypesList = null;
        
        // EDITORS
        public static FieldConnectorEditorBase<T> GetEditorForConnector<T>() where T : class =>
            GetEditorForConnector(typeof(T)) as FieldConnectorEditorBase<T>;
        public static FieldConnectorEditorBase GetEditorForConnector(Type connectorType)
        {
            if (connectorToEditorLookup == null)
            { 
                connectorToEditorLookup = new Dictionary<Type, FieldConnectorEditorBase>();
                var allFCEditors = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(domainAssembly => domainAssembly.GetTypes())
                    .Where(type => typeof(FieldConnectorEditorBase).IsAssignableFrom(type)
                    ).ToArray();
                foreach (var fcEditorType in allFCEditors)
                {
                    if (fcEditorType.ContainsGenericParameters) continue; 
                    if (fcEditorType.BaseType == typeof(System.Object)) continue;
                    
                    
                    // We need to crawl all the way up the inheritance chain to FieldConnectorEditorBase<T>, to determine what <T> is
                    var edBaseType = fcEditorType;
                    while (edBaseType.BaseType != typeof(FieldConnectorEditorBase))
                    {
                        edBaseType = edBaseType.BaseType;
                    }
                    
                    if (edBaseType.GenericTypeArguments.Length > 0)
                    {
                        var fcType = fcEditorType.BaseType.GenericTypeArguments[0];
                        var newEditor = Activator.CreateInstance(fcEditorType) as FieldConnectorEditorBase;
                        connectorToEditorLookup.Add(fcType, newEditor);
                    }
                }
            }

            if (connectorToEditorLookup.TryGetValue(connectorType, out var rtn))
            {
                return rtn;
            }

            return null;
        }

        private static Dictionary<Type, FieldConnectorEditorBase> connectorToEditorLookup = null;

        // raw name; augmented name second
        private static List<(DataSourceTypeInformation.DataSourcePropInformation, string)> propPopupScratchSpace =
            new List<(DataSourceTypeInformation.DataSourcePropInformation, string)>();
        public static string PropNamePopup(string label, DataViewFieldConnectorBase connector, string currentValue)
        {
            propPopupScratchSpace.Clear();
            int currentIndex = 0;
            foreach (var prop in connector.ParentField.ParentLink.GetAllPropInfos())
            {
                if (connector == null) continue;
                
                var (read, write) = connector.SupportsPropType(prop);
                if (!(connector is IEditableConnector)) write = false;
                if (!DoesConnectorTypeSupportObjectAndData(connector.GetType(), connector.gameObject, prop))
                {
                    read = false;
                    write = false;
                }
                if (read || write)
                {
                    propPopupScratchSpace.Add((prop, $"{prop.PropAddress} ({prop.PropType.Name}) {(read?"R":"")}{(write?"W":"")}"));
                    if (prop.PropAddress == currentValue)
                    {
                        currentIndex = propPopupScratchSpace.Count - 1;
                    }
                }
            }

            if (propPopupScratchSpace.Count == 0)
            {
                GUI.enabled = false;
                propPopupScratchSpace.Add((null, "No Fields"));
            }

            string[] popupArray = new string[propPopupScratchSpace.Count];
            for (int p = 0; p < popupArray.Length; p++)
            {
                popupArray[p] = propPopupScratchSpace[p].Item2;
            }

            int newIndex = EditorGUILayout.Popup(label, currentIndex, popupArray);
            GUI.enabled = true;
            string mainPropAddress = propPopupScratchSpace[newIndex].Item1?.PropName ?? "";


            return mainPropAddress;
        }

        public static bool DoesTypeSupportStringFormat(Type propType)
        {
            try
            {
                var method = propType.GetMethod("ToString", new[] { typeof(string) });
                if (method != null) return true;
            }
            catch (Exception e)
            {
            }

            return false;
        }
    }
}