using UnityEditor;

namespace MantaMiddleware.DataView.Editor
{
    public class TextInputConnectorEditor : SimpleEditableConnectorBaseEditor<TextInputConnector, string>
    {
        public override void OnInspectorGUI(TextInputConnector connector)
        {
            base.OnInspectorGUI(connector);
            connector.whenToTrigger = (TextInputConnector.WhenToTrigger)EditorGUILayout.EnumPopup("When To Trigger", connector.whenToTrigger);
        }
    }
}