using UnityEditor;

namespace MantaMiddleware.DataView.Editor
{
    public class ObjectActivationConnectorEditor : SimpleConnectorEditor<ObjectActivationConnector, bool>
    {
        public override void OnInspectorGUI(ObjectActivationConnector connector)
        {
            connector.invertValue = EditorGUILayout.Toggle("Invert", connector.invertValue);
            base.OnInspectorGUI(connector);
        }
    }
}