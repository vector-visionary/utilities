using UnityEditor;
using UnityEngine;

namespace MantaMiddleware.DataView.Editor
{
    public class ObjectPositionConnectorEditor : SimpleConnectorEditor<ObjectPositionConnector, Vector3>
    {
        public override void OnInspectorGUI(ObjectPositionConnector connector)
        {
            var newAnimated = EditorGUILayout.Toggle("Animate Movement", connector.movementSpeed > 0);
            if (newAnimated)
            {
                if (connector.movementSpeed <= 0f) connector.movementSpeed = 1f;
                connector.movementSpeed = EditorGUILayout.FloatField("Movement Speed", connector.movementSpeed);
            }
            else
            {
                connector.movementSpeed = -1f;
            }

            base.OnInspectorGUI(connector);
        }
    }
}