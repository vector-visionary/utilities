namespace MantaMiddleware.DataView.Editor
{
    public class ButtonFieldConnectorEditor : FieldConnectorEditorBase<ButtonFieldConnector>
    {
        public override void OnInspectorGUI(ButtonFieldConnector connector)
        {
            connector.propAddress = DataViewEditorUtilities.PropNamePopup("Prop Name", connector, connector.propAddress);
        }
    }
}