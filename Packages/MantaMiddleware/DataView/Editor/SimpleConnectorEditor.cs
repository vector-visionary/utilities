using UnityEngine;

namespace MantaMiddleware.DataView.Editor
{
    public class SimpleConnectorEditor<TConnector, TData> : FieldConnectorEditorBase<TConnector> where TConnector : SimpleConnectorBase<TData>
    {
        public override void OnInspectorGUI(TConnector connector)
        {
            connector.propAddress = DataViewEditorUtilities.PropNamePopup("Prop Name", connector, connector.propAddress);
        }
    }

    public class SimpleEditableConnectorBaseEditor<TConnector, TData> : SimpleConnectorEditor<TConnector, TData> where TConnector : SimpleConnectorBase<TData>
    {
        
    }
}