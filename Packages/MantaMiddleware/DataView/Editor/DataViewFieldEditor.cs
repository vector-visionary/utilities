using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace MantaMiddleware.DataView.Editor
{
    [CustomEditor(typeof(DataViewField))]
    [CanEditMultipleObjects]
    public class DataViewFieldEditor : UnityEditor.Editor
    {
        private int newConnectorSelectedIndex = 0;

        private static Dictionary<DataViewField, bool> _addConnectorButtonsExpanded =
            new Dictionary<DataViewField, bool>();

        public override void OnInspectorGUI()
        {
            DataViewField targetField = target as DataViewField;

            if (targetField.ParentLink == null)
            {
                GUILayout.Label($"This field has no parent {nameof(DataViewLink)}; this is necessary for the operation and editing of this field.");
                return;
            }
            
            
            // Existing Connectors
            foreach (var connector in targetField.Connectors)
            {
                var edt = DataViewEditorUtilities.GetEditorForConnector(connector.GetType());
                EditorGUILayout.Space();
                GUILayout.BeginHorizontal();
                GUILayout.Label(connector.GetType().Name);
                if (GUILayout.Button("Remove"))
                {
                    DestroyImmediate(connector);
                }
                GUILayout.EndHorizontal();
                if (edt != null)
                {
                    GUILayout.BeginVertical(connector.GetType().Name, GUI.skin.window);
                    edt.OnInspectorGUI(connector);
                    GUILayout.EndVertical();
                }
                else
                {
                    GUILayout.Label($"No connector editor found for {connector.GetType().Name}");
                }
            }


            
            // Add Connector button
            bool isExpanded = false;
            _addConnectorButtonsExpanded.TryGetValue(targetField, out isExpanded);
            if (targetField.Connectors.Count() == 0) isExpanded = true;
            
            HashSet<Type> connectorsAvailable = new HashSet<Type>();
            foreach (var propInfo in targetField.ParentLink.GetAllPropInfos())
            {
                foreach (var connectorType in DataViewEditorUtilities.GetConnectorsSupportingObjectAndData(targetField.gameObject, propInfo))
                {
                    if (!connectorsAvailable.Contains(connectorType) && !targetField.HasConnectorOfType(connectorType))
                    {
                        connectorsAvailable.Add(connectorType);
                    }
                }
            }


            if (connectorsAvailable.Count > 0)
            {
                EditorGUILayout.Space();
                bool newExpanded = EditorGUILayout.Foldout(isExpanded,"Add New Connectors");
                if (newExpanded != isExpanded)
                {
                    if (_addConnectorButtonsExpanded.ContainsKey(targetField))
                        _addConnectorButtonsExpanded[targetField] = newExpanded;
                    else _addConnectorButtonsExpanded.Add(targetField, newExpanded);
                }

                if (newExpanded)
                {
                    Type[] connectorTypes = connectorsAvailable.ToArray();
                    string[] connectorNames = DataViewUtilities.AllToStrings(connectorTypes);
                    newConnectorSelectedIndex =
                        EditorGUILayout.Popup(newConnectorSelectedIndex, connectorNames);
                    if (GUILayout.Button("Add Connector"))
                    {
                        var connector =
                            (DataViewFieldConnectorBase)targetField.gameObject.AddComponent(
                                connectorTypes[newConnectorSelectedIndex]);

                        foreach (var component in connector.HideComponentsOnConnection())
                        {
                            if (component != null)
                            {
                                UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded(component, false);
                            }
                        }
                    }
                }
            }
            else
            {
                EditorGUILayout.HelpBox("No unused connector types matching this object and the provided fields are available", MessageType.Warning);
            }
            
        
            DataViewLinkEditor.ShowSampleSelection(targetField.ParentLink);
        
            GUILayout.Space(40f);
        }
    }
}
