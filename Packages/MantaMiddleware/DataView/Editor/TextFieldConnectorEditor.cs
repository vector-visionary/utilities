using UnityEditor;
using UnityEngine;

namespace MantaMiddleware.DataView.Editor
{
    public class TextFieldConnectorEditor : FieldConnectorEditorBase<TextFieldConnector>
    {
        public override void OnInspectorGUI(TextFieldConnector connector)
        {
            GUI.changed = false;
            
            GUILayout.BeginHorizontal();
            GUILayout.Label("Text Format", GUILayout.ExpandWidth(true));
            if (GUILayout.Button("Formatting Help", GUILayout.ExpandWidth(false)))
            {
                Application.OpenURL("http://www.independent-software.com/net-string-formatting-in-csharp-cheat-sheet.html");
            }

            GUILayout.EndHorizontal();
            
            connector.StringConstructionType = (TextFieldConnector.DataViewStringConstructionType) EditorGUILayout.EnumPopup(connector.StringConstructionType);

            if (connector.StringConstructionType == TextFieldConnector.DataViewStringConstructionType.StringConstructor)
            {
                connector.customStringFormat = EditorGUILayout.TextArea(connector.customStringFormat, GUILayout.Height(50f));
                var propAddresses = connector.DataViewField.ParentLink.GetPropInfosConvertibleTo<string>().AsPropAddresses();
                foreach (var propName in propAddresses)
                {
                    GUILayout.Button($"Insert {propName}");
                }
                connector.InitializeStringFormat();
            }
            else if (connector.StringConstructionType == TextFieldConnector.DataViewStringConstructionType.Simple)
            {
                connector.simpleModePropAddress = DataViewEditorUtilities.PropNamePopup("Prop", connector, connector.simpleModePropAddress);
                var currentPropType = connector.ParentField.ParentLink.GetTypeOfProp(connector.simpleModePropAddress);
                if (DataViewEditorUtilities.DoesTypeSupportStringFormat(currentPropType)) {
                    connector.simpleModeFormat =
                        EditorGUILayout.TextField("Field Format (optional)", connector.simpleModeFormat);
                }
            }
            
            EditorGUILayout.Space();
            connector.showValueForNull = EditorGUILayout.TextField("Show for Null", connector.showValueForNull);

            if (GUI.changed)
            {
                connector.DataViewField.ParentLink.UpdateSampleData();
            }
        }
    }
}