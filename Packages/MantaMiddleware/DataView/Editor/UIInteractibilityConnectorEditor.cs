namespace MantaMiddleware.DataView.Editor
{
    public class UIInteractibilityConnectorEditor : FieldConnectorEditorBase<UIInteractabilityConnector>
    {
        public override void OnInspectorGUI(UIInteractabilityConnector connector)
        {
            connector.propAddress = DataViewEditorUtilities.PropNamePopup("Prop Name", connector, connector.propAddress);
        }
    }
}