using UnityEditor;
using UnityEngine;

namespace MantaMiddleware.DataView.Editor
{
    public class ObjectSequenceConnectorEditor : SimpleConnectorEditor<ObjectSequenceConnector, int>
    {
        private SerializedProperty prefabProperty;
        
        public override void OnInspectorGUI(ObjectSequenceConnector connector)
        {
            EditorGUILayout.PropertyField(GetSerializedProperty("objectInSequence"), new GUIContent("Object Prefab"), GUILayout.Height(20));

            base.OnInspectorGUI(connector);
        }
    }
}