using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Events;
using NUnit.Framework;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.TestTools;

public class EventCasterTests
{
    [PreTest]
    public void Init()
    {
        Debug.Log("Initializing event caster for testing");
        EventCaster.Main.CastWithResult<InitCastableRequest, bool>(new InitCastableRequest());
        EventCaster.Main.ClearAllCachedEvents();

    }
    
    [UnityTest]
    public IEnumerator BasicSubscribeListenTests()
    {
        EventCaster.Main.ClearAllCachedEvents();
        yield return null;
        NotificationListenerA a = new NotificationListenerA();
        NotificationListenerB[] numListeners = new NotificationListenerB[5];
        for (int x = 0; x < numListeners.Length; x++)
        {
            numListeners[x] = new NotificationListenerB();
        }
        
        Assert.False(a.HasReceivedNotification, "Incorrect starting state (A)");
        foreach (var b in numListeners) Assert.False(b.HasReceivedNotification, "Incorrect starting state (B)");
        yield return null;
        
        EventCaster.CastGlobally(new BasicNotificationCastableRequest());
        Assert.True(a.HasReceivedNotification, "A Failed to respond to notification");
        foreach (var b in numListeners) Assert.False(b.HasReceivedNotification, "B responded when it shouldn't have");
        a.HasReceivedNotification = false;
        yield return null;

        EventCaster.CastGlobally(new NumericalNotificationCastableRequest(5));
        Assert.False(a.HasReceivedNotification, "A responded when it shouldn't have");
        foreach (var b in numListeners)
        {
            Assert.True(b.HasReceivedNotification, $"B failed to respond");
            Assert.AreEqual(5, b.Number, $"The number was not as expected after the event");
        }
    }
    
    [Test]
    public void CachedEventSupportTests()
    {
        EventCaster.Main.ClearAllCachedEvents();
        NotificationListenerB b1 = new NotificationListenerB();
        Assert.AreEqual(b1.Number, -1, "The initial number value is not what was expected.");
        EventCaster.CastGlobally(new NumericalNotificationCastableRequest(3));
        Assert.AreEqual(b1.Number, 3, "The first listener did not set the value as expected.");
        NotificationListenerB b2 = new NotificationListenerB();
        Assert.AreEqual(b2.Number, 3, "The second listener did not receive the cached event.");
        EventCaster.CastGlobally(new NumericalNotificationCastableRequest(8));
        NotificationListenerB b3 = new NotificationListenerB();
        Assert.AreEqual(b3.Number, 8, "The third listener did not receive the correct cached event.");
        EventCaster.Main.ClearAllCachedEvents();
        NotificationListenerB b4 = new NotificationListenerB();
        Assert.AreEqual(b4.Number, -1, "A listener created after clearing the cache received a cached event anyway.");
    }

    [Test]
    public void HierarchicalEventTypeSupportTests()
    {
        EventCaster.Main.ClearAllCachedEvents();
        NotificationListenerB b = new NotificationListenerB();
        NotificationListenerC c = new NotificationListenerC();
        Assert.AreEqual(c.str, "", "The initial state of C was incorrect");
        EventCaster.CastGlobally(new NumberAndStringNotificationCastableRequest(9, "Doo"));
        Assert.AreEqual(c.str, "Doo", "C failed to receive NumberAndStringNotificationEvent");
        Assert.AreEqual(b.Number, 9, "B failed to receive NumberAndStringNotificationEvent");
    }

    [UnityTest]
    public IEnumerator LocalEventTests()
    {
        GameObject casterParent = new GameObject("TestParent");
        var testCaster = casterParent.AddComponent<EventCaster>();
        GameObject casterChild = new GameObject("TestChild");
        casterChild.transform.SetParent(casterParent.transform);
        var testChildListener = casterChild.AddComponent<BehaviourListener>();
        var testParentListener = casterParent.AddComponent<BehaviourListener>();
        
        // the control listener should receive no events
        GameObject controlParent = new GameObject("ControlParent");
        var controlListener = controlParent.AddComponent<BehaviourListener>();
        
        yield return 0;

        Assert.AreEqual(testChildListener.Number, -1, "Initial state failure on test child");
        Assert.AreEqual(testParentListener.Number, -1, "Initial state failure on test parent");
        Assert.AreEqual(controlListener.Number, -1, "Initial state failure on control parent");

        yield return 0;
        testCaster.Cast(new NumericalNotificationCastableRequest(11));
        yield return 0;
        Assert.AreEqual(testChildListener.Number, 11,  "Notification receipt failure on test child");
        Assert.AreEqual(testParentListener.Number, 11, "Notification receipt failure on test parent");
        Assert.AreEqual(controlListener.Number, -1,    "Notification receipt false positive on control parent");

        yield return 0;
        var testCachedEventListener = casterChild.AddComponent<BehaviourListener>();
        var controlCachedEventListener = controlParent.AddComponent<BehaviourListener>();
        yield return 0;
        Assert.AreEqual(testCachedEventListener.Number, 11, "Notification receipt failure on test parent with cached value");
        Assert.AreEqual(controlCachedEventListener.Number, -1,    "Notification receipt false positive on control parent with cached value");
        
        // Test that global events are still received by all
        EventCaster.CastGlobally(new NumericalNotificationCastableRequest(7));
        Assert.AreEqual(testChildListener.Number, 7,  "Global notification receipt failure on test child");
        Assert.AreEqual(testParentListener.Number, 7, "Global notification receipt failure on test parent");
        Assert.AreEqual(controlListener.Number, 7,    "Global notification receipt failure on control parent");
    }

    [UnityTest]
    public IEnumerator AsyncEventExecutionTests()
    {
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;
    }

    public class InitCastableRequest : CastableRequestBase<bool>
    {
        
    }
    public class BasicNotificationCastableRequest : CastableRequestBase<bool>
    {
        
    }

    public class NumericalNotificationCastableRequest : CastableRequestBase<bool>
    {
        public int TheNumber { get; }
        public override bool AllowThisEventToCache => true;

        public NumericalNotificationCastableRequest(int theNumber)
        {
            TheNumber = theNumber;
        }
    }

    public class NumberAndStringNotificationCastableRequest : NumericalNotificationCastableRequest
    {
        public NumberAndStringNotificationCastableRequest(int number, string theString) : base(number)
        {
            TheString = theString;
        }
        public string TheString { get; }
        public override Type ChainUntilBaseType => typeof(NumericalNotificationCastableRequest);
    }

    public class NotificationListenerA : IRequestCastListener<BasicNotificationCastableRequest, bool>
    {
        public NotificationListenerA()
        {
            EventCaster.Main.Subscribe(this);
        }
        
        public bool HasReceivedNotification = false;
        public UniTask<bool> Handle(BasicNotificationCastableRequest evt)
        {
            HasReceivedNotification = true;
            return new UniTask<bool>(true);
        }

        async UniTask IEventCastListener<BasicNotificationCastableRequest>.Handle(BasicNotificationCastableRequest evt)
        {
            await Handle(evt);
        }
    }
    
    public class NotificationListenerB : IRequestCastListener<NumericalNotificationCastableRequest, bool>
    {
        public NotificationListenerB()
        {
            EventCaster.Main.Subscribe(this);
        }
        
        public bool HasReceivedNotification = false;
        public int Number = -1;
        public UniTask<bool> Handle(NumericalNotificationCastableRequest evt)
        {
            HasReceivedNotification = true;
            Number = evt.TheNumber;
            return new UniTask<bool>(true);
        }

        async UniTask IEventCastListener<NumericalNotificationCastableRequest>.Handle(NumericalNotificationCastableRequest evt)
        {
            await Handle(evt);
        }
    }

    public class NotificationListenerC : IRequestCastListener<NumberAndStringNotificationCastableRequest, bool>
    {
        public NotificationListenerC()
        {
            EventCaster.Main.Subscribe(this);
        }
        public bool HasReceivedNotification = false;
        public int Number = -1;
        public string str = "";
        
        public UniTask<bool> Handle(NumberAndStringNotificationCastableRequest evt)
        {
            HasReceivedNotification = true;
            Number = evt.TheNumber;
            str = evt.TheString;
            return new UniTask<bool>(true);
        }

        async UniTask IEventCastListener<NumberAndStringNotificationCastableRequest>.Handle(NumberAndStringNotificationCastableRequest evt)
        {
            await Handle(evt);
        }
    }

    public class BehaviourListener : MonoBehaviour, IRequestCastListener<NumericalNotificationCastableRequest, bool>
    {
        private void Awake()
        {
            bool found = false;
            var tParent = transform;
            while (tParent != null)
            {
                var caster = tParent.GetComponent<EventCaster>();
                if (caster != null)
                {
                    caster.Subscribe(this);
                    found = true;
                    break;
                }
                tParent = tParent.parent;
            }

            if (!found)
            {
                EventCaster.Main.Subscribe(this);
            }
        }

        public int Number = -1;
        public UniTask<bool> Handle(NumericalNotificationCastableRequest evt)
        {
            Number = evt.TheNumber;
            return new UniTask<bool>(true);
        }

        async UniTask IEventCastListener<NumericalNotificationCastableRequest>.Handle(NumericalNotificationCastableRequest evt)
        {
            await Handle(evt);
        }
    }
}
