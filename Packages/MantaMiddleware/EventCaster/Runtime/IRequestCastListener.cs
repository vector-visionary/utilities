using Cysharp.Threading.Tasks;

namespace MantaMiddleware.Events
{
    public interface IEventCastListenerBase
    {
    }

    public interface IEventCastListener<in EventT> : IEventCastListenerBase where EventT : CastableEventBase
    {
        UniTask Handle(EventT evt);
    }
    public interface IRequestCastListener<in EventT, ReturnT> : IEventCastListener<EventT> where EventT : CastableRequestBase<ReturnT>
    {
        /// <summary>
        /// Handles the event of the given type. This may be an async method,
        /// and if anything is awaited, this will also allow the original event
        /// sender to await.
        /// If await is not used, simply return UniTask<bool>(true) or UniTask<bool>(false)
        /// </summary>
        /// <param name="evt">The event being handled.</param>
        /// <returns>True if the event was "used" by this listener.</returns>
        new UniTask<ReturnT> Handle(EventT evt);
    }
}