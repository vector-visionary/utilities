using System.Collections.Generic;

namespace MantaMiddleware.Events
{
    public class TrackedCastableRequestBase<ReturnT> : CastableRequestBase<ReturnT>
    {
        private Dictionary<int, IProgressReport> progressByListenerHashes =
            new Dictionary<int, IProgressReport>();

        private HashSet<IProgressWatcher> allWatchers = new HashSet<IProgressWatcher>();
        public void MarkUsageState<T>(T listener, IProgressReport state) where T : IEventCastListenerBase
        {
            int lHash = listener.GetHashCode();
            if (progressByListenerHashes.ContainsKey(lHash)) progressByListenerHashes.Remove(lHash);
            progressByListenerHashes.Add(lHash, state);
            UpdateProgressWatchers();
        }

        public void WatchProgress(IProgressWatcher watcher)
        {
            allWatchers.Add(watcher);
        }

        private void UpdateProgressWatchers()
        {
            float prog = GetProgress();
            foreach (var watcher in allWatchers)
            {
                watcher.SetProgress(prog);
            }
        }

        public float GetProgress()
        {
            if (progressByListenerHashes.Count == 0) return 1f;
            float progTotal = 0f;
            float sizeTotal = 0f;
            foreach (var kvp in progressByListenerHashes)
            {
                progTotal += kvp.Value.Progress * kvp.Value.EstimatedSize;
                sizeTotal += kvp.Value.EstimatedSize;
            }

            return progTotal / sizeTotal;
        }
    }

    public class SimpleProgressState : IProgressReport
    {
        public SimpleProgressState(float progress, float estimatedSize = 1f)
        {
            if (progress < 0f) _progress = 0f;
            else if (progress > 1f) _progress = 1f;
            else _progress = progress;

            if (estimatedSize > 1f)
            {
                _estimatedSize = estimatedSize;
            }
        }

        public float Progress => _progress;

        private float _progress = 0f;

        public float EstimatedSize => _estimatedSize;

        private float _estimatedSize = 1f;
    }

    public interface IProgressReport
    {
        float Progress { get; }
        float EstimatedSize { get; }
    }

    public interface IProgressWatcher
    {
        void SetProgress(float prog);
    }
}