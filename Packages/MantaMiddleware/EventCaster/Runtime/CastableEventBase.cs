using System;

namespace MantaMiddleware.Events
{
    /// <summary>
    /// Base class to derive and extend in order to send events using the
    /// <see cref="EventCaster"/> system.
    /// </summary>
    public class CastableEventBase
    {
        /// <summary>
        /// If true, this event will be cached (potentially replacing previously cached events of this type);
        /// new subscribers to this event type will receive the most recently sent event
        /// immediately upon subscribing. This is useful for events which communicate
        /// a new persistent state, such as player preference changes.
        /// This should NOT be used for any events which reference or store objects
        /// with potentially large amounts of data; doing so could cause the data to be held
        /// in memory longer than it should be.
        /// </summary>
        public virtual bool AllowThisEventToCache => false;
        
        internal bool ChainToBaseClass
        {
            get
            {
                if (ChainUntilBaseType == null) return false;
                if (ChainUntilBaseType == this.GetType()) return false;
                return true;
            }
        }
        
        
        /// <summary>
        /// If defined, an event of this type will also be sent
        /// to listeners of all base type up the inheritance chain
        /// of this event, until (and including) the base class
        /// defined here. <see cref="ChainUntilBaseType"/> should
        /// be in the direct inheritance chain of this event type,
        /// or unpredictable behavior may occur.
        /// For example, if CarSpawnEvent inherits VehicleSpawnEvent,
        /// a listener may subscribe to VehicleSpawnEvent only;
        /// CarSpawnEvent will chain to VehicleSpawnEvent's listener
        /// as well.
        /// </summary>
        public virtual Type ChainUntilBaseType => null;
    }
}