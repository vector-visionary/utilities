using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEditor.VersionControl;
using UnityEngine;

namespace MantaMiddleware.Events
{
    internal abstract class EventListenerSetBase {
        public abstract void Subscribe(IEventCastListenerBase listenerBase);
        public abstract void CastAndForget(CastableEventBase evt);
    }

    internal class EventListenerSet<EventT> : EventListenerSetBase where EventT : CastableEventBase
    {
        public override void Subscribe(IEventCastListenerBase listenerBase)
        {
            if (listenerBase is IEventCastListener<EventT> actualListener)
            {
                if (!eventListeners.Contains(actualListener))
                {
                    eventListeners.Add(actualListener);
                }
            }
        }

        public override void CastAndForget(CastableEventBase evt)
        {
            InvokeAllAndForget(evt as EventT);
        }

        public virtual void InvokeAllAndForget(EventT evtActual)
        {
            foreach (var listener in AllListeners)
            {
                try
                {
                    if (listener is IEventCastListener<EventT> listenerActual)
                    {
                        listenerActual.Handle(evtActual);
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(
                        $"{nameof(EventListenerSet<EventT>)}: exception in event handler {GetName(listener)} follows");
                    Debug.LogException(e);
                }
            }
        }

        protected virtual IEnumerable<IEventCastListenerBase> AllListeners
        {
            get
            {
                foreach (var listener in eventListeners) yield return listener;
            }
        }
        private HashSet<IEventCastListener<EventT>> eventListeners = new HashSet<IEventCastListener<EventT>>();


        protected static string GetName(object listener)
        {
            if (listener == null)
            {
                return "NULL";
            }
            if (listener is Component component)
            {
                return $"{component.gameObject.name}.{listener.GetType()}";
            }

            return listener.GetType().ToString();
        }
    }
    internal class EventListenerSet<EventT, ReturnT> : EventListenerSet<EventT> where EventT : CastableRequestBase<ReturnT>
    {
        public async UniTask<ReturnT> InvokeFirst(EventT evtActual)
        {
            var tasks = new List<UniTask<ReturnT>>();

            foreach (var listener in requestListeners)
            {
                try
                {
                    return await listener.Handle(evtActual);
                }
                catch (Exception e)
                {
                    Debug.Log(
                        $"{nameof(EventListenerSet<EventT, ReturnT>)}: exception in event handler {GetName(listener)} follows");
                    Debug.LogException(e);                        
                }
            }

            return default;
        }

        public async UniTask<ReturnT[]> InvokeAll(EventT evtActual)
        {
            var tasks = new List<UniTask<ReturnT>>();

            //first, kick off all tasks:
            foreach (var listener in requestListeners)
            {
                try
                {
                    tasks.Add(listener.Handle(evtActual));
                }
                catch (Exception e)
                {
                    Debug.Log(
                        $"{nameof(EventListenerSet<EventT, ReturnT>)}: exception in event handler {GetName(listener)} follows");
                    Debug.LogException(e);
                }
            }

            return await UniTask.WhenAll(tasks);
        }
        
        public override void InvokeAllAndForget(EventT evtActual)
        {
            foreach (var listener in AllListeners)
            {
                try
                {
                    if (listener is IEventCastListener<EventT> listenerActual)
                    {
                        listenerActual.Handle(evtActual);
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(
                        $"{nameof(EventListenerSet<EventT, ReturnT>)}: exception in event handler {GetName(listener)} follows");
                    Debug.LogException(e);
                }
            }
        }



        public override void Subscribe(IEventCastListenerBase listenerBase)
        {
            if (listenerBase is IRequestCastListener<EventT, ReturnT> listener)
            {
                if (!requestListeners.Contains(listener))
                {
                    requestListeners.Add(listener);
                }
            }
        }

        protected override IEnumerable<IEventCastListenerBase> AllListeners {
            get
            {
                foreach (var listener in requestListeners) yield return listener;
            }
        }
        private HashSet<IRequestCastListener<EventT, ReturnT>> requestListeners = new HashSet<IRequestCastListener<EventT, ReturnT>>();
    }
}