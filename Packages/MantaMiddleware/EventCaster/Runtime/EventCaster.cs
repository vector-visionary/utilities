using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using MantaMiddleware.Utilities;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

namespace MantaMiddleware.Events
{
    /// <summary>
    /// The main class of the EventCaster system. The <see cref="Main"/> accessor
    /// is a singleton that can be used for all events, or local casters may be created
    /// to manage events within a localized scope.
    ///
    /// Listeners (<see cref="IEventCastListener{T}"/>) must call <see cref="Subscribe{T}"/>
    /// to receive notifications.
    /// </summary>
    public class EventCaster : MonoBehaviour
    {
        /// <summary>
        /// Sends the event to any listener subscribed to any EventCaster.
        /// </summary>
        /// <param name="evt">The event to send to all listeners.</param>
        /// <returns>A bool representing whether any listener has reported that it
        /// has "used" the event</returns>
        public static async UniTask<IEnumerator<ReturnT>> CastGloballyWithResult<EventT, ReturnT>(EventT evt) where EventT : CastableRequestBase<ReturnT>
        {
            HashSet<UniTask<IEnumerator<ReturnT>>> allCasterTasks = new HashSet<UniTask<IEnumerator<ReturnT>>>();
            foreach (var caster in AllCasters)
            {
                allCasterTasks.Add(caster.CastWithResult<EventT, ReturnT>(evt));
            }

            var rtn = await UniTask.WhenAll(allCasterTasks);
            return IterateArrays(rtn);
        }

        public static void CastGlobally<EventT>(EventT evt) where EventT : CastableEventBase
        {
            foreach (var caster in AllCasters)
            {
                caster.Cast<EventT>(evt);
            }
        }

        private static IEnumerator<T> IterateArrays<T>(IEnumerator<T>[] arrays)
        {
            foreach (var arr in arrays)
            {
                foreach (var rtn in (IEnumerable<T>)arr)
                {
                    yield return rtn;
                }
            }
        }
        private static IEnumerator<T> IterateArrays<T>(T[][] arrays)
        {
            foreach (var arr in arrays)
            {
                foreach (var rtn in arr)
                {
                    yield return rtn;
                }
            }
        }

        /// <summary>
        /// A singleton instance of EventCaster.
        /// Created upon first access, then persistent afterwards.
        /// </summary>
        public static EventCaster Main
        {
            get
            {
                if (_main == null)
                {
                    GameObject newGO = new GameObject($"{nameof(EventCaster)} (Main)");
                    DontDestroyOnLoad(newGO);
                    _main = newGO.AddComponent<EventCaster>();
                }
                
                return _main;
            }
        }

        private static EventCaster _main;

        private static readonly HashSet<EventCaster> AllCasters = new HashSet<EventCaster>();


        private void OnEnable()
        {
            AllCasters.Add(this);
        }

        private void OnDisable()
        {
            AllCasters.Remove(this);
        }



        /// <summary>
        /// Sends the event to all relevant listeners subscribed to this caster.
        /// Listeners may be subscribed to the specific event type sent, or
        /// (if the event has defined <see cref="ChainToBaseClass"/>) subscribed to
        /// a parent class of the event.
        /// </summary>
        /// <param name="evt">The event to send.</param>
        /// <returns>A bool representing whether any listener has reported that it
        /// has "used" the event.</returns>
        public async UniTask<IEnumerator<ReturnT>> CastWithResult<EventT, ReturnT>(EventT evt) where EventT : CastableRequestBase<ReturnT>
        {
            var evtT = new EventTracker<EventT, ReturnT>(evt);
            HashSet<UniTask<ReturnT[]>> runningTasks = new HashSet<UniTask<ReturnT[]>>();

            if (_listeners.TryGetValue(evt.GetType(), out var lSet))
            {
                if (lSet is EventListenerSet<EventT, ReturnT> lSetActual)
                {
                    runningTasks.Add(lSetActual.InvokeAll(evt));
                }
            }

            if (evt.ChainToBaseClass)
            {
                ChainToBaseClass_All(evtT, runningTasks);
            }

            var rtn = await UniTask.WhenAll(runningTasks);
            if (evt.AllowThisEventToCache) CacheEvent(evt);
            return IterateArrays(rtn);
        }

        public void Cast<EventT>(EventT evt) where EventT : CastableEventBase
        {
            var evtT = new EventTracker<EventT>(evt);

            if (_listeners.TryGetValue(evt.GetType(), out var lSet))
            {
                if (lSet is EventListenerSet<EventT> lSetActual)
                {
                    lSetActual.CastAndForget(evt);
                }
            }

            if (evt.ChainToBaseClass)
            {
                ChainToBaseClass(evtT);
            }

            if (evt.AllowThisEventToCache) CacheEvent(evt);
        }

        private void ChainToBaseClass_All<EventT, ReturnT>(EventTracker<EventT, ReturnT> eventTracker, HashSet<UniTask<ReturnT[]>> tasks) where EventT : CastableRequestBase<ReturnT>
        {
            var baseType = eventTracker.thisEvent.GetType().BaseType;
            while (true)
            {
                Debug.Log($"ChainToBaseClass - {baseType}");
                if (_listeners.TryGetValue(baseType, out var baseLSet))
                {
                    var task = eventTracker.InvokeAll(baseLSet as EventListenerSet<EventT, ReturnT>);
                    if (tasks != null) tasks.Add(task);
                }

                if (baseType == eventTracker.thisEvent.ChainUntilBaseType) break;
                baseType = baseType.BaseType;
                if (baseType == null) break;
                    
            }
            
        }        
        
        private void ChainToBaseClass<EventT>(EventTracker<EventT> eventTracker) where EventT : CastableEventBase
        {
            var baseType = eventTracker.thisEvent.GetType().BaseType;
            while (true)
            {
                if (_listeners.TryGetValue(baseType, out var baseLSet))
                {
                    if (baseLSet is EventListenerSet<EventT> forgettableSet)
                    {
                        eventTracker.InvokeAll(forgettableSet);
                    }
                    else
                    {
                        eventTracker.InvokeAll_Untyped(baseLSet);
                    }
                }

                if (baseType == eventTracker.thisEvent.ChainUntilBaseType) break;
                baseType = baseType.BaseType;
                if (baseType == null) break;
                    
            }
            
        }

        private void CacheEvent<EventT>(EventT evt) where EventT : CastableEventBase
        {
            if (_cachedEvents.ContainsKey(evt.GetType()))
            {
                _cachedEvents[evt.GetType()] = evt;
            }
            else
            {
                _cachedEvents.Add(evt.GetType(), evt);
            }
        }

        /// <summary>
        /// Subscribes the given <see cref="IEventCastListener{T}"/> to the event of its type argument.
        /// If the listener can handle more than one type of event, each will have to be
        /// subscribed individually, using the type parameters.
        /// If the event type can cache, and an event of the type has been cast previously,
        /// then the cached event will be IMMEDIATELY send to the listener.
        /// </summary>
        /// <param name="listener">The listener to handle the given type of event.</param>
        /// <typeparam name="EventT">The type of request to listen for.
        /// <typeparam name="ReturnT">The return type of EventT.
        /// Note that this may be a base class of multiple events if <see cref="ChainToBaseClass"/>
        /// is defined in the event class.</typeparam>
        public async void Subscribe<EventT, ReturnT>(IRequestCastListener<EventT, ReturnT> listener) where EventT : CastableRequestBase<ReturnT>
        {
            if (!_listeners.ContainsKey(typeof(EventT)))
            {
                _listeners.Add(typeof(EventT), new EventListenerSet<EventT, ReturnT>());
            }

            _listeners[typeof(EventT)].Subscribe(listener);

            if (_cachedEvents.TryGetValue(typeof(EventT), out var cached))
            {
                if (cached is EventT cachedAsT)
                {
                    await new SwitchToMainThreadAwaitable();
                    await listener.Handle(cachedAsT);
                }
            }
        }



        public async void Subscribe<EventT>(IEventCastListener<EventT> listener) where EventT : CastableEventBase
        {
            if (!_listeners.ContainsKey(typeof(EventT)))
            {
                _listeners.Add(typeof(EventT), new EventListenerSet<EventT>());
            }

            _listeners[typeof(EventT)].Subscribe(listener);

            if (_cachedEvents.TryGetValue(typeof(EventT), out var cached))
            {
                if (cached is EventT cachedAsT)
                {
                    await new SwitchToMainThreadAwaitable();
                    await listener.Handle(cachedAsT);
                }
            }
            
        }
        
        public void Subscribe<EventAT, EventBT>(IEventCastListenerBase listener) 
            where EventAT : CastableEventBase
            where EventBT : CastableEventBase
        {
            if (listener is IEventCastListener<EventAT> listenerA) Subscribe(listenerA);
            if (listener is IEventCastListener<EventBT> listenerB) Subscribe(listenerB);
        }
        public void Subscribe<EventAT, EventBT, EventCT>(IEventCastListenerBase listener) 
            where EventAT : CastableEventBase
            where EventBT : CastableEventBase
            where EventCT : CastableEventBase
        {
            if (listener is IEventCastListener<EventAT> listenerA) Subscribe(listenerA);
            if (listener is IEventCastListener<EventBT> listenerB) Subscribe(listenerB);
            if (listener is IEventCastListener<EventCT> listenerC) Subscribe(listenerC);
        }
        public void Subscribe<EventAT, EventBT, EventCT, EventDT>(IEventCastListenerBase listener) 
            where EventAT : CastableEventBase
            where EventBT : CastableEventBase
            where EventCT : CastableEventBase
            where EventDT : CastableEventBase
        {
            if (listener is IEventCastListener<EventAT> listenerA) Subscribe(listenerA);
            if (listener is IEventCastListener<EventBT> listenerB) Subscribe(listenerB);
            if (listener is IEventCastListener<EventCT> listenerC) Subscribe(listenerC);
            if (listener is IEventCastListener<EventDT> listenerD) Subscribe(listenerD);
        }

        private readonly Dictionary<Type, CastableEventBase> _cachedEvents = new Dictionary<Type, CastableEventBase>();

        private readonly Dictionary<Type, EventListenerSetBase> _listeners = new Dictionary<Type, EventListenerSetBase>();

        /// <summary>
        /// Clears all cached events; useful to return the caster
        /// to a clean state.
        /// </summary>
        public void ClearAllCachedEvents()
        {
            _cachedEvents.Clear();
        }

        /// <summary>
        /// Removes any cached event for the given type.
        /// </summary>
        /// <typeparam name="T">The type of cached event to clear.</typeparam>
        public void ClearCachedEvent<T>() where T : EventBase
        {
            if (_cachedEvents.ContainsKey(typeof(T)))
            {
                _cachedEvents.Remove(typeof(T));
            }
        }

        public static EventCaster GetCasterFor(Component target)
        {
            return target.transform.GetComponentAlongParentage<EventCaster>();
        }
    }

    public class EventTracker<EventT, ReturnT> where EventT : CastableRequestBase<ReturnT>
    {
        public EventTracker(EventT thisEvent)
        {
            this.thisEvent = thisEvent;
        }

        public readonly EventT thisEvent;
        
        internal async UniTask<ReturnT[]> InvokeAll(EventListenerSet<EventT, ReturnT> listenerSet)
        {
            var rtn = await listenerSet.InvokeAll(thisEvent as EventT);

            return rtn;
        }
    }

    public class EventTracker<EventT> : EventTrackerBase where EventT : CastableEventBase
    {
        public EventTracker(EventT thisEvent)
        {
            this.thisEvent = thisEvent;
        }

        public readonly EventT thisEvent;

        internal override void InvokeAll_Untyped(EventListenerSetBase listenerSet)
        {
            listenerSet.CastAndForget(thisEvent);
        }

        internal void InvokeAll(EventListenerSet<EventT> listenerSet)
        {
            listenerSet.InvokeAllAndForget(thisEvent as EventT);
        }
        
    }

    public abstract class EventTrackerBase
    {
        internal abstract void InvokeAll_Untyped(EventListenerSetBase listenerSet);
    }
}
