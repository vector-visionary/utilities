An event management system.

# Features

* Asynchronous execution of events. Cast an event, wait for all listeners to complete their handling of the event (without halting game execution), and then resume execution within the same block of code. (This uses the UniTask library)
* Request handling, even with multicast events. Broadcast a request, await the request to let all handlers respond, and get back all the results as an array. Or, just request a single result from the first (or only) handler.
* Hierarchical event types support. If your listener has subscribed to the VehiclePositionChangeEvent, it can respond to derived event classes CarPositionChangeEvent and BicyclePositionChangeEvent with no additional code. This is fully and easily controllable in the event class implementations.
* Cached event support. If the event type is set to allow it, the system will cache the most recent event of a given type; new listeners subscribing to that event type will instantly receive that event. Especially useful for config change events; the cached config event can replace a static storage of config data, reducing redundant code (initialization and live updates get handled with the same line of code).
* Event locality. While the system supports global events, it is also identically easy for listeners and casters to use a local EventCaster instance. This may live on a prefab, for example, and SwordSwingEvent may be cast on a specific character without other character instances needing to respond to it.
* Event execution state tracking. Broadcast an event, and show a spinner or progress bar as that event's listeners work through their handlers. (coming soon)
* Prioritized event handlers. A broadcasted event may be specified to only be handled by handlers which declare themselves best suited to handle the particular event. (coming soon)

# Classes & Interfaces

## EventCaster

The main class of the package. The Main static accessor will spawn the global caster if it is needed; otherwise, local instances can be added to objects and found like any other MonoBehaviour.

In all methods, ReturnType and EventType are determined by the event and/or listener.

### Static

void **CastGlobally(event)**

Broadcasts the event to all listeners on the all instances, ignoring any returned results. If the event type supports it, caches the event for future listeners.

IEnumerator\<ReturnType\> **CastGloballyWithResult(event)**

Broadcasts the event to all listeners on all instances, waits for them to finish, and returns an enumerator for all results. If the event type supports it, caches the event for future listeners.

EventCaster **Main**

The main/global instance that handles global listeners.

### Instance

IEnumerator\<ReturnType\> **CastWithResult(event)**

Broadcasts the event to all listeners on the this instance, waits for them to finish, and returns an enumerator for all results. If the event type supports it, caches the event for future listeners.

void **Cast(event)**

Broadcasts the event to all listeners on the this instance, ignoring any returned results. If the event type supports it, caches the event for future listeners.

void **Subscribe\<EventType\>(eventListener)**

Begins listening to this event type. If one of these events is cached, immediately calls the handler with the cached event.

void **Subscribe\<EventType, ReturnType\>(requestListener)**

Begins listening to this request type. If one of these events is cached, immediately calls the handler with the cached event.

void **ClearAllCachedEvents()**

void **ClearCachedEvent\<EventType\>()**

If there are any events cached of type EventType, they will be cleared. This does not affect cached events of inherited types, only the type of event that was specifically cast.

## IEventCastListener\<EventType\>

Implement this interface on your listener to handle events of type EventType. You must call EventCaster.Subscribe(this) to start listening.

## IRequestCastListener\<EventType, ReturnType\>

Implement this interface on your listener to handle requests of type EventType (which must be CastableRequestBase\<EventType\> and return ReturnType). You must call EventCaster.Subscribe(this) to start listening.

## CastableEventBase

Create your own event types by deriving from this class. Your event should contain any data needed for the event handlers, and a constructor to set all that data. You may also override the following if you want:

**AllowThisEventToCache** (default false): if true, the most recent event of this type will be remembered by the EventCaster; new listeners will immediately respond to the cached event. Ideal for things like configuration change events. Strongly recommend that this is ONLY used for regular data, and not references to assets or complex objects; caching those events may cause assets to unexpectedly remain in memory past their usefulness.

**ChainUntilBaseType** (default null): if non-null, casting this (or a derived) event will chain up the event hierarchy until it reaches this class (inclusive), calling the listeners for those types in addition to the specific type being cast. Typically, this will be implemented on SomeClass as typeof(SomeClass), so that all inheritors will chain until the base class that defines it.

## CastableRequestBase

Create requests by deriving from this class. Like CastableEventBase, this should contain any data needed to fulfill the request.

The ReturnType works just like the return type of a method.

## TrackedCastableRequestBase 

WARNING: This class is under development, untested, and probably doesn't work except as a normal event, and it is subject to change.

Derive from this event class if you want to be able to track the progress of an event (e.g. a loading bar that automatically shows up for particular events).

To use this effectively, both the main listener and the object that will show progress should subscribe to the event. For example, if using a DownloadAssetEvent, the class that handles the downloading should subscribe to the event, as should the progress bar that will display the download's progress. If multiple listeners respond to the event, the progress bar will show the combined progress of all of the listeners.

It is recommended to take advantage of the hierarchical event support to make the appropriate progress bar show for any of multiple kinds of events.

**MarkUsageState(listener, state)**

In an event handler, call event.MarkUsageState to report on the progress of the task by this listener. Only listeners that call MarkUsageState at any point will be considered when calculating the total progress. Call this anytime the progress changes with the new state information.

**WatchProgress(watcher)**

The listener which you want to display your progress report should call this method. It will register the listener to receive new progress states via the IProgressWatcher.SetProgress callback.

**GetProgress()**

You likely don't need to call this directly at any point, but this will calculate the progress of all listeners, averaging and weighting them by their estimated size.
