# ApplicationConfig Readme

A system designed to store player configuration and setup information in JSON files.

## Advantages over PlayerPrefs

* Supports complex data structures, not just a flat list of primitives
* More efficient
* Not susceptible to simple typos causing tough errors
* Multiple sets of configs supported (either multiple options of the same type, or multiple types of configurations)
* Easy to see values for debugging purposes
* Only saves values that are not the defaults; prevents future version updates from being in unexpected "leftover" states
* Can watch filesystem for changes to the prefs file for debug purposes


## Usage

The first step is to derive from ApplicationConfig with your own class. In this class, add any fields containing information you'd like to save.

Create an instance of your derived class in a manager class. Call **Load** on this class on initialization; the filename parameter is relative to Application.persistentDataPath. If the config file exists, it will be loaded; if not, it will be created, and a config object with default values will be returned.

After changing any values in this config object, call **Save** to output it back to the persistent directory.

```c#
public class MyGameConfig : ApplicationConfig {
    [DefaultValue(0.5f)] public float volume = 0.5f; // use both DefaultValue and the initialized values for consistent behavior
    [DefaultVector3Value(1f, 2f, 3f)] public Vector3 favoriteSpot = new Vector3(1f, 2f, 3f);
}

// in a manager class
public MyGameConfig config;

void Awake() {
    config = MyGameConfig.Load<MyGameConfig>("mygame.json");
    Debug.Log($"Loaded config; volume level is {config.volume}");
}

void Update() {
    if (Input.GetKeyDown(KeyCode.A)) {
        config.volume = 1f;
        config.Save();
    }
}
```