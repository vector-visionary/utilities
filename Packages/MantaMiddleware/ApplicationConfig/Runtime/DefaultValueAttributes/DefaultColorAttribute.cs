﻿using System.ComponentModel;
using UnityEngine;

namespace MantaMiddleware.ApplicationConfiguration
{
	/// <summary>
	/// Allows a default serialization value for a Color object, which is normally not possible (since constructors are not allowed in DefaultValue attributes).
	/// </summary>
	public class DefaultColorAttribute : DefaultValueAttribute
    {
        private Color color = Color.black;
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultColorAttribute"/> class.
        /// This defines what the default value will be for the linked field or property.
        /// </summary>
        /// <param name="r">Red (0 to 1).</param>
        /// <param name="g">Green (0 to 1).</param>
        /// <param name="b">Blue (0 to 1).</param>
        public DefaultColorAttribute(float r, float g, float b)
            : base(r)
        {
            color = new Color(r, g, b, 1f);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultColorAttribute"/> class.
        /// This defines what the default value will be for the linked field or property.
        /// </summary>
        /// <param name="r">Red (0 to 1).</param>
        /// <param name="g">Green (0 to 1).</param>
        /// <param name="b">Blue (0 to 1).</param>
        /// <param name="a">Alpha (0 to 1).</param>
        public DefaultColorAttribute(float r, float g, float b, float a)
            : base(r)
        {
            color = new Color(r, g, b, a);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultColorAttribute"/> class.
        /// This defines what the default value will be for the linked field or property.
        /// </summary>
        /// <param name="c">The color.</param>
        public DefaultColorAttribute(Color c)
            : base(c.r)
        {
            color = c;
        }

        /// <summary>
        /// Gets the value of the property.
        /// </summary>
        public override object Value => color;
    }
}