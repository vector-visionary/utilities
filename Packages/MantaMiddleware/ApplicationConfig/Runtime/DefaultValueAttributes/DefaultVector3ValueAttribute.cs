﻿using System.ComponentModel;
using UnityEngine;

namespace MantaMiddleware.ApplicationConfiguration
{
    /// <summary>
    /// Allows a default serialization value for a Vector3 object, which is normally not possible (since constructors are not allowed in DefaultValue attributes).
    /// </summary>
    public class DefaultVector3ValueAttribute : DefaultValueAttribute
    {
        private Vector3 v3Value = Vector3.zero;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultVector3ValueAttribute"/> class.
        /// This defines what the default value will be for the linked field or property.
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="y">Y component.</param>
        /// <param name="z">Z component.</param>
        public DefaultVector3ValueAttribute(float x, float y, float z)
            : base(x)
        {
            v3Value = new Vector3(x, y, z);
        }

        /// <summary>
        /// Gets the value of the property.
        /// </summary>
        public override object Value => v3Value;
    }
}