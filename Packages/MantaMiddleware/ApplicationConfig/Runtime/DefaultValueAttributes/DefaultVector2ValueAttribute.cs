﻿using System.ComponentModel;
using UnityEngine;

namespace MantaMiddleware.ApplicationConfiguration
{
    /// <summary>
    /// Allows a default serialization value for a Vector2 object, which is normally not possible (since constructors are not allowed in DefaultValue attributes).
    /// </summary>
    public class DefaultVector2ValueAttribute : DefaultValueAttribute
    {
        private Vector2 v2Value = Vector2.zero;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultVector3ValueAttribute"/> class.
        /// This defines what the default value will be for the linked field or property.
        /// </summary>
        /// <param name="x">X component.</param>
        /// <param name="y">Y component.</param>
        public DefaultVector2ValueAttribute(float x, float y)
            : base(x)
        {
            v2Value = new Vector2(x, y);
        }

        /// <summary>
        /// Gets the value of the property.
        /// </summary>
        public override object Value => v2Value;
    }
}