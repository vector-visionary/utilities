using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Cysharp.Threading.Tasks;
using MantaMiddleware.LogUtilities;
using MantaMiddleware.MantaUtilities.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace MantaMiddleware.ApplicationConfig
{
    /// <summary>
    /// A base class to be derived which will load and maintain a json file matching the derived class's structure and allowing for persistent values.
    /// </summary>
    [Serializable]
    public class ApplicationConfig : IDisposable
    {
        private static HashSet<string> savingPaths = new HashSet<string>();
        private static HashSet<string> readingPaths = new HashSet<string>();

        public bool IsSaving
        {
            get
            {
                return !string.IsNullOrEmpty(jsonPath) && savingPaths.Contains(jsonPath);
            }
            set
            {
                if (string.IsNullOrEmpty(jsonPath)) { return; }
                bool prevValue = IsSaving;
                if (value && !prevValue)
                {
                    savingPaths.Add(jsonPath);
                }
                if (!value && prevValue)
                {
                    savingPaths.Remove(jsonPath);
                }
            }
        }
        public bool IsReading {
            get {
                if (string.IsNullOrEmpty(jsonPath)) { return false; }
                return readingPaths.Contains(jsonPath);
            }
            set
            {
                if (string.IsNullOrEmpty(jsonPath)) { return; }
                bool prevValue = IsReading;
                if (value && !prevValue)
                {
                    readingPaths.Add(jsonPath);
                }
                if (!value && prevValue)
                {
                    readingPaths.Remove(jsonPath);
                }
            }
        }
        [JsonIgnore]
        private string jsonPath;
        [JsonIgnore]
        private FileSystemWatcher configFileWatcher;
        private int lastFileHash = -1;

        /// <summary>
        /// Gets or sets a value indicating whether this config file will contain all fields,
        /// whether they match the field's default value or not.
        /// (By default, fields which match the default value do not get saved.)
        /// </summary>
        [DefaultValue(true)]
        public bool OutputAllJsonConfigFields { get; set; } = false;

        // Note: the DefaultValue and initialized value of OutputAllJsonConfigFields are intentionally different...
        // this is the easiest way to make this field always saved.

        /// <summary>
        /// Loads a json file at the specified path whose structure matches the provided class type.
        /// If the file does not exist, Load will create it.
        /// Optionally, sets up a callback to automatically update the config object when a file change is detected.
        /// </summary>
        /// <typeparam name="T">The type (derived from ApplicationConfig) to be loaded.</typeparam>
        /// <param name="prefsJsonPath">The full path to the JSON file.</param>
        /// <param name="actionOnFileChange">Optional. If supplied, the filesystem will watch for changes to the config file.
        /// When changes are detected, the new file will be loaded and actionOnfileChange will be invoked with the loaded config object as its parameter.
        /// It is recommended that this callback should overwrite the variable or field to which this is being assigned, and should perform any re-initialization required.</param>
        /// <returns>The loaded config object.</returns>
        public static T Load<T>(string prefsJsonPath, Action<T> actionOnFileChange = null)
            where T : ApplicationConfig, new()
        {
            T rtn = null;
            if (!Directory.Exists(Path.GetDirectoryName(prefsJsonPath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(prefsJsonPath));
            }

            if (File.Exists(prefsJsonPath))
            {
                try
                {
                    var jss = JsonHelpers.GetUnityProjectJsonSerializerSettings();
                    string fileText = File.ReadAllText(prefsJsonPath);
                    SelectiveLogger.Log(nameof(ApplicationConfig), $"Loading config file at {prefsJsonPath}. File contents:\n" +
                                                                   fileText);
                    rtn = JsonConvert.DeserializeObject<T>(fileText, jss);
                    rtn.jsonPath = prefsJsonPath;
                }
                catch (Exception e)
				{
                    Debug.LogError($"{typeof(T)}: Exception while loading existing config file. Will create a new prefs object. Debug information follows:");
                    try
                    {
                        Debug.LogException(e);
                        Debug.Log($"File info: {new FileInfo(prefsJsonPath)}");
                        Debug.Log($"File contents: {File.ReadAllText(prefsJsonPath)}");
                    }
                    catch (Exception) { }
				}
            }

            if (rtn == null)
            {
                rtn = new T();
                rtn.jsonPath = prefsJsonPath;
            }

            try
            {
                // save it back out so the user has something to edit
                rtn.Save();
                rtn.lastFileHash = GetFileHash(prefsJsonPath);
            }
            catch (Exception e)
			{
                Debug.LogError($"{typeof(T)}: Exception trying to save config file in next message");
                Debug.LogException(e);
			}

            try
            {
                if (actionOnFileChange != null)
                {
                    rtn.configFileWatcher = new FileSystemWatcher();
                    int directoryIndex = prefsJsonPath.LastIndexOfAny(new char[] { '\\', '/' });
                    rtn.configFileWatcher.Path = prefsJsonPath.Substring(0, directoryIndex);
                    rtn.configFileWatcher.Filter = prefsJsonPath.Substring(directoryIndex + 1);
                    rtn.configFileWatcher.NotifyFilter = NotifyFilters.LastWrite;
                    rtn.configFileWatcher.Changed += async (sender, eventArgs) =>
                    {
                        int newHash = GetFileHash(prefsJsonPath);
                        if (!rtn.IsSaving && File.Exists(prefsJsonPath) && newHash != rtn.lastFileHash)
                        {
                            FileSystemWatcher oldWatcher = rtn.configFileWatcher;
                            var jss = JsonHelpers.GetUnityProjectJsonSerializerSettings();
                            rtn.IsReading = true;
                            rtn = JsonConvert.DeserializeObject<T>(File.ReadAllText(prefsJsonPath), jss);
                            rtn.jsonPath = prefsJsonPath;
                            rtn.IsReading = false;
                            rtn.lastFileHash = newHash;
                            rtn.Save();
                            rtn.configFileWatcher = oldWatcher;
                            await UniTask.SwitchToMainThread(PlayerLoopTiming.Update);
                            actionOnFileChange(rtn);
                        }
                    };
                    rtn.configFileWatcher.EnableRaisingEvents = true;
                }
            }
            catch (Exception e)
			{
                Debug.LogError($"{typeof(T)}: Error setting up file watcher");
                Debug.LogException(e);
			}

            return rtn;
        }

        /// <summary>
        /// Saves the config file into the same file path from which it was loaded.
        /// </summary>
        /// <param name="writeAllFields">If true, forcibly write all fields, including those which match their default values.</param>
        public void Save(bool writeAllFields = false)
        {
            if (IsReading || IsSaving) { return; }
            IsSaving = true;
            if (writeAllFields)
            {
                OutputAllJsonConfigFields = true;
            }

            var jss = JsonHelpers.GetUnityProjectJsonSerializerSettings();
            if (OutputAllJsonConfigFields)
            {
                jss.DefaultValueHandling = DefaultValueHandling.Populate;
            }

            try
            {
                File.WriteAllText(jsonPath, JsonConvert.SerializeObject(this, Formatting.Indented, jss));
            }
            catch (IOException e)
            {
                //just log the issue and continue
                Debug.LogException(e);
            }

            IsSaving = false;
        }

        /// <summary>
        /// Disposes the file watcher associated with this config.
        /// </summary>
        public void Dispose()
        {
            if (configFileWatcher != null)
            {
                configFileWatcher.Dispose();
            }
        }

        /// <summary>
        /// Gets the MD5 hash in Int32 form of the supplied file path. Used for checking for changes in config files.
        /// </summary>
        /// <param name="path">The path to check.</param>
        /// <returns>The MD5 hash.</returns>
        private static int GetFileHash(string path)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                using (var stream = File.OpenRead(path))
                {
                    return BitConverter.ToInt32(md5.ComputeHash(stream), 0);
                }
            }
        }
    }
}
