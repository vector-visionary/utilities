using System;
using System.Collections.Generic;
using UnityEngine;

namespace MantaMiddleware.LogUtilities {
    /// <summary>
    /// An improvement over standard Debug.Log entries which allows easy options for
    /// verbosity relative to certain categories of log messages.
    /// Also allows for more detailed log messages (including update phase, exact time, frame count, etc).
    /// FUTURE: will allow logging to different places per key, and customizing the type of information logged in each message.
    /// </summary>
    public static class SelectiveLogger
    {
        private static SelectiveLogSettings DefaultSettings =
            new SelectiveLogSettings("Default")
            {
                Level = SelectiveLogLevel.Standard
            };

        private const int MAX_KEY_LENGTH = 20;

        /// <summary>
        /// Whether the given key should log critical messages. In the current implementation, this is always true.
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <returns>True.</returns>
        public static bool IsLoggingCritical(object key)
        {
            return true;
        }

        /// <summary>
        /// Whether the given key should log non-critical messages.
        /// By default, in Debug builds this will be true; in Release builds this is false.
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <returns>Whether the log level for this key (or the default) is Standard or higher.</returns>
        public static bool IsLogging(object key)
        {
            return GetLogLevel(key) >= SelectiveLogLevel.Standard;
        }

        /// <summary>
        /// Whether the given key should log highly-detailed verbose messages.
        /// By default, this is false; it can be enabled in the SROptions screen.
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <returns>Whether the log level for this key (or the default) is Verbose or higher.</returns>
        public static bool IsVerbose(object key)
        {
            return GetLogLevel(key) >= SelectiveLogLevel.Verbose;
        }

        private static SelectiveLogSettings GetSettings(object key)
        {
            if (keysToSettings.TryGetValue(KeyToString(key), out var settings))
            {
                return settings;
            }
            InitializeSetting(KeyToString(key));
            return DefaultSettings;
        }

        /// <summary>
        /// Gets the current logging level for the given key, or the default level if none is set for this key.
        /// (Will never return SelectiveLogLevel.UseDefault; will return the default log level instead.)
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <returns>The log level to use for this key.</returns>
        public static SelectiveLogLevel GetLogLevel(object key)
        {
            if (keysToSettings.TryGetValue(KeyToString(key), out var logSettings))
            {

                if (logSettings.Level != SelectiveLogLevel.UseDefault)
                {
                    return logSettings.Level;
                }

                return DefaultSettings.Level;
            }

            InitializeSetting(KeyToString(key));
            return DefaultSettings.Level;
        }

        /// <summary>
        /// Sets the log level of the given key.
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <param name="level">The new log level, or UseDefault to revert to the global default.</param>
        public static void SetLogLevel(object key, SelectiveLogLevel level)
        {
            string keyString = KeyToString(key);
            if (keysToSettings.TryGetValue(keyString, out var logSettings))
            {
                logSettings.Level = level;
            }
            else
            {
                InitializeSetting(keyString, level);
            }
        }

        private static void InitializeSetting(string keyString, SelectiveLogLevel level = SelectiveLogLevel.UseDefault)
        {
            var newSettings = new SelectiveLogSettings(keyString) {Level = level};
            keysToSettings.Add(keyString, newSettings);
        }

        /// <summary>
        /// Logs the given message according to the settings stored for <paramref name="key"/>.
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <param name="LogMessage">The message to log; will be augmented with additional data before logging.</param>
        public static void Log(object key, string LogMessage)
        {
            Log_Impl(key, LogMessage, LogType.Log);
        }
        /// <summary>
        /// Logs the given warning message according to the settings stored for <paramref name="key"/>.
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <param name="LogMessage">The message to log; will be augmented with additional data before logging.</param>
        public static void LogWarning(object key, string LogMessage)
        {
            Log_Impl(key, LogMessage, LogType.Warning);
        }
        /// <summary>
        /// Logs the given error message according to the settings stored for <paramref name="key"/>.
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <param name="LogMessage">The message to log; will be augmented with additional data before logging.</param>
        public static void LogError(object key, string LogMessage)
        {
            Log_Impl(key, LogMessage, LogType.Error);
        }

        /// <summary>
        /// Logs the given Exception according to the settings stored for <paramref name="key"/>.
        /// </summary>
        /// <param name="key">The key; may be a class (e.g. GetType()), or per object (e.g. this).</param>
        /// <param name="exception">The exception to log</param>
        /// <param name="LogMessage">The message to log; will be augmented with additional data before logging.</param>
        public static void LogException(object key, Exception exception, string LogMessage = null)
        {
            if (string.IsNullOrEmpty(LogMessage))
            {
                LogMessage = "(no context provided)";
            }
            Log_Impl(key, $"<b>{exception.GetType()}</b>; stacktrace follows. <b>{LogMessage}</b>", LogType.Exception);
            Debug.LogException(exception);
        }

        private static void Log_Impl(object key, string LogMessage, LogType logType)
        {
            UnityEngine.Object keyAsUnityObject = key as UnityEngine.Object;
            string keyAsString = KeyToString(key);
            string message = $"<b>{keyAsString}</b>: {LogMessage}\n{GetContextString(GetSettings(keyAsString))}";

            switch (logType)
            {
                case LogType.Warning:
                    Debug.LogWarning(message, keyAsUnityObject);
                    break;
                case LogType.Error: case LogType.Exception: // log exceptions as errors here since we don't have the exception object
                    Debug.LogError(message, keyAsUnityObject);
                    break;
                case LogType.Log: default:
                    Debug.Log(message, keyAsUnityObject);
                    break;
            }
        }


        /// <summary>
        /// Gets the second line for the debug log with further information.
        /// </summary>
        /// <param name="settings">Not currently used; planned use in the future for selecting the formatting of this string.</param>
        /// <returns></returns>
        private static string GetContextString(SelectiveLogSettings settings)
        {
            var now = DateTime.UtcNow;
            return $"[{Time.realtimeSinceStartup:#.000}] [F={Time.frameCount}] [U={now:yyyy/MM/dd HH:mm:ss.ffff}] [P={GetCurrentUpdatePhase()}]";
        }


        private static string GetCurrentUpdatePhase()
        {
            return PhaseTrackerBase.CurrentPhaseString;
        }

        private static string KeyToString(object key)
        {
            if (key is string keyS)
            {
                return keyS;
            }
            if (key == null)
            {
                return "NULL";
            }
            if (key is Component componentKey)
            {
                return $"{componentKey.gameObject.name}.{componentKey.GetType()}";
            }
            if (key is Type)
            {
                return key.ToString();
            }

            return key.GetType().ToString();
        }


        public enum SelectiveLogLevel { UseDefault = -1, Critical = 0, Standard = 1, Verbose = 2 }
        private static Dictionary<string, SelectiveLogSettings> keysToSettings = new Dictionary<string, SelectiveLogSettings>();

        [Serializable]
        public class SelectiveLogSettings
        {
            public SelectiveLogSettings(string key)
            {
                _key = key;
            }

            public string Key => _key;
            public int Index { get; set; }

            private string _key;
            public SelectiveLogLevel Level = SelectiveLogLevel.UseDefault;

            //TODO: options for context string
            //TODO: options for where to log (to Debug.Log as now, to a custom file, etc)


            public void CycleLogLevel()
            {
                Level = (SelectiveLogLevel)( ((int)Level + 1) % 3);
            }
        }
    }
}