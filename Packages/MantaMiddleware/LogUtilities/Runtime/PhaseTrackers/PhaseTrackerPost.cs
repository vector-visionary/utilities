namespace MantaMiddleware.LogUtilities
{

    public class PhaseTrackerPost : PhaseTrackerBase
    {
        public override bool IsPost => true;
    }
}
