namespace MantaMiddleware.LogUtilities
{
    public class PhaseTrackerPre : PhaseTrackerBase
    {
        public override bool IsPost => false;
    }
}