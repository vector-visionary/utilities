using System.Collections;
using UnityEngine;

namespace MantaMiddleware.LogUtilities
{
    public class PhaseTrackerBase : MonoBehaviour
    {
        public static string CurrentPhaseString
        {
            get
            {
                if (_currentPhaseString == null)
                {
                    _currentPhaseString = $"{_currentPhase}_{(_isCurrentPhasePost ? "After" : "During")}";
                }

                return _currentPhaseString;
            }
        }
        private static string _currentPhaseString = null;
        private static bool _isCurrentPhasePost = false;
        private static PhaseEnum _currentPhase = PhaseEnum.Uninitialized;

        public enum PhaseEnum { Uninitialized, FixedUpdate, Update, LateUpdate, EndOfFrame, Quitting }

        private static void SetCurrentPhase(PhaseEnum phase, bool isPost)
        {
            _currentPhaseString = null;
            _isCurrentPhasePost = isPost;
            _currentPhase = phase;
        }


        public virtual bool IsPost => false;

        private IEnumerator Start()
        {
            var eof = new WaitForEndOfFrame();
            while (true)
            {
                yield return eof;
                SetCurrentPhase(PhaseEnum.EndOfFrame, IsPost);
                yield return null;
            }
        }

        private void FixedUpdate()
        {
            SetCurrentPhase(PhaseEnum.FixedUpdate, IsPost);
        }

        private void Update()
        {
            SetCurrentPhase(PhaseEnum.Update, IsPost);
        }

        private void LateUpdate()
        {
            SetCurrentPhase(PhaseEnum.LateUpdate, IsPost);
        }

        private void OnApplicationQuit()
        {
            SetCurrentPhase(PhaseEnum.Quitting, IsPost);
        }
    }
}